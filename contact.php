<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- CONTACT::START -->
<div class="contact_section ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="contact_map mb_30">
                    <div id="contact-map"></div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-xl-12">
                <div class="contact_address">
                    <div class="row justify-content-end">
                        <div class="col-lg-6">
                            <div class="contact_box_wrapper">
                                <div class="contact_wiz_box">
                                    <span class="contact_box_title font_16 f_w_500 d-block lh-1 ">Call or WhatsApp:</span>
                                    <h4 class="contact_box_desc mb-0">+31(0)77 472 3683</h4>
                                </div>
                                <div class="contact_wiz_box">
                                    <span class="contact_box_title font_16 f_w_500 d-block lh-1 ">Get in touch:</span>
                                    <h4 class="contact_box_desc mb-0">info@spondonit.com</h4>
                                </div>
                                <div class="contact_wiz_box">
                                    <span class="contact_box_title font_16 f_w_500 d-block lh-1 ">Social Media:</span>
                                    <div class="contact_link">
                                        <a href="#">
                                            <i class="fab fa-facebook"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fab fa-linkedin-in"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="contact_wiz_box">
                                    <span class="contact_box_title font_16 f_w_500 d-block lh-1 ">Head office:</span>
                                    <h4 class="contact_box_desc mb-0">Ubroekweg 24, 5328 <br>
                                NM Venlo The Netherlands.</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="contact_form_box mb_30">
                                <div class="contact_info">
                                    <div class="contact_title mb_30">
                                        <h4 class="">Get in Touch</h4>
                                    </div>
                                </div>
                                <form class="form-area contact-form" id="myForm" action="mail.php" method="post">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <select class="amaz_select2 style2 wide mb_30" >
                                                <option value="1">Reason for Contact</option>
                                                <option value="1">order Details</option>
                                                <option value="1">order issue</option>
                                            </select>
                                        </div>
                                        <div class="col-xl-6">
                                            <input name="name" placeholder="First Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'"
                                                class="primary_line_input style4 mb_10" required="" type="text">
                                        </div>
                                        <div class="col-xl-6">
                                            <input name="name" placeholder="Last Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'"
                                                class="primary_line_input style4 mb_10" required="" type="text">
                                        </div>
                                        <div class="col-xl-12">
                                        <input name="email" placeholder="Type e-mail address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"
                                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail address'" class="primary_line_input style4  mb_10"
                                                required="" type="email">
                                        </div>
                                        <div class="col-xl-6">
                                            <input name="name" placeholder="Phone No" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone No'"
                                                class="primary_line_input style4 mb_10" required="" type="text">
                                        </div>
                                        <div class="col-xl-6">
                                            <input name="name" placeholder="Order No" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Order No'"
                                                class="primary_line_input style4 mb_10" required="" type="text">
                                        </div>
                                        <div class="col-xl-12">
                                            <textarea class="primary_line_textarea style4 mb_40" name="message" placeholder="Write Message here…" onfocus="this.placeholder = ''"
                                                onblur="this.placeholder = 'Write Message here…'" required=""></textarea>
                                        </div>
                                        <div class="col-lg-12 text-right">
                                            <div class="alert-msg"></div>
                                            <button type="submit" class="amaz_primary_btn style2 submit-btn text-center f_w_700 text-uppercase rounded-0 w-100" >Send message</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CONTACT::END -->


<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>