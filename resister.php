<?php include 'include/header.php' ?>

<div class="amazy_login_area">
    <div class="amazy_login_area_left d-flex align-items-center justify-content-center">
        <div class="amazy_login_form">
            <a href="index.php " class="logo mb_50 d-block">
                <img src="img/logo.png" alt="">
            </a>
            <h3 class="m-0">Sign Up</h3>
            <p class="support_text">See your growth and get consulting support!</p>
            <a href="#" class="google_logIn d-flex align-items-center justify-content-center">
                <img src="img/svg/google_icon.svg" alt="">
                <h5 class="m-0 font_16 f_w_500">Sign up with Google</h5>
            </a>
            <div class="form_sep2 d-flex align-items-center">
                <span class="sep_line flex-fill"></span>
                <span class="form_sep_text font_14 f_w_500 ">or Sign in with Email</span>
                <span class="sep_line flex-fill"></span>
            </div>
            <form action="#">
                <div class="row">
                    <div class="col-lg-12">
                        <label class="primary_label2">Name <span>*</span> </label>
                        <input name="name" placeholder="Enter First Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter First Name'" class="primary_input3 radius_5px  mb_20" required="" type="text">
                    </div>
                    <div class="col-12">
                        <label class="primary_label2">Email Address <span>*</span> </label>
                        <input name="name" placeholder="Enter user name or email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter user name or email'" class="primary_input3 radius_5px mb_20" required="" type="text">
                    </div>
                    <div class="col-12">
                        <label class="primary_label2">Password <span>*</span></label>
                        <input name="name" placeholder="Min. 8 Character" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Password'" class="primary_input3 radius_5px mb_20" required="" type="password">
                    </div>
                    <div class="col-12 mb_20">
                        <label class="primary_checkbox d-flex mb_30">
                            <input type="checkbox">
                            <span class="checkmark mr_10"></span>
                            <span class="label_name">I agree with the terms and conditions.</span>
                        </label>
                    </div>
                    <div class="col-12">
                        <button class="amaz_primary_btn style2 radius_5px  w-100 text-uppercase  text-center mb_25">Sign Up</button>
                    </div>
                    <div class="col-12">
                        <p class="sign_up_text">Already have an Account?  <a href="login.php">Sign in</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="amazy_login_area_right d-flex align-items-center justify-content-center">
        <div class="amazy_login_area_right_inner d-flex align-items-center justify-content-center flex-column">
            <div class="thumb">
                <img class="img-fluid" src="img/banner/login_img.png" alt="">
            </div>
            <div class="login_text d-flex align-items-center justify-content-center flex-column text-center">
                <h4>Turn your ideas into reality.</h4>
                <p class="m-0">Consistent quality and experience across
                all platforms and devices.</p>
            </div>
        </div>
    </div>
</div>



<?php include 'include/footer.php' ?>