<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<div class="amazy_dashboard_area dashboard_bg section_spacing6">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <?php include 'include/dashboard_sidebar.php' ?>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="dashboard_white_box style2 bg-white mb_25">
                    <div class="dashboard_white_box_body">
                        <div class="table-responsive">
                            <table class="table amazy_table style2 mb-0">
                                <thead>
                                    <tr>
                                    <th class="font_14 f_w_700" scope="col">Amount</th>
                                    <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">Name</th>
                                    <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">QTY</th>
                                    <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">Secret Code</th>
                                    <th class="font_14 f_w_700" scope="col">Is Used</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <span class="font_14 f_w_500">$140.00</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">Huge Jackman</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">02</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">6ZYEZ6OA</span>
                                        </td>
                                        <td>
                                            <a href="#" class="line_badge_btn text-nowrap text-uppercase text-center">Redeem</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="font_14 f_w_500">$140.00</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">Huge Jackman</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">02</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">6ZYEZ6OA</span>
                                        </td>
                                        <td>
                                            <a href="#" class="line_badge_btn2 text-nowrap text-uppercase text-center">Used</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="font_14 f_w_500">$140.00</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">Huge Jackman</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">02</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">6ZYEZ6OA</span>
                                        </td>
                                        <td>
                                            <a href="#" class="line_badge_btn text-nowrap text-uppercase text-center">Redeem</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="font_14 f_w_500">$140.00</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">Huge Jackman</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">02</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">6ZYEZ6OA</span>
                                        </td>
                                        <td>
                                            <a href="#" class="line_badge_btn2 text-nowrap text-uppercase text-center">Used</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="font_14 f_w_500">$140.00</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">Huge Jackman</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">02</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">6ZYEZ6OA</span>
                                        </td>
                                        <td>
                                            <a href="#" class="line_badge_btn text-nowrap text-uppercase text-center">Redeem</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="font_14 f_w_500">$140.00</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">Huge Jackman</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">02</span>
                                        </td>
                                        <td>
                                            <span class="font_14 f_w_500">6ZYEZ6OA</span>
                                        </td>
                                        <td>
                                            <a href="#" class="line_badge_btn2 text-nowrap text-uppercase text-center">Used</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>