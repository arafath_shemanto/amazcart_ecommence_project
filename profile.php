<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<div class="amazy_dashboard_area dashboard_bg section_spacing6">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <?php include 'include/dashboard_sidebar.php' ?>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="profile_white_box bg-white">
                    <ul class="nav profile_tabs mb_40" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="Info-tab" data-bs-toggle="tab" data-bs-target="#Info" type="button" role="tab" aria-controls="Info" aria-selected="true">Basic Info</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="Password-tab" data-bs-toggle="tab" data-bs-target="#Password" type="button" role="tab" aria-controls="Password" aria-selected="false">Change Password</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link " id="Address-tab" data-bs-toggle="tab" data-bs-target="#Address" type="button" role="tab" aria-controls="Address" aria-selected="false">Address</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="Info" role="tabpanel" aria-labelledby="Info-tab">
                            <!-- content ::start  -->
                            <div class="dashboard_account_wrapper mb_20">
                                <!-- form  -->
                                <form action="#">
                                    <div class="row">
                                        <div class="col-12">
                                            <label class="primary_label2 style2 ">First Name <span>*</span></label>
                                            <input name="name" placeholder="E.g. Huge" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E.g. Huge'" class="primary_input3 style4 mb_30" required="" type="text">
                                        </div>
                                        <div class="col-12">
                                            <label class="primary_label2 style2 ">Last Name</label>
                                            <input name="name" placeholder="Last Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'" class="primary_input3 style4 mb_30" required="" type="text">
                                        </div>
                                        <div class="col-12">
                                            <label class="primary_label2 style2 ">E-mail Address <span>*</span></label>
                                            <input name="name" placeholder="Type e-mail address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail address'" class="primary_input3 style4 mb_30" required="" type="text">
                                        </div>
                                        <div class="col-12">
                                            <label class="primary_label2 style2 ">Phone Number</label>
                                            <input name="name" placeholder="01XX-XXX-XXX" onfocus="this.placeholder = ''" onblur="this.placeholder = '01XX-XXX-XXX'" class="primary_input3 style4 mb_30" required="" type="text">
                                        </div>
                                        <div class="col-12 mb_30">
                                            <label class="primary_label2 style2 ">Date of Birth</label>
                                            <input id="start_datepicker" name="name" placeholder="23-06-1995" onfocus="this.placeholder = ''" onblur="this.placeholder = '23-06-1995'" class="primary_input3 style4 mb-0" required="" type="text">
                                            
                                        </div>
                                        <div class="col-12">
                                            <label class="primary_label2 style2 ">Description</label>
                                            <textarea  name="name" placeholder="Write description here…" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Write description here… here…'" class="primary_textarea4 radius_5px mb_25" required=""></textarea>
                                        </div>
                                        <div class="col-12 d-flex justify-content-end">
                                            <button class="amaz_primary_btn style2 rounded-0  text-uppercase  text-center min_200">update now</button>
                                        </div>
                                    </div>
                                </form>
                                <!--/ form  -->
                                <div class="account_img_upload">
                                    <div class="thumb mb_20">
                                        <img class="img-fluid" src="img/account_img.png" alt="">
                                    </div>
                                    <div class="theme_img_uploader mb_13 d-flex align-items-center gap_20">
                                        <span class="img_upload_btn position-relative">
                                            <input class="form-control position-absolute start-0 top-0 bottom-0 end-0 " type="file">
                                            <span class="font_14 f_w_500 ">Choose file</span>
                                        </span>
                                        <p class="font_14 f_w_400 mute_text">No file chosen</p>
                                    </div>
                                    <p class="font_14 f_w_400 m-0 lh-1">(200x200) 2MB</p>
                                </div>
                            </div>
                            
                            <!-- content ::end  -->
                        </div>
                        <div class="tab-pane fade " id="Password" role="tabpanel" aria-labelledby="Password-tab">
                            <!-- content ::start  -->
                            <form action="#">
                                <div class="row">
                                    <div class="col-12">
                                        <label class="primary_label">Current Password</label>
                                        <input name="name" placeholder="Current Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Current Password'" class="primary_input3 style3 mb_30" required="" type="password">
                                    </div>
                                    <div class="col-12">
                                        <label class="primary_label">New Password</label>
                                        <input name="name" placeholder="New Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'New Password'" class="primary_input3 style3 mb_30" required="" type="password">
                                    </div>
                                    <div class="col-12">
                                        <label class="primary_label">Re-enter New Password</label>
                                        <input name="name" placeholder="Re-enter New Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Re-enter New Password'" class="primary_input3 style3 mb_30" required="" type="password">
                                    </div>
                                    <div class="col-12">
                                        <button class="amaz_primary_btn style2 rounded-0  text-uppercase  text-center min_200">update now</button>
                                    </div>
                                </div>
                            </form>
                            <!-- content ::end  -->
                        </div>
                        <div class="tab-pane fade " id="Address" role="tabpanel" aria-labelledby="Address-tab">
                            <!-- content ::start  -->
                            <div class="table-responsive mb_30">
                                <table class="table amazy_table style6 mb-0">
                                    <thead>
                                        <tr>
                                        <th class="font_14 f_w_700" scope="col">Full Name</th>
                                        <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">Address</th>
                                        <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">Region</th>
                                        <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">E-mail</th>
                                        <th class="font_14 f_w_700 text-nowrap" scope="col">Phone Number</th>
                                        <th class="font_14 f_w_700 text-nowrap" scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">Huge Jackman</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">221B Baker Street, P. O Box 3 <br> Park Road, USA - 215431</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">New York, USA</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">info@spondon.com</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap" >088 547 3214</span>
                                            </td>
                                            <td>
                                            <button class="amazy_status_btn" data-bs-toggle="modal" data-bs-target="#Address_modal">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15">
                                                        <g  data-name="edit (1)" transform="translate(0 -0.004)">
                                                            <g  data-name="Group 1742" transform="translate(0 1.074)">
                                                            <g  data-name="Group 1741">
                                                                <path  data-name="Path 3050" d="M12.324,40.566a.536.536,0,0,0-.536.536V46.46a.536.536,0,0,1-.536.536H1.607a.536.536,0,0,1-.536-.536V35.744a.536.536,0,0,1,.536-.536h6.43a.536.536,0,1,0,0-1.072H1.607A1.607,1.607,0,0,0,0,35.744V46.46a1.607,1.607,0,0,0,1.607,1.607h9.645A1.607,1.607,0,0,0,12.86,46.46V41.1A.536.536,0,0,0,12.324,40.566Z" transform="translate(0 -34.137)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                            <g  data-name="Group 1744" transform="translate(3.229 0.004)">
                                                            <g  data-name="Group 1743" transform="translate(0 0)">
                                                                <path  data-name="Path 3051" d="M113.58.6a2.048,2.048,0,0,0-2.9,0l-7.048,7.047a.541.541,0,0,0-.129.209l-1.07,3.21a.535.535,0,0,0,.507.7.544.544,0,0,0,.169-.027l3.21-1.07a.535.535,0,0,0,.209-.129L113.58,3.5A2.048,2.048,0,0,0,113.58.6Zm-.757,2.141L105.868,9.7l-2.078.694.692-2.076,6.959-6.956a.978.978,0,1,1,1.384,1.382Z" transform="translate(-102.409 -0.004)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">Huge Jackman</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">221B Baker Street, P. O Box 3 <br> Park Road, USA - 215431</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">New York, USA</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">info@spondon.com</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap" >088 547 3214</span>
                                            </td>
                                            <td>
                                            <button class="amazy_status_btn " data-bs-toggle="modal" data-bs-target="#Address_modal">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15">
                                                        <g  data-name="edit (1)" transform="translate(0 -0.004)">
                                                            <g  data-name="Group 1742" transform="translate(0 1.074)">
                                                            <g  data-name="Group 1741">
                                                                <path  data-name="Path 3050" d="M12.324,40.566a.536.536,0,0,0-.536.536V46.46a.536.536,0,0,1-.536.536H1.607a.536.536,0,0,1-.536-.536V35.744a.536.536,0,0,1,.536-.536h6.43a.536.536,0,1,0,0-1.072H1.607A1.607,1.607,0,0,0,0,35.744V46.46a1.607,1.607,0,0,0,1.607,1.607h9.645A1.607,1.607,0,0,0,12.86,46.46V41.1A.536.536,0,0,0,12.324,40.566Z" transform="translate(0 -34.137)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                            <g  data-name="Group 1744" transform="translate(3.229 0.004)">
                                                            <g  data-name="Group 1743" transform="translate(0 0)">
                                                                <path  data-name="Path 3051" d="M113.58.6a2.048,2.048,0,0,0-2.9,0l-7.048,7.047a.541.541,0,0,0-.129.209l-1.07,3.21a.535.535,0,0,0,.507.7.544.544,0,0,0,.169-.027l3.21-1.07a.535.535,0,0,0,.209-.129L113.58,3.5A2.048,2.048,0,0,0,113.58.6Zm-.757,2.141L105.868,9.7l-2.078.694.692-2.076,6.959-6.956a.978.978,0,1,1,1.384,1.382Z" transform="translate(-102.409 -0.004)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">Huge Jackman</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">221B Baker Street, P. O Box 3 <br> Park Road, USA - 215431</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">New York, USA</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">info@spondon.com</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap" >088 547 3214</span>
                                            </td>
                                            <td>
                                            <button class="amazy_status_btn" data-bs-toggle="modal" data-bs-target="#Address_modal">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15">
                                                        <g  data-name="edit (1)" transform="translate(0 -0.004)">
                                                            <g  data-name="Group 1742" transform="translate(0 1.074)">
                                                            <g  data-name="Group 1741">
                                                                <path  data-name="Path 3050" d="M12.324,40.566a.536.536,0,0,0-.536.536V46.46a.536.536,0,0,1-.536.536H1.607a.536.536,0,0,1-.536-.536V35.744a.536.536,0,0,1,.536-.536h6.43a.536.536,0,1,0,0-1.072H1.607A1.607,1.607,0,0,0,0,35.744V46.46a1.607,1.607,0,0,0,1.607,1.607h9.645A1.607,1.607,0,0,0,12.86,46.46V41.1A.536.536,0,0,0,12.324,40.566Z" transform="translate(0 -34.137)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                            <g  data-name="Group 1744" transform="translate(3.229 0.004)">
                                                            <g  data-name="Group 1743" transform="translate(0 0)">
                                                                <path  data-name="Path 3051" d="M113.58.6a2.048,2.048,0,0,0-2.9,0l-7.048,7.047a.541.541,0,0,0-.129.209l-1.07,3.21a.535.535,0,0,0,.507.7.544.544,0,0,0,.169-.027l3.21-1.07a.535.535,0,0,0,.209-.129L113.58,3.5A2.048,2.048,0,0,0,113.58.6Zm-.757,2.141L105.868,9.7l-2.078.694.692-2.076,6.959-6.956a.978.978,0,1,1,1.384,1.382Z" transform="translate(-102.409 -0.004)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">Huge Jackman</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">221B Baker Street, P. O Box 3 <br> Park Road, USA - 215431</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">New York, USA</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap">info@spondon.com</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_400 mute_text text-nowrap" >088 547 3214</span>
                                            </td>
                                            <td>
                                            <button class="amazy_status_btn" data-bs-toggle="modal" data-bs-target="#Address_modal">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15">
                                                        <g  data-name="edit (1)" transform="translate(0 -0.004)">
                                                            <g  data-name="Group 1742" transform="translate(0 1.074)">
                                                            <g  data-name="Group 1741">
                                                                <path  data-name="Path 3050" d="M12.324,40.566a.536.536,0,0,0-.536.536V46.46a.536.536,0,0,1-.536.536H1.607a.536.536,0,0,1-.536-.536V35.744a.536.536,0,0,1,.536-.536h6.43a.536.536,0,1,0,0-1.072H1.607A1.607,1.607,0,0,0,0,35.744V46.46a1.607,1.607,0,0,0,1.607,1.607h9.645A1.607,1.607,0,0,0,12.86,46.46V41.1A.536.536,0,0,0,12.324,40.566Z" transform="translate(0 -34.137)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                            <g  data-name="Group 1744" transform="translate(3.229 0.004)">
                                                            <g  data-name="Group 1743" transform="translate(0 0)">
                                                                <path  data-name="Path 3051" d="M113.58.6a2.048,2.048,0,0,0-2.9,0l-7.048,7.047a.541.541,0,0,0-.129.209l-1.07,3.21a.535.535,0,0,0,.507.7.544.544,0,0,0,.169-.027l3.21-1.07a.535.535,0,0,0,.209-.129L113.58,3.5A2.048,2.048,0,0,0,113.58.6Zm-.757,2.141L105.868,9.7l-2.078.694.692-2.076,6.959-6.956a.978.978,0,1,1,1.384,1.382Z" transform="translate(-102.409 -0.004)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <a href="#" class="amaz_primary_btn style2 rounded-0  text-uppercase  text-center min_200" data-bs-toggle="modal" data-bs-target="#Address_modal">add new address</a>
                            <!-- content ::end  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>