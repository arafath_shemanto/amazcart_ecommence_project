<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<div class="amazy_dashboard_area dashboard_bg section_spacing6">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <?php include 'include/dashboard_sidebar.php' ?>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="dashboard_white_box_header d-flex align-items-center gap_20  mb_20">
                    <h3 class="font_20 f_w_700 mb-0 ">Purchase History</h3>
                    <select class="amaz_select3" >
                        <option value="1">All History</option>
                        <option value="1">All History</option>
                        <option value="1">All History</option>
                        <option value="1">All History</option>
                    </select>
                </div>
                <div class="dashboard_white_box bg-white mb_25 pt-0 ">
                    <div class="dashboard_white_box_body">
                        <div class="table-responsive mb_30">
                            <table class="table amazy_table2 mb-0">
                                <thead>
                                    <tr>
                                    <th class="font_14 f_w_700" scope="col">Details</th>
                                    <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">Amount</th>
                                    <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">Delivery Status</th>
                                    <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">Payment Status</th>
                                    <th class="font_14 f_w_700" scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <h4 class="font_16 f_w_700  ">20211118-18162446</h4>
                                                <p class="font_14 f_w_500 mb-0 lh-1">14 Jan, 2022</p>
                                            </div>
                                        
                                        </td>
                                        <td>
                                            <h4 class="font_16 f_w_500 m-0 ">$240.00</h4>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn text-nowrap">Order Place</a>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn style4 text-nowrap">Paid</a>
                                        </td>
                                        <td>
                                            <div class="amazy_status_btns d-flex gap_5 align-items-center">
                                                <button class="amazy_status_btn" data-bs-toggle="modal" data-bs-target="#purchase_history_modal">
                                                    <svg  xmlns="http://www.w3.org/2000/svg" width="16" height="11.5" viewBox="0 0 16 11.5">
                                                        <path  data-name="Path 4189" d="M15.333,124.168H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -117.668)" fill="#fd4949"/>
                                                        <path  data-name="Path 4190" d="M15.333,1.5H.667A.712.712,0,0,1,0,.75.712.712,0,0,1,.667,0H15.333A.712.712,0,0,1,16,.75.712.712,0,0,1,15.333,1.5Zm0,0" fill="#fd4949"/>
                                                        <path  data-name="Path 4191" d="M15.333,246.832H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -235.332)" fill="#fd4949"/>
                                                    </svg>
                                                </button>
                                                <button class="amazy_status_btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.333" height="14" viewBox="0 0 9.333 14">
                                                        <g  data-name="download (1)" transform="translate(-85.334 0)">
                                                            <g  data-name="Group 3491" transform="translate(85.334 0)">
                                                            <g  data-name="Group 3490">
                                                                <path  data-name="Path 4187" d="M89.588,11.493h0c.013.013.028.026.042.038l.021.016.025.018.025.015.023.014.027.013.025.012.026.01.028.01.026.007.029.007.031,0,.026,0a.587.587,0,0,0,.115,0l.026,0,.031,0,.029-.007.026-.007.028-.01.026-.01.025-.012.027-.013.023-.014.025-.015.025-.018.021-.016q.022-.018.042-.038h0L94.5,7.41a.583.583,0,0,0-.825-.825L90.584,9.672V.583a.583.583,0,0,0-1.167,0V9.672L86.33,6.586a.583.583,0,0,0-.825.825Z" transform="translate(-85.334)" fill="#fd4949"/>
                                                                <path  data-name="Path 4188" d="M94.084,469.333H85.917a.584.584,0,0,0,0,1.168h8.167a.584.584,0,0,0,0-1.168Z" transform="translate(-85.334 -456.501)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <h4 class="font_16 f_w_700 ">20211118-18162446</h4>
                                                <p class="font_14 f_w_500 mb-0 lh-1">14 Jan, 2022</p>
                                            </div>
                                        
                                        </td>
                                        <td>
                                            <h4 class="font_16 f_w_500 m-0 ">$240.00</h4>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn text-nowrap">Order Place</a>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn style4 text-nowrap">Paid</a>
                                        </td>
                                        <td>
                                            <div class="amazy_status_btns d-flex gap_5 align-items-center">
                                                <button class="amazy_status_btn" data-bs-toggle="modal" data-bs-target="#purchase_history_modal">
                                                    <svg  xmlns="http://www.w3.org/2000/svg" width="16" height="11.5" viewBox="0 0 16 11.5">
                                                        <path  data-name="Path 4189" d="M15.333,124.168H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -117.668)" fill="#fd4949"/>
                                                        <path  data-name="Path 4190" d="M15.333,1.5H.667A.712.712,0,0,1,0,.75.712.712,0,0,1,.667,0H15.333A.712.712,0,0,1,16,.75.712.712,0,0,1,15.333,1.5Zm0,0" fill="#fd4949"/>
                                                        <path  data-name="Path 4191" d="M15.333,246.832H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -235.332)" fill="#fd4949"/>
                                                    </svg>
                                                </button>
                                                <button class="amazy_status_btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.333" height="14" viewBox="0 0 9.333 14">
                                                        <g  data-name="download (1)" transform="translate(-85.334 0)">
                                                            <g  data-name="Group 3491" transform="translate(85.334 0)">
                                                            <g  data-name="Group 3490">
                                                                <path  data-name="Path 4187" d="M89.588,11.493h0c.013.013.028.026.042.038l.021.016.025.018.025.015.023.014.027.013.025.012.026.01.028.01.026.007.029.007.031,0,.026,0a.587.587,0,0,0,.115,0l.026,0,.031,0,.029-.007.026-.007.028-.01.026-.01.025-.012.027-.013.023-.014.025-.015.025-.018.021-.016q.022-.018.042-.038h0L94.5,7.41a.583.583,0,0,0-.825-.825L90.584,9.672V.583a.583.583,0,0,0-1.167,0V9.672L86.33,6.586a.583.583,0,0,0-.825.825Z" transform="translate(-85.334)" fill="#fd4949"/>
                                                                <path  data-name="Path 4188" d="M94.084,469.333H85.917a.584.584,0,0,0,0,1.168h8.167a.584.584,0,0,0,0-1.168Z" transform="translate(-85.334 -456.501)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <h4 class="font_16 f_w_700 ">20211118-18162446</h4>
                                                <p class="font_14 f_w_500 mb-0 lh-1">14 Jan, 2022</p>
                                            </div>
                                        
                                        </td>
                                        <td>
                                            <h4 class="font_16 f_w_500 m-0 ">$240.00</h4>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn text-nowrap">Order Place</a>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn style4 text-nowrap">Paid</a>
                                        </td>
                                        <td>
                                            <div class="amazy_status_btns d-flex gap_5 align-items-center">
                                                <button class="amazy_status_btn" data-bs-toggle="modal" data-bs-target="#purchase_history_modal">
                                                    <svg  xmlns="http://www.w3.org/2000/svg" width="16" height="11.5" viewBox="0 0 16 11.5">
                                                        <path  data-name="Path 4189" d="M15.333,124.168H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -117.668)" fill="#fd4949"/>
                                                        <path  data-name="Path 4190" d="M15.333,1.5H.667A.712.712,0,0,1,0,.75.712.712,0,0,1,.667,0H15.333A.712.712,0,0,1,16,.75.712.712,0,0,1,15.333,1.5Zm0,0" fill="#fd4949"/>
                                                        <path  data-name="Path 4191" d="M15.333,246.832H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -235.332)" fill="#fd4949"/>
                                                    </svg>
                                                </button>
                                                <button class="amazy_status_btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.333" height="14" viewBox="0 0 9.333 14">
                                                        <g  data-name="download (1)" transform="translate(-85.334 0)">
                                                            <g  data-name="Group 3491" transform="translate(85.334 0)">
                                                            <g  data-name="Group 3490">
                                                                <path  data-name="Path 4187" d="M89.588,11.493h0c.013.013.028.026.042.038l.021.016.025.018.025.015.023.014.027.013.025.012.026.01.028.01.026.007.029.007.031,0,.026,0a.587.587,0,0,0,.115,0l.026,0,.031,0,.029-.007.026-.007.028-.01.026-.01.025-.012.027-.013.023-.014.025-.015.025-.018.021-.016q.022-.018.042-.038h0L94.5,7.41a.583.583,0,0,0-.825-.825L90.584,9.672V.583a.583.583,0,0,0-1.167,0V9.672L86.33,6.586a.583.583,0,0,0-.825.825Z" transform="translate(-85.334)" fill="#fd4949"/>
                                                                <path  data-name="Path 4188" d="M94.084,469.333H85.917a.584.584,0,0,0,0,1.168h8.167a.584.584,0,0,0,0-1.168Z" transform="translate(-85.334 -456.501)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <h4 class="font_16 f_w_700 ">20211118-18162446</h4>
                                                <p class="font_14 f_w_500 mb-0 lh-1">14 Jan, 2022</p>
                                            </div>
                                        
                                        </td>
                                        <td>
                                            <h4 class="font_16 f_w_500 m-0 ">$240.00</h4>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn text-nowrap">Order Place</a>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn style4 text-nowrap">Paid</a>
                                        </td>
                                        <td>
                                            <div class="amazy_status_btns d-flex gap_5 align-items-center">
                                                <button class="amazy_status_btn" data-bs-toggle="modal" data-bs-target="#purchase_history_modal">
                                                    <svg  xmlns="http://www.w3.org/2000/svg" width="16" height="11.5" viewBox="0 0 16 11.5">
                                                        <path  data-name="Path 4189" d="M15.333,124.168H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -117.668)" fill="#fd4949"/>
                                                        <path  data-name="Path 4190" d="M15.333,1.5H.667A.712.712,0,0,1,0,.75.712.712,0,0,1,.667,0H15.333A.712.712,0,0,1,16,.75.712.712,0,0,1,15.333,1.5Zm0,0" fill="#fd4949"/>
                                                        <path  data-name="Path 4191" d="M15.333,246.832H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -235.332)" fill="#fd4949"/>
                                                    </svg>
                                                </button>
                                                <button class="amazy_status_btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.333" height="14" viewBox="0 0 9.333 14">
                                                        <g  data-name="download (1)" transform="translate(-85.334 0)">
                                                            <g  data-name="Group 3491" transform="translate(85.334 0)">
                                                            <g  data-name="Group 3490">
                                                                <path  data-name="Path 4187" d="M89.588,11.493h0c.013.013.028.026.042.038l.021.016.025.018.025.015.023.014.027.013.025.012.026.01.028.01.026.007.029.007.031,0,.026,0a.587.587,0,0,0,.115,0l.026,0,.031,0,.029-.007.026-.007.028-.01.026-.01.025-.012.027-.013.023-.014.025-.015.025-.018.021-.016q.022-.018.042-.038h0L94.5,7.41a.583.583,0,0,0-.825-.825L90.584,9.672V.583a.583.583,0,0,0-1.167,0V9.672L86.33,6.586a.583.583,0,0,0-.825.825Z" transform="translate(-85.334)" fill="#fd4949"/>
                                                                <path  data-name="Path 4188" d="M94.084,469.333H85.917a.584.584,0,0,0,0,1.168h8.167a.584.584,0,0,0,0-1.168Z" transform="translate(-85.334 -456.501)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <h4 class="font_16 f_w_700 ">20211118-18162446</h4>
                                                <p class="font_14 f_w_500 mb-0 lh-1">14 Jan, 2022</p>
                                            </div>
                                        
                                        </td>
                                        <td>
                                            <h4 class="font_16 f_w_500 m-0 ">$240.00</h4>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn text-nowrap">Order Place</a>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn style4 text-nowrap">Paid</a>
                                        </td>
                                        <td>
                                            <div class="amazy_status_btns d-flex gap_5 align-items-center">
                                                <button class="amazy_status_btn" data-bs-toggle="modal" data-bs-target="#purchase_history_modal">
                                                    <svg  xmlns="http://www.w3.org/2000/svg" width="16" height="11.5" viewBox="0 0 16 11.5">
                                                        <path  data-name="Path 4189" d="M15.333,124.168H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -117.668)" fill="#fd4949"/>
                                                        <path  data-name="Path 4190" d="M15.333,1.5H.667A.712.712,0,0,1,0,.75.712.712,0,0,1,.667,0H15.333A.712.712,0,0,1,16,.75.712.712,0,0,1,15.333,1.5Zm0,0" fill="#fd4949"/>
                                                        <path  data-name="Path 4191" d="M15.333,246.832H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -235.332)" fill="#fd4949"/>
                                                    </svg>
                                                </button>
                                                <button class="amazy_status_btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.333" height="14" viewBox="0 0 9.333 14">
                                                        <g  data-name="download (1)" transform="translate(-85.334 0)">
                                                            <g  data-name="Group 3491" transform="translate(85.334 0)">
                                                            <g  data-name="Group 3490">
                                                                <path  data-name="Path 4187" d="M89.588,11.493h0c.013.013.028.026.042.038l.021.016.025.018.025.015.023.014.027.013.025.012.026.01.028.01.026.007.029.007.031,0,.026,0a.587.587,0,0,0,.115,0l.026,0,.031,0,.029-.007.026-.007.028-.01.026-.01.025-.012.027-.013.023-.014.025-.015.025-.018.021-.016q.022-.018.042-.038h0L94.5,7.41a.583.583,0,0,0-.825-.825L90.584,9.672V.583a.583.583,0,0,0-1.167,0V9.672L86.33,6.586a.583.583,0,0,0-.825.825Z" transform="translate(-85.334)" fill="#fd4949"/>
                                                                <path  data-name="Path 4188" d="M94.084,469.333H85.917a.584.584,0,0,0,0,1.168h8.167a.584.584,0,0,0,0-1.168Z" transform="translate(-85.334 -456.501)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <h4 class="font_16 f_w_700 ">20211118-18162446</h4>
                                                <p class="font_14 f_w_500 mb-0 lh-1">14 Jan, 2022</p>
                                            </div>
                                        
                                        </td>
                                        <td>
                                            <h4 class="font_16 f_w_500 m-0 ">$240.00</h4>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn text-nowrap">Order Place</a>
                                        </td>
                                        <td>
                                            <a href="#" class="table_badge_btn style4 text-nowrap">Paid</a>
                                        </td>
                                        <td>
                                            <div class="amazy_status_btns d-flex gap_5 align-items-center">
                                                <button class="amazy_status_btn" data-bs-toggle="modal" data-bs-target="#purchase_history_modal">
                                                    <svg  xmlns="http://www.w3.org/2000/svg" width="16" height="11.5" viewBox="0 0 16 11.5">
                                                        <path  data-name="Path 4189" d="M15.333,124.168H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -117.668)" fill="#fd4949"/>
                                                        <path  data-name="Path 4190" d="M15.333,1.5H.667A.712.712,0,0,1,0,.75.712.712,0,0,1,.667,0H15.333A.712.712,0,0,1,16,.75.712.712,0,0,1,15.333,1.5Zm0,0" fill="#fd4949"/>
                                                        <path  data-name="Path 4191" d="M15.333,246.832H.667a.755.755,0,0,1,0-1.5H15.333a.755.755,0,0,1,0,1.5Zm0,0" transform="translate(0 -235.332)" fill="#fd4949"/>
                                                    </svg>
                                                </button>
                                                <button class="amazy_status_btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.333" height="14" viewBox="0 0 9.333 14">
                                                        <g  data-name="download (1)" transform="translate(-85.334 0)">
                                                            <g  data-name="Group 3491" transform="translate(85.334 0)">
                                                            <g  data-name="Group 3490">
                                                                <path  data-name="Path 4187" d="M89.588,11.493h0c.013.013.028.026.042.038l.021.016.025.018.025.015.023.014.027.013.025.012.026.01.028.01.026.007.029.007.031,0,.026,0a.587.587,0,0,0,.115,0l.026,0,.031,0,.029-.007.026-.007.028-.01.026-.01.025-.012.027-.013.023-.014.025-.015.025-.018.021-.016q.022-.018.042-.038h0L94.5,7.41a.583.583,0,0,0-.825-.825L90.584,9.672V.583a.583.583,0,0,0-1.167,0V9.672L86.33,6.586a.583.583,0,0,0-.825.825Z" transform="translate(-85.334)" fill="#fd4949"/>
                                                                <path  data-name="Path 4188" d="M94.084,469.333H85.917a.584.584,0,0,0,0,1.168h8.167a.584.584,0,0,0,0-1.168Z" transform="translate(-85.334 -456.501)" fill="#fd4949"/>
                                                            </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="amaz_pagination d-flex align-items-center justify-content-start">
                            <a class="arrow_btns d-inline-flex align-items-center justify-content-center ms-0" href="#">
                                <i class="fas fa-chevron-left"></i>
                                <span>Prev</span>
                            </a>
                            <a class="page_counter active" href="#">1</a>
                            <a class="page_counter" href="#">2</a>
                            <a class="page_counter" href="#">3</a>
                            <a class="page_counter_dot"  href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="3" viewBox="0 0 15 3">
                                    <g id="dot" transform="translate(-998 -3958)">
                                        <circle id="Ellipse_92" data-name="Ellipse 92" cx="1.5" cy="1.5" r="1.5" transform="translate(998 3958)" fill="#00124e"/>
                                        <circle id="Ellipse_93" data-name="Ellipse 93" cx="1.5" cy="1.5" r="1.5" transform="translate(1004 3958)" fill="#00124e"/>
                                        <circle id="Ellipse_94" data-name="Ellipse 94" cx="1.5" cy="1.5" r="1.5" transform="translate(1010 3958)" fill="#00124e"/>
                                    </g>
                                </svg>
                            </a>
                            <a class="page_counter" href="#">8</a>
                            <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                                <span>Next</span>
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>