<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<div class="order_confirm_banner section_spacing6">
    <div class="container">
        <div class="row justify-content-center ">
            <div class="col-lg-10 col-xl-8">
                <div class="order_conrim_banner text-center">
                    <img class="img-fluid mb_20" src="img/banner/order_confirm_img.png" alt="">
                    <h3 class="font_30 f_w_700 mb-1">Thank you for your Purchase!</h3>
                    <p class="font_16 f_w_500 m-0">Your order number is order- <span class="priamry_text">3905-210610113613</span></p>
                </div>
            </div>
            <div class="col-lg-10 col-xl-8">
                <div class="order_confirm_table_wrap mb_30">
                    <h4 class="font_20 f_w_700 lh-1 mb_18 ">Order Details</h4>
                    <div class="table-responsive">
                        <table class="table custom_table4 mb_20">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Prise</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="product_td">
                                        <a href="#" class="product_name d-flex align-items-center flex-wrap">
                                            <div class="thumb">
                                                <img class="img-fluid" src="img/cart/order_details_1.png" alt="">
                                            </div>
                                            <span class="product_text secondary_text font_16 f_w_500 "> Sony Vlog Camera ZV1 full box</span>
                                            <span class="product_quentity font_16 f_w_500 mute_text">x 2</span>
                                        </a>
                                    </td>
                                    <td class="product_td">
                                        <h4 class="font_16 f_w_700 m-0 priamry_text">$143.00</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="product_td">
                                        <a href="#" class="product_name d-flex align-items-center flex-wrap">
                                            <div class="thumb">
                                                <img class="img-fluid" src="img/cart/order_details_1.png" alt="">
                                            </div>
                                            <span class="product_text secondary_text font_16 f_w_500 "> Sony Vlog Camera ZV1 full box</span>
                                            <span class="product_quentity font_16 f_w_500 mute_text">x 2</span>
                                        </a>
                                    </td>
                                    <td class="product_td">
                                        <h4 class="font_16 f_w_700 m-0 priamry_text">$143.00</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_gray_bg">
                                        <h4 class="font_16 f_w_500 m-0 text-end">Subtotal</h4>
                                    </td>
                                    <td class="td_gray_bg">
                                        <h4 class="font_16 f_w_500 m-0">$1243.00</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4 class="font_16 f_w_500 m-0 text-end">Delivery Fee</h4>
                                    </td>
                                    <td>
                                        <h4 class="font_16 f_w_500 m-0">$1243.00</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_gray_bg">
                                        <h4 class="font_16 f_w_500 m-0 text-end">Discount</h4>
                                    </td>
                                    <td class="td_gray_bg">
                                        <h4 class="font_16 f_w_500 m-0">$1243.00</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4 class="font_16 f_w_500 m-0 text-end">Total (Incl. VAT)</h4>
                                    </td>
                                    <td>
                                        <h4 class="font_16 f_w_500 m-0">$1243.00</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="product_footer" colspan="2">
                                        <div class="view_order_info d-flex align-items-center gap_15">
                                            <div class="order_info flex-fill">
                                                <span class="font_14 f_w_400 m-0 mute_text">For more details , track your delivery Status order </span>
                                                <span class="font_16 f_w_700 mb-0 priamry_text "> My Acount > My Order</span>
                                            </div>
                                            <a href="#" class="amaz_primary_btn style2">view orders</a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="order_confirm_email d-flex flex-wrap align-items-center gap_15">
                        <svg xmlns="http://www.w3.org/2000/svg" width="26.297" height="20.012" viewBox="0 0 26.297 20.012">
                            <g id="envelope" transform="translate(0 -57.75)">
                                <g id="Group_1714" data-name="Group 1714" transform="translate(0 57.75)">
                                <path id="Path_2080" data-name="Path 2080" d="M23.087,57.75H3.216A3.218,3.218,0,0,0,0,60.966V74.547a3.218,3.218,0,0,0,3.216,3.216H23.081A3.218,3.218,0,0,0,26.3,74.547V60.971A3.215,3.215,0,0,0,23.087,57.75Zm1.747,16.8a1.749,1.749,0,0,1-1.747,1.747H3.216a1.749,1.749,0,0,1-1.747-1.747V60.971a1.749,1.749,0,0,1,1.747-1.747H23.081a1.749,1.749,0,0,1,1.747,1.747V74.547Z" transform="translate(0 -57.75)" fill="#fd4949"/>
                                <path id="Path_2081" data-name="Path 2081" d="M69.3,115.957l6.431-5.768a.736.736,0,1,0-.985-1.094l-8.869,7.96-1.73-1.545c-.005-.005-.011-.011-.011-.016a1.087,1.087,0,0,0-.12-.1l-7.041-6.3a.736.736,0,1,0-.979,1.1l6.508,5.817-6.48,6.067a.738.738,0,0,0-.033,1.039.752.752,0,0,0,.539.234.736.736,0,0,0,.5-.2L63.612,117,65.4,118.59a.733.733,0,0,0,.979-.005l1.834-1.643,6.54,6.214a.736.736,0,0,0,1.039-.027.737.737,0,0,0-.027-1.039Z" transform="translate(-52.719 -106.119)" fill="#fd4949"/>
                                </g>
                            </g>
                        </svg>
                        <p class="m-0">We have a confirmation email to <a href="#">support@spondonit.com</a> with the order details</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>