<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- brand_banner::start  -->
<div class="brand_banner">
    <img src="img/banner/brand_banner.jpg" alt="brand_banner" class="img-fluid w-100">
</div>
<!-- brand_banner::end  -->

<div class="new_user_section section_spacing7 ">
    <div class="container">
        <!-- content ::start -->
        <div class="row">
            <div class="col-12">
                <div class="section__title d-flex align-items-center gap_30 mb_30 flex-wrap">
                    <h3 class="m-0 ">More To Love</h3>
                    <div class="nav new_portfolio_tab " id="nav-tab1" role="tablist">
                        <button class="nav-link active" id="nav-All-tab" data-bs-toggle="tab" data-bs-target="#nav-All" type="button" role="tab" aria-controls="nav-All" aria-selected="true">All</button>
                        <button class="nav-link" id="nav-Shoes-tab" data-bs-toggle="tab" data-bs-target="#nav-Shoes" type="button" role="tab" aria-controls="nav-Shoes" aria-selected="false">Women Shoes</button>
                        <button class="nav-link" id="nav-Grocery-tab" data-bs-toggle="tab" data-bs-target="#nav-Grocery" type="button" role="tab" aria-controls="nav-Grocery" aria-selected="false">Grocery</button>
                        <button class="nav-link" id="nav-Electronics-tab" data-bs-toggle="tab" data-bs-target="#nav-Electronics" type="button" role="tab" aria-controls="nav-Electronics" aria-selected="false">Electronics</button>
                        <button class="nav-link" id="nav-Gadget-tab" data-bs-toggle="tab" data-bs-target="#nav-Gadget" type="button" role="tab" aria-controls="nav-Gadget" aria-selected="false">Gadget</button>
                        <button class="nav-link" id="nav-Fashion-tab" data-bs-toggle="tab" data-bs-target="#nav-Fashion" type="button" role="tab" aria-controls="nav-Fashion" aria-selected="false">Men Fashion</button>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="tab-content" id="nav-tabContent1">
                    <div class="tab-pane fade show active" id="nav-All" role="tabpanel" aria-labelledby="nav-All-tab">
                        <!-- content:start  -->
                        <div class="row ">
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content:end  -->
                    </div>
                    <div class="tab-pane fade" id="nav-Shoes" role="tabpanel" aria-labelledby="nav-Shoes-tab">
                        <!-- content:start  -->
                        <div class="row ">
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content:end  -->
                    </div>
                    <div class="tab-pane fade" id="nav-Grocery" role="tabpanel" aria-labelledby="nav-Grocery-tab">
                        <!-- content:start  -->
                        <div class="row ">
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content:end  -->
                    </div>
                    <div class="tab-pane fade" id="nav-Electronics" role="tabpanel" aria-labelledby="nav-Electronics-tab">
                        <!-- content:start  -->
                        <div class="row ">
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content:end  -->
                    </div>
                    <div class="tab-pane fade" id="nav-Gadget" role="tabpanel" aria-labelledby="nav-Gadget-tab">
                        <!-- content:start  -->
                        <div class="row ">
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content:end  -->
                    </div>
                    <div class="tab-pane fade" id="nav-Fashion" role="tabpanel" aria-labelledby="nav-Fashion-tab">
                        <!-- content:start  -->
                        <div class="row ">
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="#" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content:end  -->
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="amaz_pagination d-flex align-items-center mb_30">
                    <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                        <i class="fas fa-chevron-left"></i>
                        <span>Prev</span>
                    </a>
                    <a class="page_counter active" href="#">1</a>
                    <a class="page_counter" href="#">2</a>
                    <a class="page_counter" href="#">3</a>
                    <a href="#">...</a>
                    <a class="page_counter" href="#">8</a>
                    <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                        <span>Next</span>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- content ::end -->
    </div>
</div>
<!-- new_user_section::end  -->

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>