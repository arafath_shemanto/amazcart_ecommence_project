<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<div class="amazy_dashboard_area dashboard_bg section_spacing6">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <?php include 'include/dashboard_sidebar.php' ?>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="dashboard_white_box style2 bg-white mb_25">
                    <div class="dashboard_white_box_header d-flex align-items-center">
                        <h4 class="font_24 f_w_700 mb_20">My Wallet</h4>
                    </div>

                    <div class="dashboard_wallet_boxes mb_40">
                        <div class="singl_dashboard_wallet green_box d-flex align-items-center justify-content-center flex-column">
                            <h4 class="font_16 f_w_400 lh-1">Wallet Balance</h4>
                            <h3 class="f_w_700 m-0 lh-1">$4328.32</h3>
                        </div>
                        <div class="singl_dashboard_wallet pink_box d-flex align-items-center justify-content-center flex-column">
                            <h4 class="font_16 f_w_400 lh-1">Wallet Balance</h4>
                            <h3 class="f_w_700 m-0 lh-1">$4328.32</h3>
                        </div>
                        <div  data-bs-toggle="modal" data-bs-target="#recharge_wallet" class="singl_dashboard_wallet bordered d-flex align-items-center justify-content-center flex-column gj-cursor-pointer ">
                            <h4 class="font_16 f_w_400 lh-1 mb_10 mute_text">Recharge Wallet</h4>
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                <path id="plus_1_" data-name="plus (1)" d="M12.5,0A12.5,12.5,0,1,0,25,12.5,12.514,12.514,0,0,0,12.5,0Zm0,23.437A10.937,10.937,0,1,1,23.438,12.5,10.95,10.95,0,0,1,12.5,23.438ZM19.435,12.5a.781.781,0,0,1-.781.781H13.282v5.371a.781.781,0,0,1-1.563,0V13.282H6.349a.781.781,0,1,1,0-1.563H11.72V6.349a.781.781,0,1,1,1.563,0V11.72h5.371A.781.781,0,0,1,19.435,12.5Z" transform="translate(-0.001 -0.001)" fill="#687083"/>
                            </svg>
                        </div>
                    </div>

                    <div class="dashboard_white_box_header d-flex align-items-center">
                        <h4 class="font_20 f_w_700 mb_20">Wallet Recharge History</h4>
                    </div>
                    <div class="dashboard_white_box_body">
                        <div class="table_border_whiteBox mb_30">
                            <div class="table-responsive">
                                <table class="table amazy_table style3 mb-0">
                                    <thead>
                                        <tr>
                                        <th class="font_14 f_w_700 priamry_text" scope="col">Date</th>
                                        <th class="font_14 f_w_700 priamry_text border-start-0 border-end-0" scope="col">TXN ID</th>
                                        <th class="font_14 f_w_700 priamry_text border-start-0 border-end-0" scope="col">Amount</th>
                                        <th class="font_14 f_w_700 priamry_text border-start-0 border-end-0" scope="col">Type</th>
                                        <th class="font_14 f_w_700 priamry_text border-start-0 border-end-0" scope="col">Payment Method</th>
                                        <th class="font_14 f_w_700 priamry_text" scope="col">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">14 Jan, 2022</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">FDR3EA$$12</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">$2340.00</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">Deposit</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">SSLcommerz</span>
                                            </td>
                                            <td>
                                                <a href="#" class="table_badge_btn style4 text-nowrap">Approved</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">14 Jan, 2022</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">FDR3EA$$12</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">$2340.00</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">Deposit</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">SSLcommerz</span>
                                            </td>
                                            <td>
                                                <a href="#" class="table_badge_btn style4 text-nowrap">Approved</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">14 Jan, 2022</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">FDR3EA$$12</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">$2340.00</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">Deposit</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">SSLcommerz</span>
                                            </td>
                                            <td>
                                                <a href="#" class="table_badge_btn style4 text-nowrap">Approved</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">14 Jan, 2022</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">FDR3EA$$12</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">$2340.00</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">Deposit</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">SSLcommerz</span>
                                            </td>
                                            <td>
                                                <a href="#" class="table_badge_btn style4 text-nowrap">Approved</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">14 Jan, 2022</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">FDR3EA$$12</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">$2340.00</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">Deposit</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">SSLcommerz</span>
                                            </td>
                                            <td>
                                                <a href="#" class="table_badge_btn style4 text-nowrap">Approved</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">14 Jan, 2022</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">FDR3EA$$12</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">$2340.00</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">Deposit</span>
                                            </td>
                                            <td>
                                                <span class="font_14 f_w_500 mute_text">SSLcommerz</span>
                                            </td>
                                            <td>
                                                <a href="#" class="table_badge_btn style4 text-nowrap">Approved</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="amaz_pagination d-flex align-items-center justify-content-center mb_10">
                            <a class="arrow_btns d-inline-flex align-items-center justify-content-center ms-0" href="#">
                                <i class="fas fa-chevron-left"></i>
                                <span>Prev</span>
                            </a>
                            <a class="page_counter active" href="#">1</a>
                            <a class="page_counter" href="#">2</a>
                            <a class="page_counter" href="#">3</a>
                            <a class="page_counter_dot" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="3" viewBox="0 0 15 3">
                                    <g id="dot" transform="translate(-998 -3958)">
                                        <circle id="Ellipse_92" data-name="Ellipse 92" cx="1.5" cy="1.5" r="1.5" transform="translate(998 3958)" fill="#00124e"></circle>
                                        <circle id="Ellipse_93" data-name="Ellipse 93" cx="1.5" cy="1.5" r="1.5" transform="translate(1004 3958)" fill="#00124e"></circle>
                                        <circle id="Ellipse_94" data-name="Ellipse 94" cx="1.5" cy="1.5" r="1.5" transform="translate(1010 3958)" fill="#00124e"></circle>
                                    </g>
                                </svg>
                            </a>
                            <a class="page_counter" href="#">8</a>
                            <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                                <span>Next</span>
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>