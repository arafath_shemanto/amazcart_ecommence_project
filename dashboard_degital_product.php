<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<div class="amazy_dashboard_area dashboard_bg section_spacing6">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <?php include 'include/dashboard_sidebar.php' ?>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="dashboard_white_box style2 bg-white mb_25">
                    <div class="d-flex align-items-center gap_20 mb_30">
                        <h5 class="font_20 f_w_700 flex-fill m-0">Digital Products</h5>
                        <div class="wish_selects d-flex align-items-center gap_10 flex-wrap">
                            <select class="amaz_select4">
                                <option value="#">Show 20 item’s</option>
                                <option value="#">Show 20 item’s</option>
                                <option value="#">Show 20 item’s</option>
                                <option value="#">Show 20 item’s</option>
                            </select>
                            <select class="amaz_select4">
                                <option value="#">Default Shorting</option>
                                <option value="#">Default Shorting</option>
                                <option value="#">Default Shorting</option>
                                <option value="#">Default Shorting</option>
                            </select>
                        </div>
                    </div>
                    <div class="dashboard_wishlist_grid mb_40">
                        <div class="product_widget5 style3 bg-white">
                            <div class="product_thumb_upper">
                                <a href="product_details.php" class="thumb">
                                    <img src="img/wishListProducts/1.png" alt="">
                                </a>
                                <div class="product_action">
                                    <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                    <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-star"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product__meta text-center">
                                <span class="product_banding ">Studio Design</span>
                                <a href="product_details.php">
                                    <h4>Support Wifi Projector</h4>
                                </a>
                                <div class="stars justify-content-center">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <div class="product_prise">
                                    <p>$20.00</p>
                                    <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="product_widget5 style3 bg-white">
                            <div class="product_thumb_upper">
                                <a href="product_details.php" class="thumb">
                                    <img src="img/wishListProducts/2.png" alt="">
                                </a>
                                <div class="product_action">
                                    <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                    <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-star"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product__meta text-center">
                                <span class="product_banding ">Studio Design</span>
                                <a href="product_details.php">
                                    <h4>Support Wifi Projector</h4>
                                </a>
                                <div class="stars justify-content-center">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <div class="product_prise">
                                    <p>$20.00</p>
                                    <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="product_widget5 style3 bg-white">
                            <div class="product_thumb_upper">
                                <a href="product_details.php" class="thumb">
                                <img src="img/wishListProducts/3.png" alt="">
                                </a>
                                <div class="product_action">
                                    <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                    <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-star"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product__meta text-center">
                                <span class="product_banding ">Studio Design</span>
                                <a href="product_details.php">
                                    <h4>Support Wifi Projector</h4>
                                </a>
                                <div class="stars justify-content-center">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <div class="product_prise">
                                    <p>$20.00</p>
                                    <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="product_widget5 style3 bg-white">
                            <div class="product_thumb_upper">
                                <a href="product_details.php" class="thumb">
                                    <img src="img/wishListProducts/4.png" alt="">
                                </a>
                                <div class="product_action">
                                    <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                    <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-star"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product__meta text-center">
                                <span class="product_banding ">Studio Design</span>
                                <a href="product_details.php">
                                    <h4>Support Wifi Projector</h4>
                                </a>
                                <div class="stars justify-content-center">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <div class="product_prise">
                                    <p>$20.00</p>
                                    <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="product_widget5 style3 bg-white">
                            <div class="product_thumb_upper">
                                <a href="product_details.php" class="thumb">
                                    <img src="img/wishListProducts/5.png" alt="">
                                </a>
                                <div class="product_action">
                                    <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                    <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-star"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product__meta text-center">
                                <span class="product_banding ">Studio Design</span>
                                <a href="product_details.php">
                                    <h4>Support Wifi Projector</h4>
                                </a>
                                <div class="stars justify-content-center">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <div class="product_prise">
                                    <p>$20.00</p>
                                    <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="product_widget5 style3 bg-white">
                            <div class="product_thumb_upper">
                                <a href="product_details.php" class="thumb">
                                <img src="img/wishListProducts/6.png" alt="">
                                </a>
                                <div class="product_action">
                                    <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                    <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-star"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product__meta text-center">
                                <span class="product_banding ">Studio Design</span>
                                <a href="product_details.php">
                                    <h4>Support Wifi Projector</h4>
                                </a>
                                <div class="stars justify-content-center">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <div class="product_prise">
                                    <p>$20.00</p>
                                    <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="product_widget5 style3 bg-white">
                            <div class="product_thumb_upper">
                                <a href="product_details.php" class="thumb">
                                <img src="img/wishListProducts/7.png" alt="">
                                </a>
                                <div class="product_action">
                                    <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                    <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-star"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product__meta text-center">
                                <span class="product_banding ">Studio Design</span>
                                <a href="product_details.php">
                                    <h4>Support Wifi Projector</h4>
                                </a>
                                <div class="stars justify-content-center">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <div class="product_prise">
                                    <p>$20.00</p>
                                    <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="product_widget5 style3 bg-white">
                            <div class="product_thumb_upper">
                                <a href="product_details.php" class="thumb">
                                <img src="img/wishListProducts/8.png" alt="">
                                </a>
                                <div class="product_action">
                                    <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                    <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-star"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product__meta text-center">
                                <span class="product_banding ">Studio Design</span>
                                <a href="product_details.php">
                                    <h4>Support Wifi Projector</h4>
                                </a>
                                <div class="stars justify-content-center">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <div class="product_prise">
                                    <p>$20.00</p>
                                    <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="amaz_pagination d-flex align-items-center justify-content-center mb_10">
                        <a class="arrow_btns d-inline-flex align-items-center justify-content-center ms-0" href="#">
                            <i class="fas fa-chevron-left"></i>
                            <span>Prev</span>
                        </a>
                        <a class="page_counter active" href="#">1</a>
                        <a class="page_counter" href="#">2</a>
                        <a class="page_counter" href="#">3</a>
                        <a class="page_counter_dot" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="3" viewBox="0 0 15 3">
                                <g id="dot" transform="translate(-998 -3958)">
                                    <circle id="Ellipse_92" data-name="Ellipse 92" cx="1.5" cy="1.5" r="1.5" transform="translate(998 3958)" fill="#00124e"></circle>
                                    <circle id="Ellipse_93" data-name="Ellipse 93" cx="1.5" cy="1.5" r="1.5" transform="translate(1004 3958)" fill="#00124e"></circle>
                                    <circle id="Ellipse_94" data-name="Ellipse 94" cx="1.5" cy="1.5" r="1.5" transform="translate(1010 3958)" fill="#00124e"></circle>
                                </g>
                            </svg>
                        </a>
                        <a class="page_counter" href="#">8</a>
                        <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                            <span>Next</span>
                            <i class="fas fa-chevron-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>