<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<div class="breadcrumb_area bradcam_bg_1">
    <div class="container-fluid">
        <div class="breadcrumb_iner ">
            <div class="bradcam_text">
                <div class="row justify-content-center">
                    <div class="col-lg-12 text-center">
                        <h3>Hi, welcome to Amazcart!</h3>
                        <h2 class="m-0">New User Gifts – Choose one!</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- new_user_section::start  -->
<div class="new_user_zone_wrap">
    <ul class="nav new_user_tabs position_user_tab  justify-content-center" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">$US 240</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Exclusive Price</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Cupons</button>
        </li>
    </ul>
</div>
<div class="new_user_section section_spacing7 ">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade  " id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <!-- content ::start -->
                        <div class="row">
                            <div class="col-12">
                                <div class="section__title d-flex align-items-center gap-3 mb_30 flex-wrap">
                                    <h3 class="m-0 flex-fill">Hot Picks For You</h3>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_25">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1" > -20% </span>
                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="amaz_pagination d-flex align-items-center mb_30">
                                    <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                                        <i class="fas fa-chevron-left"></i>
                                        <span>Prev</span>
                                    </a>
                                    <a class="page_counter active" href="#">1</a>
                                    <a class="page_counter" href="#">2</a>
                                    <a class="page_counter" href="#">3</a>
                                    <a href="#">...</a>
                                    <a class="page_counter" href="#">8</a>
                                    <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                                        <span>Next</span>
                                        <i class="fas fa-chevron-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- content ::end -->
                    </div>
                    <div class="tab-pane fade " id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <!-- content ::start -->
                        <div class="row">
                            <div class="col-12">
                                <div class="section__title d-flex align-items-center gap_30 mb_30 flex-wrap">
                                    <h3 class="m-0 ">More To Love</h3>
                                    <div class="nav new_portfolio_tab " id="nav-tab1" role="tablist">
                                        <button class="nav-link active" id="nav-All-tab" data-bs-toggle="tab" data-bs-target="#nav-All" type="button" role="tab" aria-controls="nav-All" aria-selected="true">All</button>
                                        <button class="nav-link" id="nav-Shoes-tab" data-bs-toggle="tab" data-bs-target="#nav-Shoes" type="button" role="tab" aria-controls="nav-Shoes" aria-selected="false">Women Shoes</button>
                                        <button class="nav-link" id="nav-Grocery-tab" data-bs-toggle="tab" data-bs-target="#nav-Grocery" type="button" role="tab" aria-controls="nav-Grocery" aria-selected="false">Grocery</button>
                                        <button class="nav-link" id="nav-Electronics-tab" data-bs-toggle="tab" data-bs-target="#nav-Electronics" type="button" role="tab" aria-controls="nav-Electronics" aria-selected="false">Electronics</button>
                                        <button class="nav-link" id="nav-Gadget-tab" data-bs-toggle="tab" data-bs-target="#nav-Gadget" type="button" role="tab" aria-controls="nav-Gadget" aria-selected="false">Gadget</button>
                                        <button class="nav-link" id="nav-Fashion-tab" data-bs-toggle="tab" data-bs-target="#nav-Fashion" type="button" role="tab" aria-controls="nav-Fashion" aria-selected="false">Men Fashion</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="tab-content" id="nav-tabContent1">
                                    <div class="tab-pane fade show active" id="nav-All" role="tabpanel" aria-labelledby="nav-All-tab">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                    <div class="tab-pane fade" id="nav-Shoes" role="tabpanel" aria-labelledby="nav-Shoes-tab">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                    <div class="tab-pane fade" id="nav-Grocery" role="tabpanel" aria-labelledby="nav-Grocery-tab">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                    <div class="tab-pane fade" id="nav-Electronics" role="tabpanel" aria-labelledby="nav-Electronics-tab">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                    <div class="tab-pane fade" id="nav-Gadget" role="tabpanel" aria-labelledby="nav-Gadget-tab">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                    <div class="tab-pane fade" id="nav-Fashion" role="tabpanel" aria-labelledby="nav-Fashion-tab">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="amaz_pagination d-flex align-items-center mb_30">
                                    <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                                        <i class="fas fa-chevron-left"></i>
                                        <span>Prev</span>
                                    </a>
                                    <a class="page_counter active" href="#">1</a>
                                    <a class="page_counter" href="#">2</a>
                                    <a class="page_counter" href="#">3</a>
                                    <a href="#">...</a>
                                    <a class="page_counter" href="#">8</a>
                                    <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                                        <span>Next</span>
                                        <i class="fas fa-chevron-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- content ::end -->
                    </div>
                    <div class="tab-pane fade show active" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <!-- content ::start -->
                        <div class="row">
                            <div class="col-12">
                                <div class="coupon_gift_box">
                                    <div class="coupon_gift_box_left flex-fill">
                                        <img class="img-fluid" src="img/amazCat/new_user_coupon.png" alt="">
                                    </div>
                                    <div class="coupon_gift_box_right d-flex align-items-center justify-content-center">
                                        <a class="amaz_primary_btn style5" href="#">Get now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="section__title d-flex align-items-center gap_30 mb_30 flex-wrap">
                                    <h3 class="m-0 ">More To Love</h3>
                                    <div class="nav new_portfolio_tab " id="nav-tab2" role="tablist">
                                        <button class="nav-link active" id="nav-All-tab1" data-bs-toggle="tab" data-bs-target="#nav-All1" type="button" role="tab" aria-controls="nav-All1" aria-selected="true">All</button>
                                        <button class="nav-link" id="nav-Shoes-tab1" data-bs-toggle="tab" data-bs-target="#nav-Shoes1" type="button" role="tab" aria-controls="nav-Shoes1" aria-selected="false">Women Shoes</button>
                                        <button class="nav-link" id="nav-Grocery-tab1" data-bs-toggle="tab" data-bs-target="#nav-Grocery1" type="button" role="tab" aria-controls="nav-Grocery1" aria-selected="false">Grocery</button>
                                        <button class="nav-link" id="nav-Electronics-tab1" data-bs-toggle="tab" data-bs-target="#nav-Electronics1" type="button" role="tab" aria-controls="nav-Electronics1" aria-selected="false">Electronics</button>
                                        <button class="nav-link" id="nav-Gadget-tab1" data-bs-toggle="tab" data-bs-target="#nav-Gadget1" type="button" role="tab" aria-controls="nav-Gadget1" aria-selected="false">Gadget</button>
                                        <button class="nav-link" id="nav-Fashion-tab1" data-bs-toggle="tab" data-bs-target="#nav-Fashion1" type="button" role="tab" aria-controls="nav-Fashion1" aria-selected="false">Men Fashion</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="tab-content" id="nav-tabContent2">
                                    <div class="tab-pane fade show active" id="nav-All1" role="tabpanel" aria-labelledby="nav-All-tab1">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                    <div class="tab-pane fade" id="nav-Shoes1" role="tabpanel" aria-labelledby="nav-Shoes-tab1">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                    <div class="tab-pane fade" id="nav-Grocery1" role="tabpanel" aria-labelledby="nav-Grocery-tab1">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                    <div class="tab-pane fade" id="nav-Electronics1" role="tabpanel" aria-labelledby="nav-Electronics-tab1">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                    <div class="tab-pane fade" id="nav-Gadget1" role="tabpanel" aria-labelledby="nav-Gadget-tab1">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                    <div class="tab-pane fade" id="nav-Fashion1" role="tabpanel" aria-labelledby="nav-Fashion-tab1">
                                        <!-- content:start  -->
                                        <div class="row ">
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/1.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/2.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/3.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-4 col-md-6">
                                                <div class="product_widget5 mb_25">
                                                    <div class="product_thumb_upper">
                                                        <a href="product_details.php" class="thumb">
                                                            <img src="img/amazPorduct/4.png" alt="">
                                                        </a>
                                                        <div class="product_action">
                                                            <a href="#">
                                                                <i class="ti-control-shuffle"></i>
                                                            </a>
                                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                                <i class="ti-eye"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="ti-star"></i>
                                                            </a>
                                                        </div>
                                                        <span class="badge_1" > -20% </span>
                                                        <span class="badge_1 style2 text-uppercase" > New </span>
                                                    </div>
                                                    <div class="product__meta text-center">
                                                        <span class="product_banding ">Studio Design</span>
                                                        <a href="product_details.php">
                                                            <h4>Designs Woolrich Kletter</h4>
                                                        </a>
                                                        <div class="stars justify-content-center">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="product_prise">
                                                            <p><span>$29.00 </span>  $20.00</p>
                                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- content:end  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="amaz_pagination d-flex align-items-center mb_30">
                                    <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                                        <i class="fas fa-chevron-left"></i>
                                        <span>Prev</span>
                                    </a>
                                    <a class="page_counter active" href="#">1</a>
                                    <a class="page_counter" href="#">2</a>
                                    <a class="page_counter" href="#">3</a>
                                    <a href="#">...</a>
                                    <a class="page_counter" href="#">8</a>
                                    <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                                        <span>Next</span>
                                        <i class="fas fa-chevron-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- content ::end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- new_user_section::end  -->

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>