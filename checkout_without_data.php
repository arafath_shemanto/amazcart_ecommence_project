<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- checkout_wrapper_area::start   -->
<div class="checkout_wrapper_area section_spacing5">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-xl-11">
                <div class="checkout_wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="checkout_return_text mb_35">
                                <i class="ti-folder"></i>
                                <p> Returning customer? <a data-bs-toggle="modal" data-bs-target="#checkot_login_form" href="#">Click here to login</a></p>
                            </div>
                        </div>
                        <div class="col-xl-7">
                            <div class="billing_details_wrapper mb_30">
                                <div class="billing_details_box">
                                    <div class="billing_details_single">
                                        <div class="billing_details_box_head d-flex align-items-center gap-2 flex-wrap">
                                            <h4 class="flex-fill font_18 f_w_700 m-0">Billing Details</h4>
                                            <a href="#" class="amaz_primary_btn3 round_style f_w_600 min_140 text-center text-uppercase justify-content-center">+ Add address</a>
                                        </div>
                                        <div class="billing_details_box_body">
                                            <div class="billing_details_box_body_inner d-flex ">
                                                <div class="without_address_notify">
                                                    No Data Found
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="billing_details_single">
                                        <div class="billing_details_box_head d-flex align-items-center gap-2 flex-wrap">
                                            <h4 class="flex-fill font_18 f_w_700 m-0">Shipping Details</h4>
                                            <a href="#" class="amaz_primary_btn3 round_style f_w_600 min_140 text-center text-uppercase justify-content-center">+ Add address</a>
                                        </div>
                                        <div class="billing_details_box_body">
                                            <div class="billing_details_box_body_inner d-flex ">
                                                <div class="without_address_notify">
                                                    No Data Found
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="billing_details_single">
                                        <div class="billing_details_box_head d-flex align-items-center gap-2 flex-wrap">
                                            <h4 class="flex-fill font_18 f_w_700 m-0">Contact Number</h4>
                                            <a href="#" class="amaz_primary_btn3 round_style f_w_600 min_140 text-center text-uppercase justify-content-center">+ Add number</a>
                                        </div>
                                        <div class="billing_details_box_body">
                                            <div class="billing_details_box_body_inner d-flex ">
                                            <div class="without_address_notify">
                                                    No Data Found
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="billing_details_single">
                                        <div class="billing_details_box_head d-flex align-items-center gap-2 flex-wrap mb_30">
                                            <h4 class="flex-fill font_18 f_w_700 m-0">Payment Option</h4>
                                            <a href="#" class="amaz_primary_btn3 round_style f_w_600 min_140 text-center text-uppercase justify-content-center">+ Add Card</a>
                                        </div>
                                        <div class="billing_details_box_body">
                                            <div class="billing_details_box_body_inner d-flex pb-0 ">
                                                <div class="without_address_notify">
                                                    No Data Found
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-5">
                            <div class="order_wrapper mb_30">
                                <h3 class="font_20 f_w_700 mb_18">Your order</h3>
                                <div class="ordered_products_lists">
                                    <div class="single_lists">
                                        <span class=" total_text" >Product</span>
                                        <span class="total_prise">Total</span>
                                    </div>
                                    <div class="single_lists">
                                        <span class=" total_text" >Vestibulum suscipit × 1</span>
                                        <span class="total_prise">$165.00</span>
                                    </div>
                                    <div class="single_lists">
                                        <span class=" total_text" >Vestibulum dictum magna × 1</span>
                                        <span class="total_prise">$60.00</span>
                                    </div>
                                    <div class="single_lists">
                                        <span class=" total_text" >Cart Subtotal</span>
                                        <span class="total_prise ">$215.00</span>
                                    </div>
                                    <div class="single_lists">
                                        <span class=" total_text" >Shipping	</span>
                                        <div class="select_list">
                                            <label class="primary_checkbox style2  d-flex">
                                                <input type="checkbox">
                                                <span class="checkmark mr_10"></span>
                                                <span class="label_name f_w_400">Flat Rate: $9.00</span>
                                            </label>
                                            <label class="primary_checkbox style2  d-flex ">
                                                <input type="checkbox">
                                                <span class="checkmark mr_10"></span>
                                                <span class="label_name f_w_400">Free Shipping</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="single_lists">
                                        <span class="total_text" >Order Total</span>
                                        <span class="total_prise mark_text">$215.00</span>
                                    </div>
                                </div>
                                <div class="bank_transfer">
                                    <button class="amaz_primary_btn style2 f_w_600  text-center rounded-0 text-uppercase w-100">update cart</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- checkout_wrapper_area::end   -->


<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>