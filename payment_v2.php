<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- checkout_v3_area::start  -->
<div class="checkout_v3_area">
    <div class="checkout_v3_left d-flex justify-content-end">
        <div class="checkout_v3_inner">
            <div class="shiping_address_box checkout_form m-0">
                <div class="billing_address">
                    
                    <div class="row">
                        <div class="col-12">
                            <div class="shipingV3_info mb_30">
                                <div class="single_shipingV3_info d-flex align-items-start">
                                    <span>Contact</span>
                                    <h5 class="m-0 flex-fill">info@spondonit.com</h5>
                                    <a href="#" class="edit_info_text">Change</a>
                                </div>
                                <div class="single_shipingV3_info d-flex align-items-start">
                                    <span>Ship to</span>
                                    <h5 class="m-0 flex-fill">78/2 Razia Tower Boston, Manhattan,  <br>
                                        NY - 483145, Germany</h5>
                                    <a href="#" class="edit_info_text">Change</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mb_10">
                            <h3 class="check_v3_title2">Payment</h3>
                            <h6 class="shekout_subTitle_text">All transactions are secure and encrypted.</h6>
                        </div>
                        <div class="col-12">
                            <div class="accordion checkout_acc_style mb_30" id="accordionExample">
                                <div class="accordion-item">
                                    <div class="accordion-header" id="headingOne">
                                        <span class="accordion-button shadow-none" data-bs-toggle="collapse" data-bs-target="#collapseOne"  aria-controls="collapseOne">
                                            <span>
                                                <label class="primary_checkbox d-inline-flex style5 gap_10" >
                                                    <input checked name="card"  type="radio">
                                                    <span class="checkmark m-0"></span>
                                                    <span class="label_name f_w_500 ">Credit card</span>
                                                </label>
                                            </span>
                                        </span>
                                    </div>
                                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <!-- content ::start  -->
                                            <form action="#" name="#">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="primary_label2 style3" >Card Number</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="xxxx - xxxx - xxxx - xxxx">
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="primary_label2 style3" >Name on Card</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="Robert Downey JR">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >Expiration Date (MM/YY)</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="12 / 2026">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >Security Code</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="Robert Downey JR">
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- content ::end  -->
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingTwo">
                                    <span class="accordion-button shadow-none collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo"  aria-controls="collapseTwo">
                                        <span>
                                            <label class="primary_checkbox d-inline-flex style5 gap_10">
                                                <input name="card"  type="radio">
                                                <span class="checkmark m-0"></span>
                                                <span class="label_name f_w_500 ">
                                                    Cash on Delivery (COD)
                                                </span>
                                            </label>
                                        </span>
                                    </span>
                                    </h2>
                                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <!-- content ::start  -->
                                            <form action="#" name="#">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="primary_label2 style3" >Card Number</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="xxxx - xxxx - xxxx - xxxx">
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="primary_label2 style3" >Name on Card</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="Robert Downey JR">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >Expiration Date (MM/YY)</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="12 / 2026">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >Security Code</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="Robert Downey JR">
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- content ::end  -->
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingThree">
                                    <span class="accordion-button shadow-none collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree"  aria-controls="collapseThree">
                                        <label class="primary_checkbox d-inline-flex style5 gap_10">
                                            <input name="card"  type="radio">
                                            <span class="checkmark m-0"></span>
                                            <span class="label_name f_w_500 ">
                                                    Paypal
                                            </span>
                                        </label>
                                    </span>
                                    </h2>
                                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <!-- content ::start  -->
                                            <form action="#" name="#">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="primary_label2 style3" >Card Number</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="xxxx - xxxx - xxxx - xxxx">
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="primary_label2 style3" >Name on Card</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="Robert Downey JR">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >Expiration Date (MM/YY)</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="12 / 2026">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >Security Code</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="Robert Downey JR">
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- content ::end  -->
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingThree1">
                                    <span class="accordion-button shadow-none collapsed"  data-bs-toggle="collapse" data-bs-target="#collapseThree1" aria-controls="collapseThree1">
                                        <label class="primary_checkbox d-inline-flex style5 gap_10">
                                                <input name="card"  type="radio">
                                                <span class="checkmark m-0"></span>
                                                <span class="label_name f_w_500 ">
                                                    Stripe
                                                </span>
                                            </label>
                                    </span>
                                    </h2>
                                    <div id="collapseThree1" class="accordion-collapse collapse" aria-labelledby="headingThree1" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <!-- content ::start  -->
                                            <form action="#" name="#">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="primary_label2 style3" >Card Number</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="xxxx - xxxx - xxxx - xxxx">
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="primary_label2 style3" >Name on Card</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="Robert Downey JR">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >Expiration Date (MM/YY)</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="12 / 2026">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >Security Code</label>
                                                        <input class="primary_input3 style4 radius_3px mb_20" type="text"  placeholder="Robert Downey JR">
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- content ::end  -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mb_10">
                            <h3 class="check_v3_title2">Billing Address</h3>
                            <h6 class="shekout_subTitle_text">All transactions are secure and encrypted.</h6>
                        </div>
                        <div class="col-12">
                            <div class="accordion checkout_acc_style style2 mb_30" id="accordionExample1">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingTwo1">
                                    <span class="accordion-button shadow-none collapsed"  data-bs-toggle="collapse" >
                                        <label class="primary_checkbox d-inline-flex style5 gap_10">
                                            <input checked name="card2"  type="radio">
                                            <span class="checkmark m-0"></span>
                                            <span class="label_name f_w_500 ">
                                            Same as shipping address
                                            </span>
                                        </label>
                                    </span>
                                    </h2>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingTwo44">
                                    <span class="accordion-button shadow-none collapsed"  data-bs-toggle="collapse" data-bs-target="#collapseTwo44"  aria-controls="collapseTwo44">
                                        <label class="primary_checkbox d-inline-flex style5 gap_10">
                                            <input name="card2"  type="radio">
                                            <span class="checkmark m-0"></span>
                                            <span class="label_name f_w_500 ">
                                            Use a different billing address
                                            </span>
                                        </label>
                                        
                                    </span>
                                    </h2>
                                    <div id="collapseTwo44" class="accordion-collapse collapse" aria-labelledby="headingTwo44" data-bs-parent="#accordionExample44">
                                        <div class="accordion-body">
                                            <!-- content ::start  -->
                                            <form action="#" name="#">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >First Name</label>
                                                        <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="Enter your first and last name">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >Last Name</label>
                                                        <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="Enter your first and last name">
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="primary_label2 style3" >Address</label>
                                                        <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="House# 122, Street# 245, ABC Road">
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="primary_label2 style3" >Address Line 2 (Optional)</label>
                                                        <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="Apartment, suite, etc. (optional)">
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="primary_label2 style3" >City</label>
                                                        <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="Manhattan)">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >Country / Region</label>
                                                        <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="United Kingdom">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="primary_label2 style3" >Postal Code</label>
                                                        <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="154327">
                                                    </div>
                                                    
                                                </div>
                                            </form>
                                            <!-- content ::end  -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="check_v3_btns flex-wrap d-flex align-items-center">
                                <a href="#" class="amaz_primary_btn style2  min_200 text-center text-uppercase ">Pay now</a>
                                <a href="#" class="return_text">Return to shipping</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="checkout_v3_right d-flex justify-content-start">
        <div class="order_sumery_box flex-fill">
            <h3 class="check_v3_title mb_25">Order Summary</h3>
            <div class="subtotal_lists">
                <div class="single_total_list d-flex align-items-center">
                    <div class="single_total_left flex-fill">
                        <h4>Subtotal</h4>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 1324.35</span>
                    </div>
                </div>
                <div class="single_total_list d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <h4>Shipping Charge</h4>
                        <p>Package Wise Shipping Charge</p>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 75.35</span>
                    </div>
                </div>
                <div class="single_total_list d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <h4>Discount</h4>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 75.35</span>
                    </div>
                </div>
                <div class="total_amount d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <span class="total_text">Total (Incl. VAT)</span>
                    </div>
                    <div class="single_total_right">
                        <span class="total_text">USD <span>$1324.35</span></span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- checkout_v3_area::end  -->

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>