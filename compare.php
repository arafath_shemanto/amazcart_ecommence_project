<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- section ::start  -->
<div class="amaz_section section_spacing6 ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <h3 class="fs-4 fw-bold mb_30">Comparing Products</h3>
                <div class="comparing_box_area mb_30">
                    <div class="compare_product_descList">
                        <div class="">
                            <button class="reset_compare_text">Reset Compare</button>
                        </div>
                        <div class="single_product_list product_tricker compare_product">
                            
                            <ul class="comparison_lists style2">
                                <li >
                                    Description
                                </li>
                                <li >
                                    Vendor
                                </li>
                                <li >
                                    Color
                                </li>
                                <li >
                                    Size
                                </li>
                                <li >
                                    Color
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="compare_product_carousel">
                        <div class="compare_product_active owl-carousel">
                            <!-- single item  -->
                            <div class="single_product_list product_tricker compare_product">
                                <div class="compare_product_inner">
                                    <div class="product_widget5 border-0">
                                        <div class="product_thumb_upper">
                                            <div class="delete_pro position-absolute ">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="12.182" height="14.999" viewBox="0 0 12.182 14.999">
                                                    <g id="delete" transform="translate(0.003 0.001)">
                                                        <path id="Path_1969" data-name="Path 1969" d="M222.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,222.75,154.7Zm0,0" transform="translate(-214.589 -149.27)"/>
                                                        <path id="Path_1970" data-name="Path 1970" d="M104.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,104.75,154.7Zm0,0" transform="translate(-100.734 -149.27)"/>
                                                        <path id="Path_1971" data-name="Path 1971" d="M.994,4.464v8.655a1.938,1.938,0,0,0,.515,1.337A1.73,1.73,0,0,0,2.765,15H9.411a1.729,1.729,0,0,0,1.255-.543,1.938,1.938,0,0,0,.515-1.337V4.464a1.342,1.342,0,0,0-.344-2.639h-1.8V1.386A1.38,1.38,0,0,0,7.648,0H4.528A1.38,1.38,0,0,0,3.137,1.386v.439h-1.8A1.342,1.342,0,0,0,.994,4.464ZM9.411,14.3H2.765A1.112,1.112,0,0,1,1.7,13.119V4.5h8.782v8.624A1.112,1.112,0,0,1,9.411,14.3ZM3.84,1.386A.677.677,0,0,1,4.528.7H7.648a.677.677,0,0,1,.689.685v.439H3.84Zm-2.5,1.142h9.5a.632.632,0,1,1,0,1.265h-9.5a.632.632,0,1,1,0-1.265Zm0,0" transform="translate(0 0)"/>
                                                        <path id="Path_1972" data-name="Path 1972" d="M163.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,163.75,154.7Zm0,0" transform="translate(-157.662 -149.27)"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <a href="product_details.php" class="thumb">
                                                <img src="img/amazPorduct/1.png" alt="">
                                            </a>
                                            <div class="product_action">
                                                <a href="#">
                                                    <i class="ti-control-shuffle"></i>
                                                </a>
                                                <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                    <i class="ti-eye"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="ti-star"></i>
                                                </a>
                                            </div>
                                            <span class="badge_1"> -20% </span>
                                            <span class="badge_1 style2 text-uppercase"> New </span>
                                        </div>
                                        <div class="product__meta text-center">
                                            <span class="product_banding ">Studio Design</span>
                                            <a href="product_details.php">
                                                <h4>Designs Woolrich Kletter</h4>
                                            </a>
                                            <div class="stars justify-content-center">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <div class="product_prise">
                                                <p><span>$29.00 </span>  $20.00</p>
                                                <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal" href="#">Add To Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="comparison_lists">
                                    <li>
                                        Short unique description
                                    </li>
                                    <li>
                                        Gap
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                    <li>
                                        19” Inch
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                </ul>
                            </div>
                            <!-- single item  -->
                            <div class="single_product_list product_tricker compare_product">
                                <div class="compare_product_inner">
                                    <div class="product_widget5 border-0">
                                        <div class="product_thumb_upper">
                                            <div class="delete_pro position-absolute ">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="12.182" height="14.999" viewBox="0 0 12.182 14.999">
                                                    <g id="delete" transform="translate(0.003 0.001)">
                                                        <path id="Path_1969" data-name="Path 1969" d="M222.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,222.75,154.7Zm0,0" transform="translate(-214.589 -149.27)"/>
                                                        <path id="Path_1970" data-name="Path 1970" d="M104.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,104.75,154.7Zm0,0" transform="translate(-100.734 -149.27)"/>
                                                        <path id="Path_1971" data-name="Path 1971" d="M.994,4.464v8.655a1.938,1.938,0,0,0,.515,1.337A1.73,1.73,0,0,0,2.765,15H9.411a1.729,1.729,0,0,0,1.255-.543,1.938,1.938,0,0,0,.515-1.337V4.464a1.342,1.342,0,0,0-.344-2.639h-1.8V1.386A1.38,1.38,0,0,0,7.648,0H4.528A1.38,1.38,0,0,0,3.137,1.386v.439h-1.8A1.342,1.342,0,0,0,.994,4.464ZM9.411,14.3H2.765A1.112,1.112,0,0,1,1.7,13.119V4.5h8.782v8.624A1.112,1.112,0,0,1,9.411,14.3ZM3.84,1.386A.677.677,0,0,1,4.528.7H7.648a.677.677,0,0,1,.689.685v.439H3.84Zm-2.5,1.142h9.5a.632.632,0,1,1,0,1.265h-9.5a.632.632,0,1,1,0-1.265Zm0,0" transform="translate(0 0)"/>
                                                        <path id="Path_1972" data-name="Path 1972" d="M163.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,163.75,154.7Zm0,0" transform="translate(-157.662 -149.27)"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <a href="product_details.php" class="thumb">
                                                <img src="img/amazPorduct/2.png" alt="">
                                            </a>
                                            <div class="product_action">
                                                <a href="#">
                                                    <i class="ti-control-shuffle"></i>
                                                </a>
                                                <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                    <i class="ti-eye"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="ti-star"></i>
                                                </a>
                                            </div>
                                            <span class="badge_1"> -20% </span>
                                            <span class="badge_1 style2 text-uppercase"> New </span>
                                        </div>
                                        <div class="product__meta text-center">
                                            <span class="product_banding ">Studio Design</span>
                                            <a href="product_details.php">
                                                <h4>Designs Woolrich Kletter</h4>
                                            </a>
                                            <div class="stars justify-content-center">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <div class="product_prise">
                                                <p><span>$29.00 </span>  $20.00</p>
                                                <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal" href="#">Add To Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="comparison_lists">
                                    <li>
                                        Short unique description
                                    </li>
                                    <li>
                                        Gap
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                    <li>
                                        19” Inch
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                </ul>
                            </div>
                            <!-- single item  -->
                            <div class="single_product_list product_tricker compare_product">
                                <div class="compare_product_inner">
                                    <div class="product_widget5 border-0">
                                        <div class="product_thumb_upper">
                                            <div class="delete_pro position-absolute ">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="12.182" height="14.999" viewBox="0 0 12.182 14.999">
                                                    <g id="delete" transform="translate(0.003 0.001)">
                                                        <path id="Path_1969" data-name="Path 1969" d="M222.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,222.75,154.7Zm0,0" transform="translate(-214.589 -149.27)"/>
                                                        <path id="Path_1970" data-name="Path 1970" d="M104.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,104.75,154.7Zm0,0" transform="translate(-100.734 -149.27)"/>
                                                        <path id="Path_1971" data-name="Path 1971" d="M.994,4.464v8.655a1.938,1.938,0,0,0,.515,1.337A1.73,1.73,0,0,0,2.765,15H9.411a1.729,1.729,0,0,0,1.255-.543,1.938,1.938,0,0,0,.515-1.337V4.464a1.342,1.342,0,0,0-.344-2.639h-1.8V1.386A1.38,1.38,0,0,0,7.648,0H4.528A1.38,1.38,0,0,0,3.137,1.386v.439h-1.8A1.342,1.342,0,0,0,.994,4.464ZM9.411,14.3H2.765A1.112,1.112,0,0,1,1.7,13.119V4.5h8.782v8.624A1.112,1.112,0,0,1,9.411,14.3ZM3.84,1.386A.677.677,0,0,1,4.528.7H7.648a.677.677,0,0,1,.689.685v.439H3.84Zm-2.5,1.142h9.5a.632.632,0,1,1,0,1.265h-9.5a.632.632,0,1,1,0-1.265Zm0,0" transform="translate(0 0)"/>
                                                        <path id="Path_1972" data-name="Path 1972" d="M163.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,163.75,154.7Zm0,0" transform="translate(-157.662 -149.27)"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <a href="product_details.php" class="thumb">
                                                <img src="img/amazPorduct/3.png" alt="">
                                            </a>
                                            <div class="product_action">
                                                <a href="#">
                                                    <i class="ti-control-shuffle"></i>
                                                </a>
                                                <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                    <i class="ti-eye"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="ti-star"></i>
                                                </a>
                                            </div>
                                            <span class="badge_1"> -20% </span>
                                            <span class="badge_1 style2 text-uppercase"> New </span>
                                        </div>
                                        <div class="product__meta text-center">
                                            <span class="product_banding ">Studio Design</span>
                                            <a href="product_details.php">
                                                <h4>Designs Woolrich Kletter</h4>
                                            </a>
                                            <div class="stars justify-content-center">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <div class="product_prise">
                                                <p><span>$29.00 </span>  $20.00</p>
                                                <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal" href="#">Add To Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="comparison_lists">
                                    <li>
                                        Short unique description
                                    </li>
                                    <li>
                                        Gap
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                    <li>
                                        19” Inch
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                </ul>
                            </div>
                            <!-- single item  -->
                            <div class="single_product_list product_tricker compare_product">
                                <div class="compare_product_inner">
                                    <div class="product_widget5 border-0">
                                        <div class="product_thumb_upper">
                                            <div class="delete_pro position-absolute ">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="12.182" height="14.999" viewBox="0 0 12.182 14.999">
                                                    <g id="delete" transform="translate(0.003 0.001)">
                                                        <path id="Path_1969" data-name="Path 1969" d="M222.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,222.75,154.7Zm0,0" transform="translate(-214.589 -149.27)"/>
                                                        <path id="Path_1970" data-name="Path 1970" d="M104.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,104.75,154.7Zm0,0" transform="translate(-100.734 -149.27)"/>
                                                        <path id="Path_1971" data-name="Path 1971" d="M.994,4.464v8.655a1.938,1.938,0,0,0,.515,1.337A1.73,1.73,0,0,0,2.765,15H9.411a1.729,1.729,0,0,0,1.255-.543,1.938,1.938,0,0,0,.515-1.337V4.464a1.342,1.342,0,0,0-.344-2.639h-1.8V1.386A1.38,1.38,0,0,0,7.648,0H4.528A1.38,1.38,0,0,0,3.137,1.386v.439h-1.8A1.342,1.342,0,0,0,.994,4.464ZM9.411,14.3H2.765A1.112,1.112,0,0,1,1.7,13.119V4.5h8.782v8.624A1.112,1.112,0,0,1,9.411,14.3ZM3.84,1.386A.677.677,0,0,1,4.528.7H7.648a.677.677,0,0,1,.689.685v.439H3.84Zm-2.5,1.142h9.5a.632.632,0,1,1,0,1.265h-9.5a.632.632,0,1,1,0-1.265Zm0,0" transform="translate(0 0)"/>
                                                        <path id="Path_1972" data-name="Path 1972" d="M163.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,163.75,154.7Zm0,0" transform="translate(-157.662 -149.27)"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <a href="product_details.php" class="thumb">
                                                <img src="img/amazPorduct/4.png" alt="">
                                            </a>
                                            <div class="product_action">
                                                <a href="#">
                                                    <i class="ti-control-shuffle"></i>
                                                </a>
                                                <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                    <i class="ti-eye"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="ti-star"></i>
                                                </a>
                                            </div>
                                            <span class="badge_1"> -20% </span>
                                            <span class="badge_1 style2 text-uppercase"> New </span>
                                        </div>
                                        <div class="product__meta text-center">
                                            <span class="product_banding ">Studio Design</span>
                                            <a href="product_details.php">
                                                <h4>Designs Woolrich Kletter</h4>
                                            </a>
                                            <div class="stars justify-content-center">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <div class="product_prise">
                                                <p><span>$29.00 </span>  $20.00</p>
                                                <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal" href="#">Add To Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="comparison_lists">
                                    <li>
                                        Short unique description
                                    </li>
                                    <li>
                                        Gap
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                    <li>
                                        19” Inch
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                </ul>
                            </div>
                            <!-- single item  -->
                            <div class="single_product_list product_tricker compare_product">
                                <div class="compare_product_inner">
                                    <div class="product_widget5 border-0">
                                        <div class="product_thumb_upper">
                                            <div class="delete_pro position-absolute ">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="12.182" height="14.999" viewBox="0 0 12.182 14.999">
                                                    <g id="delete" transform="translate(0.003 0.001)">
                                                        <path id="Path_1969" data-name="Path 1969" d="M222.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,222.75,154.7Zm0,0" transform="translate(-214.589 -149.27)"/>
                                                        <path id="Path_1970" data-name="Path 1970" d="M104.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,104.75,154.7Zm0,0" transform="translate(-100.734 -149.27)"/>
                                                        <path id="Path_1971" data-name="Path 1971" d="M.994,4.464v8.655a1.938,1.938,0,0,0,.515,1.337A1.73,1.73,0,0,0,2.765,15H9.411a1.729,1.729,0,0,0,1.255-.543,1.938,1.938,0,0,0,.515-1.337V4.464a1.342,1.342,0,0,0-.344-2.639h-1.8V1.386A1.38,1.38,0,0,0,7.648,0H4.528A1.38,1.38,0,0,0,3.137,1.386v.439h-1.8A1.342,1.342,0,0,0,.994,4.464ZM9.411,14.3H2.765A1.112,1.112,0,0,1,1.7,13.119V4.5h8.782v8.624A1.112,1.112,0,0,1,9.411,14.3ZM3.84,1.386A.677.677,0,0,1,4.528.7H7.648a.677.677,0,0,1,.689.685v.439H3.84Zm-2.5,1.142h9.5a.632.632,0,1,1,0,1.265h-9.5a.632.632,0,1,1,0-1.265Zm0,0" transform="translate(0 0)"/>
                                                        <path id="Path_1972" data-name="Path 1972" d="M163.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,163.75,154.7Zm0,0" transform="translate(-157.662 -149.27)"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <a href="product_details.php" class="thumb">
                                                <img src="img/amazPorduct/5.png" alt="">
                                            </a>
                                            <div class="product_action">
                                                <a href="#">
                                                    <i class="ti-control-shuffle"></i>
                                                </a>
                                                <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                    <i class="ti-eye"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="ti-star"></i>
                                                </a>
                                            </div>
                                            <span class="badge_1"> -20% </span>
                                            <span class="badge_1 style2 text-uppercase"> New </span>
                                        </div>
                                        <div class="product__meta text-center">
                                            <span class="product_banding ">Studio Design</span>
                                            <a href="product_details.php">
                                                <h4>Designs Woolrich Kletter</h4>
                                            </a>
                                            <div class="stars justify-content-center">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <div class="product_prise">
                                                <p><span>$29.00 </span>  $20.00</p>
                                                <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal" href="#">Add To Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="comparison_lists">
                                    <li>
                                        Short unique description
                                    </li>
                                    <li>
                                        Gap
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                    <li>
                                        19” Inch
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                </ul>
                            </div>
                            <!-- single item  -->
                            <div class="single_product_list product_tricker compare_product">
                                <div class="compare_product_inner">
                                    <div class="product_widget5 border-0">
                                        <div class="product_thumb_upper">
                                            <div class="delete_pro position-absolute ">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="12.182" height="14.999" viewBox="0 0 12.182 14.999">
                                                    <g id="delete" transform="translate(0.003 0.001)">
                                                        <path id="Path_1969" data-name="Path 1969" d="M222.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,222.75,154.7Zm0,0" transform="translate(-214.589 -149.27)"/>
                                                        <path id="Path_1970" data-name="Path 1970" d="M104.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,104.75,154.7Zm0,0" transform="translate(-100.734 -149.27)"/>
                                                        <path id="Path_1971" data-name="Path 1971" d="M.994,4.464v8.655a1.938,1.938,0,0,0,.515,1.337A1.73,1.73,0,0,0,2.765,15H9.411a1.729,1.729,0,0,0,1.255-.543,1.938,1.938,0,0,0,.515-1.337V4.464a1.342,1.342,0,0,0-.344-2.639h-1.8V1.386A1.38,1.38,0,0,0,7.648,0H4.528A1.38,1.38,0,0,0,3.137,1.386v.439h-1.8A1.342,1.342,0,0,0,.994,4.464ZM9.411,14.3H2.765A1.112,1.112,0,0,1,1.7,13.119V4.5h8.782v8.624A1.112,1.112,0,0,1,9.411,14.3ZM3.84,1.386A.677.677,0,0,1,4.528.7H7.648a.677.677,0,0,1,.689.685v.439H3.84Zm-2.5,1.142h9.5a.632.632,0,1,1,0,1.265h-9.5a.632.632,0,1,1,0-1.265Zm0,0" transform="translate(0 0)"/>
                                                        <path id="Path_1972" data-name="Path 1972" d="M163.75,154.7a.351.351,0,0,0-.351.351v6.639a.351.351,0,1,0,.7,0v-6.639A.351.351,0,0,0,163.75,154.7Zm0,0" transform="translate(-157.662 -149.27)"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <a href="product_details.php" class="thumb">
                                                <img src="img/amazPorduct/1.png" alt="">
                                            </a>
                                            <div class="product_action">
                                                <a href="#">
                                                    <i class="ti-control-shuffle"></i>
                                                </a>
                                                <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                    <i class="ti-eye"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="ti-star"></i>
                                                </a>
                                            </div>
                                            <span class="badge_1"> -20% </span>
                                            <span class="badge_1 style2 text-uppercase"> New </span>
                                        </div>
                                        <div class="product__meta text-center">
                                            <span class="product_banding ">Studio Design</span>
                                            <a href="product_details.php">
                                                <h4>Designs Woolrich Kletter</h4>
                                            </a>
                                            <div class="stars justify-content-center">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <div class="product_prise">
                                                <p><span>$29.00 </span>  $20.00</p>
                                                <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal" href="#">Add To Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="comparison_lists">
                                    <li>
                                        Short unique description
                                    </li>
                                    <li>
                                        Gap
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                    <li>
                                        19” Inch
                                    </li>
                                    <li>
                                        Black
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- section ::end  -->

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>