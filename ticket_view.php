<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<div class="amazy_dashboard_area dashboard_bg section_spacing6">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3">
                <?php include 'include/dashboard_sidebar.php' ?>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="dashboard_white_box style4 bg-white mb_25">
                    <h3 class="font_20 f_w_700 lh-base mb_10 max_450px">#204 - Status of some DZ, DL and XB shipments showing delivered</h3>
                    <p class="font_14 f_w_400 mb_25">06/14/2021, 09:20 AM</p>
                    <div class="ticket_view_box theme_border gray_color_1 radius_5px mb_10">
                        <div class="ticket_view_box_head d-flex align-items-center mb_20">
                            <div class="thicket_view_profile d-flex align-items-center gap_15 flex-fill">
                                <div class="thumb">
                                    <img src="img/ticket/ticket_img.png" alt="" class="img-fluid">
                                </div>
                                <h4 class="font_18 f_w_700 m-0">Robert Downey</h4>
                            </div>
                            <span class="font_14 f_w_400 mute_text">03/25/2021, 04:32 AM</span>
                        </div>
                        <div class="ticket_view_box_body">
                            <p class="lineHeight1">Konseptbizden believes everyone should live in a home they love of in a home they love throughhnology and innovation everyone should.</p>
                        </div>
                    </div>
                    <div class="ticket_view_box theme_border gray_color_1 radius_5px mb_10">
                        <div class="ticket_view_box_head d-flex align-items-center mb_20">
                            <div class="thicket_view_profile d-flex align-items-center gap_15 flex-fill">
                                <div class="thumb">
                                    <img src="img/ticket/ticket_img.png" alt="" class="img-fluid">
                                </div>
                                <h4 class="font_18 f_w_700 m-0">Robert Downey</h4>
                            </div>
                            <span class="font_14 f_w_400 mute_text">03/25/2021, 04:32 AM</span>
                        </div>
                        <div class="ticket_view_box_body">
                            <p class="mb_10">Hi, Downey</p>
                            <p class="lineHeight1 mb_10">Konseptbizden believes everyone should live in a home they love hero nology and this is innovation, Konseptbizden makes it possible for shoppers to quickly and easy rester of fiexactly what they want.</p>
                            <p class="mb_10">Regards</p>
                            <p>Peter Parker <br>
                            SpondonIT Support <br>
                            +880 124 5678 321</p>
                        </div>
                    </div>
                    <div class="ticket_view_box theme_border gray_color_1 radius_5px mb_20">
                        <div class="ticket_view_box_head d-flex align-items-center mb_20">
                            <div class="thicket_view_profile d-flex align-items-center gap_15 flex-fill">
                                <div class="thumb">
                                    <img src="img/ticket/ticket_img.png" alt="" class="img-fluid">
                                </div>
                                <h4 class="font_18 f_w_700 m-0">Robert Downey</h4>
                            </div>
                            <span class="font_14 f_w_400 mute_text">03/25/2021, 04:32 AM</span>
                        </div>
                        <div class="ticket_view_box_body">
                            <p class="lineHeight1 mb_10">Konseptbizden believes everyone should live in a home they love of in a home they love throughhnology and innovation everyone should.</p>
                            <a href="#" class="file_name amaz_badge_btn6 d-inline-flex align-items-center gap_12">
                                <i class="fas fa-file-alt"></i>
                                <span class="lh-1">Main.XD</span>
                            </a>
                        </div>
                    </div>
                    <!-- <div class="ticket_viewBtn_group d-flex align-items-center">
                        <input type="text" class="flex-fill view_input" placeholder="Type message here…">
                        <span class="file_btn position-relative gj-cursor-pointer">
                            <i class="fas fa-paperclip"></i>
                            <input type="file" class="file_up position-absolute  w-100 top-0 start-0 end-0 bottom-0 gj-cursor-pointer ">
                        </span>
                        <button class="amaz_primary_btn style8 radius_3px" type="button">Reply</button>
                    </div> -->
                    <div class="d-flex align-items-center justify-content-end mb_20">
                        <button data-bs-toggle="collapse" data-bs-target="#replay_toggler" aria-expanded="false" aria-controls="replay_toggler" id="replay_box_toggler" class="amaz_primary_btn style2 " type="button">Reply</button>
                    </div>
                    <div  class="collapse" id="replay_toggler">
                        <div id="replay_form_boxInner" class="row ">
                            <div class="col-12 mb_20">
                                <label class="primary_label2 style2 ">Description</label>
                                <textarea  placeholder="Write description here…" id="summernote" name="editordata" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Write description here… here…'" class="primary_textarea4 radius_5px mb_25" required=""></textarea>
                            </div>
                            <div class="col-12">
                                <div class="tkt_uploader d-flex align-items-center gap_15 position-relative mb_20  justify-content-center">
                                <input type="file" class=" position-absolute start-0 top-0 end-0 bottom-0 w-100 gj-cursor-pointer">
                                    <i class="fas fa-file-alt font_14 mute_text"></i>
                                    <p class="font_14 mute_text f_w_500 m-0">Drop your file here or <a href="#">choose files to upload</a></p>
                                </div>
                            </div>
                            <div class="col-12 d-flex justify-content-end">
                                <button class="amaz_primary_btn style2 rounded-0  text-uppercase  text-center w-100">+ Reply now</button>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <div class="dashboard_white_box style5 bg-white mb_25">
                    <div class="thumb">
                        <img class="img-fluid mb_20" src="img/ticket/ticket_img3.png" alt="">
                    </div>
                    <h3 class="font_16 f_w_700 mb_3">Robert Downey JR</h3>
                    <p class="font_14 f_w_400 mb_15">Agent</p>
                    <div class="user_wrapper_lists mb_20">
                        <div class="user_wrapper_list d-flex align-items-center align-items-center">
                            <div class="user_wrapper_left d-flex align-items-center justify-content-between">
                                <span class="font_14 f_w_500">Stauts</span>
                                <span class="font_14 f_w_500">:</span>
                            </div>
                            <a href="#" class="amaz_badge_btn7 d-inline-flex align-items-center">
                                Pending
                            </a>
                        </div>
                        <div class="user_wrapper_list d-flex align-items-center align-items-center">
                            <div class="user_wrapper_left d-flex align-items-center justify-content-between">
                                <span class="font_14 f_w_500">Stauts</span>
                                <span class="font_14 f_w_500">:</span>
                            </div>
                            <a href="#" class="amaz_badge_btn7 d-inline-flex align-items-center">
                                High
                            </a>
                        </div>
                        <div class="user_wrapper_list d-flex align-items-center align-items-center">
                            <div class="user_wrapper_left d-flex align-items-center justify-content-between">
                                <span class="font_14 f_w_500">Category</span>
                                <span class="font_14 f_w_500">:</span>
                            </div>
                            <a href="#" class="amaz_badge_btn7 d-inline-flex align-items-center">
                                Installation
                            </a>
                        </div>
                    </div>
                    <p class="font_14 f_w_500 mb_3">Last Update :</p>
                    <h3 class="font_14 f_w_500 m-0">June 12, 2021 at 8:47 PM</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>