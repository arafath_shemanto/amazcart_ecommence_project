<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- checkout_v3_area::start  -->
<div class="checkout_v3_area">
    <div class="checkout_v3_left d-flex justify-content-end">
        <div class="checkout_v3_inner">
            <div class="table-responsive mb-0">
                <table class="table amazy_table3 style4 mb-0">
                    <thead>
                        <tr>
                            <th class="font_14 f_w_700 m-0 text-nowrap priamry_text">
                                Products
                            </th>
                            <th class="font_14 f_w_700 m-0 text-nowrap priamry_text">
                                Price
                            </th>
                            <th class="font_14 f_w_700 m-0 text-nowrap priamry_text">
                                QUANTITY
                            </th>
                            <th class="font_14 f_w_700 m-0 text-nowrap priamry_text">
                                Subtotal
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <a href="product_details.php" class="d-flex align-items-center gap_20">
                                    <div class="thumb">
                                        <img src="img/amazPorduct/summery_product_1.png" alt="">
                                    </div>
                                    <div class="summery_pro_content">
                                        <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                        <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                    </div>
                                </a>
                            </td>
                            <td>
                                <h4 class="font_16 f_w_700 m-0 text-nowrap">$3500.00</h4>
                            </td>
                            <td>
                                <div class="product_number_count style_4" data-target="amount-1">
                                    <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                    <input id="amount-1" class="count_single_item input-number" type="text" value="1">
                                    <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                                </div>
                            </td>
                            <td class="f_w_700">
                                <div class="m-0 d-flex gap_10 align-items-center">
                                    <h4 class="font_16 f_w_700 m-0 lh-1">
                                        $4,000.00 
                                    </h4>
                                    <span class="close_icon style_2 lh-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.249" height="15.076" viewBox="0 0 12.249 15.076">
                                            <g  transform="translate(-48)">
                                                <path  data-name="Path 1449" d="M59.071,1.884H56.48V1.413A1.415,1.415,0,0,0,55.067,0H53.182a1.415,1.415,0,0,0-1.413,1.413v.471H49.178A1.179,1.179,0,0,0,48,3.062V4.711a.471.471,0,0,0,.471.471h.257l.407,8.547a1.412,1.412,0,0,0,1.412,1.346H57.7a1.412,1.412,0,0,0,1.412-1.346l.407-8.547h.257a.471.471,0,0,0,.471-.471V3.062A1.179,1.179,0,0,0,59.071,1.884Zm-6.36-.471a.472.472,0,0,1,.471-.471h1.884a.472.472,0,0,1,.471.471v.471H52.711ZM48.942,3.062a.236.236,0,0,1,.236-.236h9.893a.236.236,0,0,1,.236.236V4.24H48.942Zm9.23,10.623a.471.471,0,0,1-.471.449H50.547a.471.471,0,0,1-.471-.449l-.4-8.5h8.905Z" fill="#00124e"></path>
                                                <path  data-name="Path 1450" d="M240.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,240.471,215.067Z" transform="translate(-186.347 -201.875)" fill="#00124e"></path>
                                                <path  data-name="Path 1451" d="M320.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,320.471,215.067Z" transform="translate(-263.991 -201.875)" fill="#00124e"></path>
                                                <path  data-name="Path 1452" d="M160.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,0,0-.942,0V214.6A.471.471,0,0,0,160.471,215.067Z" transform="translate(-108.702 -201.875)" fill="#00124e"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </div>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="product_details.php" class="d-flex align-items-center gap_20">
                                    <div class="thumb">
                                        <img src="img/amazPorduct/summery_product_1.png" alt="">
                                    </div>
                                    <div class="summery_pro_content">
                                        <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                        <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                    </div>
                                </a>
                            </td>
                            <td>
                                <h4 class="font_16 f_w_700 m-0 text-nowrap">$3500.00</h4>
                            </td>
                            <td>
                                <div class="product_number_count style_4" data-target="amount-2">
                                    <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                    <input id="amount-2" class="count_single_item input-number" type="text" value="1">
                                    <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                                </div>
                            </td>
                            <td class="f_w_700">
                                <div class="m-0 d-flex gap_10 align-items-center">
                                    <h4 class="font_16 f_w_700 m-0 lh-1">
                                        $4,000.00 
                                    </h4>
                                    <span class="close_icon style_2 lh-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.249" height="15.076" viewBox="0 0 12.249 15.076">
                                            <g  transform="translate(-48)">
                                                <path  data-name="Path 1449" d="M59.071,1.884H56.48V1.413A1.415,1.415,0,0,0,55.067,0H53.182a1.415,1.415,0,0,0-1.413,1.413v.471H49.178A1.179,1.179,0,0,0,48,3.062V4.711a.471.471,0,0,0,.471.471h.257l.407,8.547a1.412,1.412,0,0,0,1.412,1.346H57.7a1.412,1.412,0,0,0,1.412-1.346l.407-8.547h.257a.471.471,0,0,0,.471-.471V3.062A1.179,1.179,0,0,0,59.071,1.884Zm-6.36-.471a.472.472,0,0,1,.471-.471h1.884a.472.472,0,0,1,.471.471v.471H52.711ZM48.942,3.062a.236.236,0,0,1,.236-.236h9.893a.236.236,0,0,1,.236.236V4.24H48.942Zm9.23,10.623a.471.471,0,0,1-.471.449H50.547a.471.471,0,0,1-.471-.449l-.4-8.5h8.905Z" fill="#00124e"></path>
                                                <path  data-name="Path 1450" d="M240.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,240.471,215.067Z" transform="translate(-186.347 -201.875)" fill="#00124e"></path>
                                                <path  data-name="Path 1451" d="M320.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,320.471,215.067Z" transform="translate(-263.991 -201.875)" fill="#00124e"></path>
                                                <path  data-name="Path 1452" d="M160.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,0,0-.942,0V214.6A.471.471,0,0,0,160.471,215.067Z" transform="translate(-108.702 -201.875)" fill="#00124e"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </div>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="product_details.php" class="d-flex align-items-center gap_20">
                                    <div class="thumb">
                                        <img src="img/amazPorduct/summery_product_1.png" alt="">
                                    </div>
                                    <div class="summery_pro_content">
                                        <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                        <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                    </div>
                                </a>
                            </td>
                            <td>
                                <h4 class="font_16 f_w_700 m-0 text-nowrap">$3500.00</h4>
                            </td>
                            <td>
                                <div class="product_number_count style_4" data-target="amount-3">
                                    <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                    <input id="amount-3" class="count_single_item input-number" type="text" value="1">
                                    <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                                </div>
                            </td>
                            <td class="f_w_700">
                                <div class="m-0 d-flex gap_10 align-items-center">
                                    <h4 class="font_16 f_w_700 m-0 lh-1">
                                        $4,000.00 
                                    </h4>
                                    <span class="close_icon style_2 lh-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.249" height="15.076" viewBox="0 0 12.249 15.076">
                                            <g  transform="translate(-48)">
                                                <path  data-name="Path 1449" d="M59.071,1.884H56.48V1.413A1.415,1.415,0,0,0,55.067,0H53.182a1.415,1.415,0,0,0-1.413,1.413v.471H49.178A1.179,1.179,0,0,0,48,3.062V4.711a.471.471,0,0,0,.471.471h.257l.407,8.547a1.412,1.412,0,0,0,1.412,1.346H57.7a1.412,1.412,0,0,0,1.412-1.346l.407-8.547h.257a.471.471,0,0,0,.471-.471V3.062A1.179,1.179,0,0,0,59.071,1.884Zm-6.36-.471a.472.472,0,0,1,.471-.471h1.884a.472.472,0,0,1,.471.471v.471H52.711ZM48.942,3.062a.236.236,0,0,1,.236-.236h9.893a.236.236,0,0,1,.236.236V4.24H48.942Zm9.23,10.623a.471.471,0,0,1-.471.449H50.547a.471.471,0,0,1-.471-.449l-.4-8.5h8.905Z" fill="#00124e"></path>
                                                <path  data-name="Path 1450" d="M240.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,240.471,215.067Z" transform="translate(-186.347 -201.875)" fill="#00124e"></path>
                                                <path  data-name="Path 1451" d="M320.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,320.471,215.067Z" transform="translate(-263.991 -201.875)" fill="#00124e"></path>
                                                <path  data-name="Path 1452" d="M160.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,0,0-.942,0V214.6A.471.471,0,0,0,160.471,215.067Z" transform="translate(-108.702 -201.875)" fill="#00124e"></path>
                                            </g>
                                        </svg>
                                    </span>
                                </div>
                                
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="d-flex gap_10 align-items-center flex-wrap mt_20">
                <div class="d-flex align-items-center gap_10 flex-fill flex-wrap">
                <button class="amaz_primary_btn2 style3">Update cart</button>
                <a href="product.php" class="amaz_primary_btn2 style3">COntinue shopping</a>
                </div>
                <a href="checkout_v2.php" class="amaz_primary_btn min_200 style2">Process to checkout</a>
            </div>
        </div>
    </div>
    <div class="checkout_v3_right d-flex justify-content-start">
        <div class="order_sumery_box flex-fill">
            <h3 class="check_v3_title mb_25">Order Summary</h3>
            <div class="subtotal_lists">
                <div class="single_total_list d-flex align-items-center">
                    <div class="single_total_left flex-fill">
                        <h4>Subtotal</h4>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 1324.35</span>
                    </div>
                </div>
                <div class="single_total_list d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <h4>Shipping Charge</h4>
                        <p>Package Wise Shipping Charge</p>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 75.35</span>
                    </div>
                </div>
                <div class="single_total_list d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <h4>Discount</h4>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 75.35</span>
                    </div>
                </div>
                <div class="total_amount d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <span class="total_text">Total (Incl. VAT)</span>
                    </div>
                    <div class="single_total_right">
                        <span class="total_text">USD <span>$1324.35</span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- checkout_v3_area::end  -->

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>