<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>



<!-- product_details_wrapper::start  -->
<div class="product_details_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xl-9">
                <div class="row">
                    <div class="col-lg-6 col-xl-6">
                        <div class="slider-container slick_custom_container mb_30">
                            <div class="slider-for">
                                <div class="item-slick">
                                    <img id="product_zoom_1" src="img/amazPorduct/details_view_img.jpg" data-zoom-image="img/amazPorduct/details_view_img.jpg" alt="">
                                </div>
                                <div class="item-slick">
                                    <img id="product_zoom_2" src="img/amazPorduct/details_view_img.jpg" data-zoom-image="img/amazPorduct/details_view_img.jpg" alt="">
                                </div>
                                <div class="item-slick">
                                    <img id="product_zoom_3" src="img/amazPorduct/details_view_img.jpg" data-zoom-image="img/amazPorduct/details_view_img.jpg" alt="">
                                </div>
                                <div class="item-slick">
                                <img id="product_zoom_3" src="img/amazPorduct/details_view_img.jpg" data-zoom-image="img/amazPorduct/details_view_img.jpg" alt="">
                                </div>
                            </div>
                            <div class="slider-nav"> 
                                <div class="item-slick">
                                    <img src="img/amazPorduct/details_view_small.png" alt="">
                                </div>
                                <div class="item-slick">
                                    <img src="img/amazPorduct/details_view_img2.png" alt="">
                                </div>
                                <div class="item-slick">
                                    <img src="img/amazPorduct/details_view_img3.png" alt="">
                                </div>
                                <div class="item-slick">
                                    <img src="img/amazPorduct/details_view_img3.png" alt="">
                                </div>
                            </div>
                        </div>
                        <!-- prev design  -->
                        <!-- <div class="details_tab_style style5 mb_30">
                            <div class="tab-content details_tab_content " id="myTabContent2">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <img id="product_zoom_2" src="img/amazPorduct/details_view_img.jpg" data-zoom-image="img/amazPorduct/details_view_img.jpg" alt="">
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <img id="product_zoom_3" src="img/amazPorduct/details_view_img.jpg" data-zoom-image="img/amazPorduct/details_view_img.jpg" alt="">
                                </div>
                            </div>
                            <ul class="nav" id="myTab2" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                                        
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                                        <img src="img/amazPorduct/details_view_img2.png" alt="">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                                        <img src="img/amazPorduct/details_view_img3.png" alt="">
                                    </a>
                                </li>
                            </ul>
                        </div> -->
                        <!-- prev design::end  -->

                    </div>
                    <div class="col-lg-6 col-xl-6">
                        <div class="product_content_details mb_20">
                            <span class="stoke_badge">In Stoke</span>
                            <h3>QUBO Big1 - 4500mAh Big Battery, 6.26”
                                WaterDrop Screen, 1GB+16GB Memory 
                                5MP+8MP Camera</h3>
                            <div class="viendor_text d-flex align-items-center">
                                <p class="stock_text"> <span class="text-uppercase">SKU:</span> 00114с-1</p>
                                <p class="stock_text"> <span class="text-uppercase">VENDOR:</span> Dior</p>
                            </div>
                            <div class="product_ratings">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span>(8k+ reviews)</span>
                            </div>

                            <div class="destils_prise_information_box mb_20">
                                <h2 class="pro_details_prise d-flex align-items-center  m-0">
                                    <span>£</span>
                                    <span>283.00</span>
                                </h2>
                                <div class="pro_details_disPrise d-flex align-items-center gap_15">
                                    <h4 class="discount_prise  m-0  "><span class="text-decoration-line-through">£</span> <span class="text-decoration-line-through"> 283.00</span> </h4>
                                    <span class="diccount_percents">
                                        -51%
                                    </span>
                                </div>
                                <p class="pro_details_text">+ Shipping from £180 to LEKKI-AJAH (SANGOTEDO)</p>
                            </div>
                            <div class="product_color_varient mb_20">
                                <h5 class="font_14 f_w_500 theme_text3  text-capitalize d-block mb_10" >Color:</h5>
                                <div class="color_List d-flex gap_5">
                                    <label class="round_checkbox blue_check  d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="round_checkbox paste_check  d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="round_checkbox violate_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="product_info">
                                <div class="size_quide_info d-flex alingn-items-center mb_20 flex-wrap">
                                    <a href="#"  data-bs-toggle="modal" data-bs-target="#asq_about_form" class="single_side_guide m-0">
                                        <img src="img/svg/chatting.svg" alt="#"> Ask about this product
                                    </a>
                                </div>
                                <div class="single_pro_varient">
                                    <h5 class="font_14 f_w_500 theme_text3 " >Quantity:</h5>
                                    <div class="product_number_count mr_5" data-target="amount-1">
                                        <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                        <input id="amount-1" class="count_single_item input-number" type="text" value="1">
                                        <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                                    </div>
                                    <!-- <a href="#" class="amaz_primary_btn style2  add_to_cart text-uppercase flex-fill text-center">Add to Cart</a> -->
                                    <div class="size_quide_info d-flex alingn-items-center mb-0 flex-wrap flex-fill">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#size_modal"  class="single_side_guide  text-nowrap">
                                            <img src="img/svg/size.svg" alt="#"> Size Guide
                                        </a>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#shiping_modal" class="single_side_guide  m-0 text-nowrap">
                                            <img src="img/svg/ship.svg" alt="#"> Shipping
                                        </a>
                                    </div>
                                </div>
                                <!-- <label class="primary_checkbox d-flex mb_30">
                                    <input type="checkbox">
                                    <span class="checkmark mr_10"></span>
                                    <span class="label_name">I agree with the terms and conditions.</span>
                                </label> -->
                                <div class="row mt_30 ">
                                    <div class="col-md-6">
                                    <a href="#" class="amaz_primary_btn style2 mb_20  add_to_cart text-uppercase flex-fill text-center w-100">Add to Cart</a>
                                    </div>
                                    <div class="col-md-6">
                                    <a href="#" class="amaz_primary_btn3 mb_20  w-100 text-center justify-content-center text-uppercase">Buy it now</a>
                                    </div>
                                </div>
                                
                                <div class="add_wish_compare d-flex alingn-items-center mb_20">
                                    <a href="#" class="single_wish_compare text-uppercase text-nowrap">
                                        <i class="ti-heart"></i> Add to Wishlist
                                    </a>
                                    <a href="compare.php" class="single_wish_compare text-uppercase text-nowrap">
                                        <i class="ti-control-shuffle"></i> Add to Compare
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_details_sujjetion">
                            <h4 class="font_14 f_w_700 text-uppercase mb_12 lh-1">YOU CAN ALSO BUY:</h4>
                            <div class="product_details_sujjetion_box">
                                <a href="#" class="product_details_sujjetion_single d-flex align-items-center gap_15">
                                    <div class="thumb">
                                        <img src="img/product_details/sujject_thumb.png" alt="">
                                    </div>
                                    <div class="product_details_sujjetion_content">
                                        <h4 class="fs-6 f_w_700">Jumia Protect - Device Insurance</h4>
                                        <p class="font_14 f_w_500 mb-0 lh-1">$320.00</p>
                                    </div>
                                </a>
                                <div class="product_details_sujjetion_total d-flex align-items-center gap_15">
                                    <div class="product_details_sujjetion_left flex-fill">
                                        <span class="font_12 f_w_500 d-block">Total Price:</span>
                                        <h4 class="font_16 f_w_700 m-0 lh-1">$380.00</h4>
                                    </div>
                                    <a href="#" class="amaz_primary_btn style3 text-uppercase">Buy Both</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="product_details_dec mb_76">
                            <div class="product_details_dec_header">
                                <h4 class="font_20 f_w_400 m-0 ">Product Details</h4>
                            </div>
                            <div class="product_details_dec_body">
                                <div class="product_details_dec_img mb_20">
                                    <img class="img-fluid" src="img/product_details/product_small_banner.png" alt="">
                                </div>
                                <div class="single_desc mb_25">
                                    <h5 class="font_18 f_w_400 mb_15 ">Description:</h5>
                                    <p class="mb_20">Powered by 4500mAh battery which lets you be more energetic in business, games,videos and reading. 5MP selfie camera allows you to enjoy your 
                                    spare time.Beside, HD Rear Camera 8MP  enable you get High quality shooting moments. The BIG 1 quipped with 6.26 inch FW+ IPS screen which 
                                    will bring you a big view on games,videos,reading.</p>
                                    <p>With 16GB of Rom which make you have more room to do more things you like, such games, videos,books and pictures. Thanks to fingerprint 
                                    recognition. You don’t need to remember a complex password,only your fingerprint can unlock the phone.</p>
                                </div>
                                <div class="single_desc style2">
                                    <h5 class="font_18 f_w_400 mb_10 ">Main Features:</h5>
                                    <p class="f_w_500 m-0">Display: 6.26 inch IPS FW+ waterdrop fullview screen</p>
                                    <p class="f_w_500 m-0">Memory: RAM 1GB / ROM16GB ( Expandable up to 32GB via microSD card)</p>
                                    <p class="f_w_500 m-0">Camera: Rear main camera 8.0MP with flashlight /Front camera 5.0MP</p>
                                    <p class="f_w_500 m-0">Battery: 4500 mAh battery</p>
                                    <p class="f_w_500 m-0">CPU: MTK6580-Quad Core 1.3 GHz</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <div class="section__title d-flex align-items-center gap-3 mb_30">
                                    <h3 class="m-0 flex-fill">Customers who viewed this also viewed</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_30">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_30">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6">
                                <div class="product_widget5 mb_30">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="product_reviews_wrapper">
                            <div class="product_reviews_wrapper_head d-flex align-items-center justify-content-between">
                                <h4 class="font_20 f_w_700 m-0">Customer Feedback</h4>
                                <a href="product.php" class="title_link d-flex align-items-center lh-1">
                                    <span class="title_text">View all</span>
                                    <span class="title_icon">
                                        <i class="fas fa-chevron-right"></i>
                                    </span>
                                </a>
                            </div>
                            <div class="course_cutomer_reviews">
                                <div class="course_feedback">
                                    <div class="course_feedback_left">
                                        <h2>4.7</h2>
                                        <div class="feedmak_stars">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <span>Course Rating</span>
                                    </div>
                                    <div class="feedbark_progressbar">
                                        <div class="single_progrssbar">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                                </div>
                                            </div>
                                            <div class="rating_percent d-flex align-items-center">
                                                <div class="feedmak_stars d-flex align-items-center">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                </div>
                                                <span>70%</span>
                                            </div>
                                        </div>
                                        <div class="single_progrssbar">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                                </div>
                                            </div>
                                            <div class="rating_percent d-flex align-items-center">
                                                <div class="feedmak_stars d-flex align-items-center">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                </div>
                                                <span>20%</span>
                                            </div>
                                        </div>
                                        <div class="single_progrssbar">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                                </div>
                                            </div>
                                            <div class="rating_percent d-flex align-items-center">
                                                <div class="feedmak_stars d-flex align-items-center">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                </div>
                                                <span>10%</span>
                                            </div>
                                        </div>
                                        <div class="single_progrssbar">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                                </div>
                                            </div>
                                            <div class="rating_percent d-flex align-items-center">
                                                <div class="feedmak_stars d-flex align-items-center">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                </div>
                                                <span>01%</span>
                                            </div>
                                        </div>
                                        <div class="single_progrssbar">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                                </div>
                                            </div>
                                            <div class="rating_percent d-flex align-items-center">
                                                <div class="feedmak_stars d-flex align-items-center">
                                                    <i class="fas fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                    <i class="far fa-star"></i>
                                                </div>
                                                <span>01%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="customers_reviews">
                                    <div class="single_reviews">
                                        <div class="thumb">
                                            ks
                                        </div>
                                        <div class="review_content">
                                            <div class="review_content_head d-flex justify-content-between align-items-start flex-wrap">
                                                <div class="review_content_head_left">
                                                    <h4 class="f_w_700 font_20" >Kristen Stewart</h4>
                                                    <div class="rated_customer d-flex align-items-center">
                                                        <div class="feedmak_stars">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <span>2 weeks ago</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <p>See-through delicate embroidered organza blue lining luxury acetate-mix stretch pleat detailing Leather detail shoulder contrastic colour contour stunni silhouette working peplum. Statement buttons cover-up tweaks patch pockets perennia lapel collar.</p>
                                        </div>
                                    </div>
                                    <div class="single_reviews">
                                        <div class="thumb">
                                            ks
                                        </div>
                                        <div class="review_content">
                                            <div class="review_content_head d-flex justify-content-between align-items-start flex-wrap">
                                                <div class="review_content_head_left">
                                                    <h4 class="f_w_700 font_20" >Kristen Stewart</h4>
                                                    <div class="rated_customer d-flex align-items-center">
                                                        <div class="feedmak_stars">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <span>2 weeks ago</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <p>See-through delicate embroidered organza blue lining luxury acetate-mix stretch pleat detailing Leather detail shoulder contrastic colour contour stunni silhouette working peplum. Statement buttons cover-up tweaks patch pockets perennia lapel collar.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="amazcart_delivery_wiz mb_20">
                    <div class="amazcart_delivery_wiz_head">
                        <h4 class="font_18 f_w_700 m-0">Delivery & Returns</h4>
                    </div>
                    <div class="amazcart_delivery_wiz_body">
                        <h4 class="font_16 f_w_700 mb_6">Infix Express Delivery</h4>
                        <p class="delivery_text font_14 f_w_400">Free Shipping on Jumia Express orders
                            above N12,000 in Lagos</p>
                    </div>
                </div>
                <div class="amazcart_delivery_wiz mb_20">
                    <div class="amazcart_delivery_wiz_head">
                        <h4 class="font_18 f_w_700 m-0">Choose your Location</h4>
                    </div>
                    <div class="amazcart_delivery_wiz_body">
                        <div class="loc_city_selectBox d-flex flex-column">
                            <div class="selectBox_box ">
                                <select class="amaz_select2 mb_10 w-100" >
                                    <option data-display="Choose City">Choose City</option>
                                    <option value="1">Dhaka</option>
                                    <option value="2">Rangpur</option>
                                    <option value="1">Dhaka</option>
                                    <option value="2">Rangpur</option>
                                </select>
                            </div>
                            <div class="selectBox_box">
                                <select class="amaz_select2 w-100" >
                                    <option data-display="Choose sub-location">Choose sub-location</option>
                                    <option value="1">Dhaka</option>
                                    <option value="2">Rangpur</option>
                                    <option value="1">Dhaka</option>
                                    <option value="2">Rangpur</option>
                                </select>
                            </div>
                        </div>
                        <div class="amazcart_delivery_wiz_sep d-flex gap_15 mb_10">
                            <div class="icon d-flex align-items-center justify-content-center ">
                                <img src="img/product_details/details_car.svg" alt="">
                            </div>
                            <div class="amazcart_delivery_wiz_content">
                                <h4 class="font_16 f_w_700 mb_6">Door Delivery</h4>
                                <p class="delivery_text font_14 f_w_400">Delivery by 16 December when you
                                    order within next 7hrs 44mins</p>
                            </div>
                        </div>
                        <div class="amazcart_delivery_wiz_sep d-flex gap_15 ">
                            <div class="icon d-flex align-items-center justify-content-center ">
                                <img src="img/product_details/details_pickup.svg" alt="">
                            </div>
                            <div class="amazcart_delivery_wiz_content">
                                <h4 class="font_16 f_w_700 mb_6">Pickup Station</h4>
                                <p class="delivery_text font_14 f_w_400 mb-0">Delivery by 16 December when you
                                    order within next 7hrs 44mins</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="amazcart_delivery_wiz mb_20">
                    <div class="amazcart_delivery_wiz_body">
                        <div class="amazcart_delivery_wiz_sep d-flex gap_15 mb_10">
                            <div class="icon d-flex align-items-center justify-content-center ">
                                <img src="img/product_details/details_pickup.svg" alt="">
                            </div>
                            <div class="amazcart_delivery_wiz_content">
                                <h4 class="font_16 f_w_700 mb_6">Return Policy</h4>
                                <p class="delivery_text font_14 f_w_400">Delivery by 16 December when you
                                    order within next 7hrs 44mins</p>
                            </div>
                        </div>
                        <div class="amazcart_delivery_wiz_sep d-flex gap_15 ">
                            <div class="icon d-flex align-items-center justify-content-center ">
                                <img src="img/product_details/details_warranty.svg" alt="">
                            </div>
                            <div class="amazcart_delivery_wiz_content">
                                <h4 class="font_16 f_w_700 mb_6">Warranty</h4>
                                <p class="delivery_text font_14 f_w_400 mb-0">Delivery by 16 December when you
                                    order within next 7hrs 44mins</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="amazcart_delivery_wiz mb_30">
                    <div class="amazcart_delivery_wiz_head">
                        <h4 class="font_18 f_w_700 m-0">Seller Information</h4>
                    </div>
                    <div class="amazcart_delivery_wiz_body">
                        <h4 class="font_14 f_w_700 mb-0">QUBO Official-CODx</h4>
                        <div class="Information_box d-flex gap-2 flex-wrap ">
                            <div class="Information_box_left flex-fill">
                                <div class="single_info_seller d-flex align-items-center gap_15">
                                    <h4 class="font_14 f_w_500 m-0">83%</h4>
                                    <p class="font_14 f_w_400 m-0">Seller Score</p>
                                </div>
                                <div class="single_info_seller d-flex align-items-center gap_15">
                                    <h4 class="font_14 f_w_500 m-0">2387</h4>
                                    <p class="font_14 f_w_400 m-0">Followers</p>
                                </div>
                            </div>
                            <div class="Information_box_right">
                                <a href="#" class="amaz_primary_btn style3 text-uppercase">Follow</a>
                            </div>
                        </div>
                        <div class="seller_performance_box">
                            <h4 class="font_14 f_w_700 text-uppercase ">Seller Performance</h4>
                            <div class="single_seller_performance d-flex align-items-center gap_10 mb-1">
                                <img src="img/product_details/star.svg" alt="">
                                <p class="font_14 f_w_400 m-0">Order Fulfilment Rate:</p>
                                <h4 class="font_14 f_w_500 m-0">Excellent</h4>
                            </div>
                            <div class="single_seller_performance d-flex align-items-center gap_10">
                                <img src="img/product_details/star.svg" alt="">
                                <p class="font_14 f_w_400 m-0">Order Fulfilment Rate:</p>
                                <h4 class="font_14 f_w_500 m-0">Excellent</h4>
                            </div>
                            <div class="single_seller_performance d-flex align-items-center gap_10">
                                <img src="img/product_details/star.svg" alt="">
                                <p class="font_14 f_w_400 m-0">Order Fulfilment Rate:</p>
                                <h4 class="font_14 f_w_500 m-0">Excellent</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- product_details_wrapper::end  -->


<!-- sujjested_prosuct_area::start  -->
<div class="sujjested_prosuct_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title d-flex align-items-center gap-3 mb_30">
                    <h3 class="m-0 flex-fill">Related Products</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_30">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/1.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                             <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_30">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/2.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                             <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_30">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/3.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                             <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_30">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/4.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                             <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- sujjested_prosuct_area::end  -->

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>