<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- faq_area::start  -->
<div class="faq_area section_spacing6">
    <div class="container">
        <!-- content  -->
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-7">
                <h3 class="font_30 f_w_700 mb_35">Frequently Asked Questions</h3>
                <div class="theme_according mb_30" id="accordion1">
                    <div class="card">
                        <div class="card-header pink_bg" id="headingFour">
                            <h5 class="mb-0">
                            <button class="btn btn-link text_white collapsed" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Are your product for commercial use?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseFour" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pink_bg" id="headingFive">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Placing your order to cancel or change your mind completely free or charge?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseFive" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pink_bg" id="headingSix">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">Which show our newest and best-selling pieces in real life?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix" aria-labelledby="headingSix" data-parent="#accordion1">
                            <div class="card-body">
                            <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pink_bg" id="headingSix4">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-bs-toggle="collapse" data-bs-target="#collapseSix4" aria-expanded="false" aria-controls="collapseSix">Can you provide a fabric sample?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix4" aria-labelledby="headingSix4" data-parent="#accordion1">
                            <div class="card-body">
                            <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pink_bg" id="headingSix5">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-bs-toggle="collapse" data-bs-target="#collapseSix5" aria-expanded="false" aria-controls="headingSix5">Do you have a furniture collection service?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix5" aria-labelledby="headingSix5" data-parent="#accordion1">
                            <div class="card-body">
                            <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pink_bg" id="headingSix6">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-bs-toggle="collapse" data-bs-target="#collapseSix6" aria-expanded="false" aria-controls="headingSix6">Do you have all your items on display in your showroom?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix6" aria-labelledby="headingSix6" data-parent="#accordion1">
                            <div class="card-body">
                            <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ content  -->
    </div>
</div>
<!-- faq_area::end  -->


<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>