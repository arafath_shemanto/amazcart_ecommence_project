<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<div class="amazy_dashboard_area dashboard_bg section_spacing6">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <?php include 'include/dashboard_sidebar.php' ?>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="dashboard_white_box style4 bg-white mb_25">
                    <div class="dashboard_white_box_header d-flex align-items-center gap_20 flex-wrap mb_35">
                        <h4 class="font_24 f_w_700 flex-fill m-0">Create New Tickets </h4>
                    </div>
                    <div class="dashboard_white_box_body">
                         <!-- form  -->
                         <form action="#">
                            <div class="row">
                                <div class="col-12">
                                    <label class="primary_label2 style2 ">Full Name <span>*</span></label>
                                    <input name="name" placeholder="E.g. Huge" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E.g. Huge'" class="primary_input3 style4 mb_20" required="" type="text">
                                </div>
                                <div class="col-xl-6">
                                    <label class="primary_label2 style2 ">Category<span>*</span></label>
                                    <select class="theme_select style2 wide mb_20" >
                                        <option value="#">E.g. Huge</option>
                                        <option value="#">E.g. Huge</option>
                                        <option value="#">E.g. Huge</option>
                                    </select>
                                </div>
                                <div class="col-xl-6">
                                    <label class="primary_label2 style2 ">Priority<span>*</span></label>
                                    <select class="theme_select style2 wide mb_20"  >
                                        <option value="#">E.g. Huge</option>
                                        <option value="#">E.g. Huge</option>
                                        <option value="#">E.g. Huge</option>
                                    </select>
                                </div>
                                <div class="col-12 mb_20">
                                    <label class="primary_label2 style2 ">Description</label>
                                    <textarea  placeholder="Write description here…" id="summernote" name="editordata" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Write description here… here…'" class="primary_textarea4 radius_5px mb_25" required=""></textarea>
                                </div>
                                <div class="col-12">
                                    <div class="tkt_uploader d-flex align-items-center gap_15 position-relative mb_20  justify-content-center">
                                    <input type="file" class=" position-absolute start-0 top-0 end-0 bottom-0 w-100 gj-cursor-pointer">
                                        <i class="fas fa-file-alt font_14 mute_text"></i>
                                        <p class="font_14 mute_text f_w_500 m-0">Drop your file here or <a href="#">choose files to upload</a></p>
                                    </div>
                                </div>
                                <div class="col-12 d-flex justify-content-end">
                                    <button class="amaz_primary_btn style2 rounded-0  text-uppercase  text-center w-100">+ create now</button>
                                </div>
                            </div>
                        </form>
                        <!--/ form  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>