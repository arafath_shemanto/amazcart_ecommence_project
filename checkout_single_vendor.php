<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- checkout_v3_area::start  -->
<div class="checkout_v3_area">
    <div class="checkout_v3_left d-flex justify-content-end">
        <div class="checkout_v3_inner">
            <div class="checout_head">
                <i class="ti-folder"></i>
                <p>Returning customer? <a data-bs-toggle="modal" data-bs-target="#checkot_login_form" href="#">Click here to login</a></p>
            </div>
            <div class="shiping_address_box checkout_form m-0">
                <div class="billing_address">
                    <h3 class="check_v3_title mb_25">Shipping Address</h3>
                    <form action="#" >
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="primary_label2 style3" >First Name</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="Enter your first and last name">
                            </div>
                            <div class="col-lg-6">
                                <label class="primary_label2 style3" >Last Name</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="Enter your first and last name">
                            </div>
                            <div class="col-lg-12">
                                <label class="primary_label2 style3" >Address</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="House# 122, Street# 245, ABC Road">
                            </div>
                            <div class="col-lg-12">
                                <label class="primary_label2 style3" >Address Line 2 (Optional)</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="Apartment, suite, etc. (optional)">
                            </div>
                            <div class="col-lg-6">
                                <label class="primary_label2 style3" >Country / Region</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="United Kingdom">
                            </div>
                            <div class="col-lg-6">
                                <label class="primary_label2 style3" >Postal Code</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="154327">
                            </div>
                            <div class="col-12 mb_25">
                                <label class="primary_checkbox d-flex">
                                    <input checked="" type="checkbox">
                                    <span class="checkmark mr_15"></span>
                                    <span class="label_name f_w_400 ">I agree with the terms and conditions.</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <div class="check_v3_btns flex-wrap d-flex align-items-center">
                                    <a href="product.php" class="amaz_primary_btn style2  min_200 text-center text-uppercase ">Continue to shipping</a>
                                    <a href="cart_v2.php" class="return_text">Return to cart</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="checkout_v3_right d-flex justify-content-start">
        <div class="order_sumery_box flex-fill">
            <h3 class="check_v3_title mb_25">Order Summary</h3>
            <div class="singleVendor_product_lists">
                <div class="singleVendor_product_list d-flex align-items-center">
                    <div class="thumb">
                        <img src="img/summery_img.png" alt="">
                    </div>
                    <div class="product_list_content">
                        <h4><a href="#">The Unbundled University</a></h4>
                        <h5 class="d-flex align-items-center"><span class="product_count_text">2<span>x</span></span>$4200.00</h5>
                    </div>
                </div>
            </div>
            <div class="subtotal_lists">
                <div class="single_total_list d-flex align-items-center">
                    <div class="single_total_left flex-fill">
                        <h4>Subtotal</h4>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 1324.35</span>
                    </div>
                </div>
                <div class="single_total_list d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <h4>Shipping Charge</h4>
                        <p>Package Wise Shipping Charge</p>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 75.35</span>
                    </div>
                </div>
                <div class="single_total_list d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <h4>Discount</h4>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 75.35</span>
                    </div>
                </div>
                <div class="total_amount d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <span class="total_text">Total (Incl. VAT)</span>
                    </div>
                    <div class="single_total_right">
                        <span class="total_text">USD <span>$1324.35</span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- checkout_v3_area::end  -->

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>