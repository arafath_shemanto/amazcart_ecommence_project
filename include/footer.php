
   

    <!-- shoping_cart::start  -->
    <div class="shoping_wrapper">
        <!-- <div class="dark_overlay"></div> -->
        <div class="shoping_cart">
            <div class="shoping_cart_inner">
                <div class="cart_header d-flex justify-content-between">
                    <div class="cart_header_text">
                        <h4>Shoping Cart</h4>
                        <p>03 Item’s selected</p>
                    </div>
                    
                    <div class="chart_close">   
                        <i class="ti-close"></i>
                    </div>
                </div>
                <div class="single_cart">
                    <div class="thumb d-flex align-items-center gap_10 mr_15">
                        <label class="primary_checkbox d-flex">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <div class="thumb_inner">
                            <img src="img/cart/quick_cart_view.png " alt="">
                        </div>
                    </div>
                    <div class="cart_content flex-fill">
                        <a href="product.php">
                            <h5>Wireless Headphone</h5>
                        </a>
                        <div class="cart_content_text d-flex align-items-center gap_10 flex-fill flex-wrap">
                            <div class="product_number_count style_2" data-target="amountc-1">
                                <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                <input id="amountc-1" class="count_single_item input-number" type="text" value="1" >
                                <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                            </div>
                            <p><span class="prise" >$420.00</span> </p>
                        </div>
                        
                    </div>
                    <div class="cart_trash_icon d-flex align-items-center  justify-content-end">
                        <img src="img/svg/trash.svg" alt="">
                    </div>
                </div>
                <div class="single_cart">
                    <div class="thumb d-flex align-items-center gap_10 mr_15">
                        <label class="primary_checkbox d-flex">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <div class="thumb_inner">
                            <img src="img/cart/quick_cart_view2.png " alt="">
                        </div>
                    </div>
                    <div class="cart_content flex-fill">
                        <a href="product.php ">
                            <h5>Wireless Headphone</h5>
                        </a>
                        <div class="cart_content_text d-flex align-items-center gap_10 flex-fill flex-wrap">
                            <div class="product_number_count style_2" data-target="amountc-2">
                                <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                <input id="amountc-2" class="count_single_item input-number" type="text" value="1" >
                                <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                            </div>
                            <p><span class="prise" >$420.00</span> </p>
                        </div>
                        
                    </div>
                    <div class="cart_trash_icon d-flex align-items-center  justify-content-end">
                        <img src="img/svg/trash.svg" alt="">
                    </div>
                </div>
                <div class="single_cart">
                    <div class="thumb d-flex align-items-center gap_10 mr_15">
                        <label class="primary_checkbox d-flex">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <div class="thumb_inner">
                            <img src="img/cart/quick_cart_view3.png " alt="">
                        </div>
                    </div>
                    <div class="cart_content flex-fill">
                        <a href="product.php">
                            <h5>Wireless Headphone</h5>
                        </a>
                        <div class="cart_content_text d-flex align-items-center gap_10 flex-fill flex-wrap">
                            <div class="product_number_count style_2" data-target="amountc-23">
                                <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                <input id="amountc-23" class="count_single_item input-number" type="text" value="1" >
                                <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                            </div>
                            <p><span class="prise" >$420.00</span> </p>
                        </div>
                        
                    </div>
                    <div class="cart_trash_icon d-flex align-items-center  justify-content-end">
                        <img src="img/svg/trash.svg" alt="">
                    </div>
                </div>
            </div>
            <div class="shoping_cart_subtotal d-flex justify-content-between align-items-center">
                <h4 class="m-0">Subtotal</h4>
                <span>$ 1324.35</span>
            </div>
            <div class="view_checkout_btn d-flex justify-content-end mb_30 flex-column gap_10">
                <a href="cart_v2.php" class="amaz_primary_btn style2 text-uppercase ">View cart</a>
                <a href="checkout_v2.php" class="amaz_primary_btn style2 text-uppercase ">Process to Checkout</a>
            </div>
        </div>
    </div>
    <!-- shoping_cart::end  -->

    <!-- Modal::start  -->
    <div class="modal fade theme_modal" id="theme_modal" tabindex="-1" role="dialog" aria-labelledby="theme_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="product_quick_view ">
                    <button type="button" class="close_modal_icon" data-bs-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                        <div class="product_details_img"></div>
                        <div class="product_details_wrapper">
                            <div class="product_content_details mb_30">
                                <p class="stock_text"> <span>SKU:</span> 2231</p>
                                <p class="stock_text"> <span>Availability:</span> 1 in stock</p>
                                <h3>East Hampton Fleece Hoodie</h3>
                                <h5 class="prise_text d-flex align-items-center">$440.00 <span>Tax included</span></h5>
                                <div class="product_ratings">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span>(8k+ reviews)</span>
                                </div>
                                <div class="product_color_varient mb_30">
                                    <h5 class="font_14 f_w_700 theme_text3  text-uppercase d-block mb_10" >Color:</h5>
                                    <div class="color_List d-flex gap_10">
                                        <label class="round_checkbox blue_check  d-flex">
                                            <input name="color_filt" type="radio">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="round_checkbox paste_check  d-flex">
                                            <input name="color_filt" type="radio">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="round_checkbox violate_check  d-flex">
                                            <input name="color_filt" type="radio">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="product_info">
                                    <div class="size_quide_info d-flex alingn-items-center mb_20 flex-wrap">
                                        <a href="#" class="single_side_guide m-0" data-bs-toggle="modal" data-bs-dismiss="modal" data-bs-target="#asq_about_form">
                                            <img src="img/svg/chatting.svg" alt="#"> Ask about this product
                                        </a>
                                    </div>
                                    <div class="single_pro_varient">
                                        <h5 class="font_14 f_w_500 theme_text3 " >Quantity:</h5>
                                        <div class="product_number_count mr_5" data-target="amount-10">
                                            <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                            <input id="amount-10" class="count_single_item input-number" type="text" value="1">
                                            <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                                        </div>
                                        <div class="size_quide_info d-flex alingn-items-center mb-0 flex-wrap">
                                            <a href="#" class="single_side_guide" data-bs-toggle="modal" data-bs-dismiss="modal" data-bs-target="#size_modal">
                                                <img src="img/svg/size.svg" alt="#"> Size Guide
                                            </a>
                                            <a href="#" class="single_side_guide" data-bs-toggle="modal" data-bs-dismiss="modal" data-bs-target="#shiping_modal">
                                                <img src="img/svg/ship.svg" alt="#"> Shipping
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row mt_30">
                                        <div class="col-md-6">
                                            <a href="#" class="home10_primary_btn2 mb_20 w-100 text-center add_to_cart text-uppercase flex-fill text-center">Add to Cart</a>
                                        </div>
                                        <div class="col-md-6">
                                        <a href="#" class="home10_primary_btn4  w-100 radius_5px mb_20 w-100 text-center justify-content-center text-uppercase">+ Buy it now</a>
                                        </div>
                                    </div>
                                   
                                    <div class="add_wish_compare d-flex alingn-items-center mb-0">
                                        <a href="#" class="single_wish_compare">
                                            <i class="ti-heart"></i> Add to Wishlist
                                        </a>
                                        <a href="compare.php" class="single_wish_compare">
                                            <i class="ti-control-shuffle"></i> Add to Compare
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal::end  -->

    <!-- about:start  -->
    <div class="modal fade login_modal about_modal" id="asq_about_form" tabindex="-1" role="dialog" aria-labelledby="asq_about_form" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                <div data-bs-dismiss="modal" class="close_modal">
                    <i class="ti-close"></i>
                </div>
                <!-- infix_login_area::start  -->
                    <div class="infix_login_area p-0">
                        <div class="login_area_inner">
                            <h3 class="sign_up_text mb_20 fs-5">Have A question?</h3>
                            <form action="#">
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea placeholder="Your Message" class="primary_textarea3 mb_20 bg-white" ></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <input placeholder="Your Name" type="text" class="primary_input3 mb_20 bg-white">
                                    </div>
                                    <div class="col-md-12">
                                        <input placeholder="Email" type="email" class="primary_input3 mb_20 bg-white">
                                    </div>
                                    <div class="col-md-12">
                                        <input placeholder="Your Phone" type="text" class="primary_input3 mb_30 bg-white">
                                    </div>
                                    <div class="col-12">
                                        <button class="home10_primary_btn2 text-center f_w_700">SEND MESSAGE</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- infix_login_area::end  -->
                    
                </div>
            </div>
        </div>
    </div>
    <!-- about:end  -->

    <!-- shiping_modal:start  -->
    <div class="modal fade login_modal shiping_modal" id="shiping_modal" tabindex="-1" role="dialog" aria-labelledby="shiping_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                <div data-bs-dismiss="modal" class="close_modal">
                    <i class="ti-close"></i>
                </div>
                <!-- infix_login_area::start  -->
                    <div class="infix_login_area p-0">
                        <div class="login_area_inner">
                            <h3 class="sign_up_text mb_10 fs-5 mt-0">SHIPPING</h3>
                            <ul class="ps-3 mb_30">
                                <li class="list_disc">Complimentary ground shipping within 1 to 7 business days</li>
                                <li class="list_disc">In-store collection available within 1 to 7 business days</li>
                                <li class="list_disc">Next-day and Express delivery options also available</li>
                                <li class="list_disc">Purchases are delivered in an orange box tied with a Bolduc ribbon, with the exception of certain items</li>
                                <li class="list_disc">See the delivery FAQs for details on shipping methods, costs and delivery times</li>
                                <li class="list_disc">Easy and complimentary, within 14 days</li>
                            </ul>
                            <h3 class="sign_up_text mb_10 fs-5 mt-0">RETURNS AND EXCHANGES</h3>
                            <ul  class="ps-3">
                                <li class="list_disc">Easy and complimentary, within 14 days</li>
                                <li class="list_disc">See conditions and procedure in our return FAQs</li>
                            </ul>
                        </div>
                    </div>
                    <!-- infix_login_area::end  -->
                    
                </div>
            </div>
        </div>
    </div>
    <!-- shiping_modal:end  -->

    <!-- size_modal:start  -->
    <div class="modal fade login_modal size_modal" id="size_modal" tabindex="-1" role="dialog" aria-labelledby="size_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                <div data-bs-dismiss="modal" class="close_modal">
                    <i class="ti-close"></i>
                </div>
                <!-- infix_login_area::start  -->
                    <div class="infix_login_area p-0">
                        <div class="login_area_inner text-center">
                            <h3 class="theme_text3  mb-1 fs-4 f_w_700">SIZE GUIDE</h3>
                            <p class="mb_10">This is an approximate conversion table to help you find your size.</p>
                            <div class="table-responsive">
                                <table class="table size_table mb-0">
                                    <thead>
                                        <tr>
                                            <th>Italian</th>
                                            <th>Spanish</th>
                                            <th>German</th>
                                            <th>UK</th>
                                            <th>USA</th>
                                            <th>Japanese</th>
                                            <th>Chinese</th>
                                            <th>Russian</th>
                                            <th>Korean</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>34</td>
                                            <td>30</td>
                                            <td>28</td>
                                            <td>04</td>
                                            <td>00</td>
                                            <td>3</td>
                                            <td>155/75A</td>
                                            <td>36</td>
                                            <td>44</td>
                                        </tr>
                                        <tr>
                                            <td>34</td>
                                            <td>30</td>
                                            <td>28</td>
                                            <td>04</td>
                                            <td>00</td>
                                            <td>3</td>
                                            <td>155/75A</td>
                                            <td>36</td>
                                            <td>44</td>
                                        </tr>
                                        <tr>
                                            <td>34</td>
                                            <td>30</td>
                                            <td>28</td>
                                            <td>04</td>
                                            <td>00</td>
                                            <td>3</td>
                                            <td>155/75A</td>
                                            <td>36</td>
                                            <td>44</td>
                                        </tr>
                                        <tr>
                                            <td>34</td>
                                            <td>30</td>
                                            <td>28</td>
                                            <td>04</td>
                                            <td>00</td>
                                            <td>3</td>
                                            <td>155/75A</td>
                                            <td>36</td>
                                            <td>44</td>
                                        </tr>
                                        <tr>
                                            <td>34</td>
                                            <td>30</td>
                                            <td>28</td>
                                            <td>04</td>
                                            <td>00</td>
                                            <td>3</td>
                                            <td>155/75A</td>
                                            <td>36</td>
                                            <td>44</td>
                                        </tr>
                                        <tr>
                                            <td>34</td>
                                            <td>30</td>
                                            <td>28</td>
                                            <td>04</td>
                                            <td>00</td>
                                            <td>3</td>
                                            <td>155/75A</td>
                                            <td>36</td>
                                            <td>44</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- infix_login_area::end  -->
                    
                </div>
            </div>
        </div>
    </div>
    <!-- size_modal:end  -->

    <!-- checkout_login_form:start -->
    <div class="modal fade login_modal" id="checkot_login_form" tabindex="-1" role="dialog" aria-labelledby="checkot_login_form" aria-hidden="true">
        <div class="modal-dialog style2 modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div data-bs-dismiss="modal" class="close_modal">
                        <i class="ti-close"></i>
                    </div>
                    <!-- amaz_checkout_loginArea::start  -->
                    <div class="amaz_checkout_loginArea p-0">
                        <div class="login_area_inner">
                            <h4 class="text-start">Welcome back <br>
                            Please login to your account </h4>
                            <form action="#">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="input-group custom_group_field mb_35">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <img src="img/my_account/email.svg" alt="">
                                                </span>
                                            </div>
                                            <input type="email" class="form-control" placeholder="E.g. example@gmail.com" aria-label="E.g. example@gmail.com" >
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="input-group custom_group_field ">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <img src="img/my_account/pass.svg" alt="">
                                                </span>
                                            </div>
                                            <input type="password" class="form-control" placeholder="Enter Password" aria-label="Enter Password" >
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="remember_pass mb_40">
                                            <label class="primary_checkbox d-flex">
                                                <input checked="" type="checkbox">
                                                <span class="checkmark mr_15"></span>
                                                <span class="label_name">Remember Me</span>
                                            </label>
                                            <a class="forgot_pass" href="#">Forgot Password</a>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button class="amaz_primary_btn style2 radius_5px  w-100 text-uppercase  text-center">Sign In</button>
                                    </div>
                                    <div class="form_sep d-flex align-items-center">
                                        <span class="sep_line flex-fill"></span>
                                        <span class="form_sep_text font_14 f_w_700 text-uppercase ">OR</span>
                                        <span class="sep_line flex-fill"></span>
                                    </div>
                                    <div class="col-12">
                                        <button data-bs-toggle="modal" data-bs-dismiss="modal" data-bs-target="#checkot_login_form_reg" class="amaz_primary_btn2  style2 radius_5px text-center  w-100 text-uppercase text-center justify-content-center">Register Now</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- amaz_checkout_loginArea::end  -->
                </div>
            </div>
        </div>
    </div>
    <!-- checkout_login_form:end  -->

    <!-- checkot_login_form_reg:start -->
    <div class="modal fade login_modal" id="checkot_login_form_reg" tabindex="-1" role="dialog" aria-labelledby="checkot_login_form_reg" aria-hidden="true">
        <div class="modal-dialog style2 modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div data-bs-dismiss="modal" class="close_modal">
                        <i class="ti-close"></i>
                    </div>
                    <!-- amaz_checkout_loginArea::start  -->
                    <div class="amaz_checkout_loginArea p-0">
                        <div class="login_area_inner">
                            <h4 class="text-start">Welcome! <br> Create an account within a minute.</h4>
                            <form action="#">
                                <div class="row">
                                    <div class="col-12">
                                        <input name="name" placeholder="Enter Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Name'"  class="primary_line_input mb_20" required="" type="text">

                                        <input name="email" placeholder="Type e-mail address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"
                                            onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail address'" class="primary_line_input mb_10" required="" type="email">
                                        <input name="password" placeholder="Enter password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter password'"
                                            class="primary_line_input mb-0" required="" type="password">
                                        <input name="password" placeholder="Re-enter password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Re-enter password'" class="primary_line_input mb_10" required="" type="password">
                                    </div>
                                    <div class="col-12">
                                        <div class="remember_pass mb_55 justify-content-start">
                                            <label class="primary_checkbox d-flex ">
                                                <input checked="" type="checkbox">
                                                <span class="checkmark mr_15"></span>
                                            </label>
                                            <p class="font_14 f_w_500 mb-0 check_text">By signing up, you agree to <a class="text_underline" href="#"> Terms of Service</a> and <a class="text_underline" >Privacy Policy.</a></p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button class="amaz_primary_btn style2 radius_5px  w-100 text-uppercase  text-center">Sign Up</button>
                                    </div>
                                    <div class="col-12">
                                        <p class="sign_up_text text-center" >Already have an account? <a data-bs-toggle="modal" data-bs-dismiss="modal" href="#" data-bs-target="#checkot_login_form" >Login</a></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- amaz_checkout_loginArea::end  -->
                </div>
            </div>
        </div>
    </div>
    <!-- checkot_login_form_reg:end  -->

    <!-- purchase_history_modal::start  -->
    <div class="modal fade theme_modal2" id="purchase_history_modal" tabindex="-1" role="dialog" aria-labelledby="theme_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="summery_modal_body">
                        <div class="summery_modal_header d-flex align-items-center gap-2 flex-wrap">
                            <h5 class="font_16 f_w_700 m-0 flex-fill">Order code : 20211228-06450123</h5>
                            <button type="button" class="close_modal_icon" data-bs-dismiss="modal">
                                <i class="ti-close"></i>
                            </button>
                        </div>
                        <div class="summery_modal_body_inner ">
                            <div class="order_place_progress mb_30">
                                <div class="single_order_progress position-relative d-flex align-items-center flex-column">
                                    <div class="icon position-relative ">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                                            <g  data-name="1" transform="translate(-613 -335)">
                                                <circle  data-name="Ellipse 239" cx="15" cy="15" r="15" transform="translate(613 335)" fill="#50cd89"/>
                                                <path id="Path_4193" data-name="Path 4193" d="M95.541,18.379a1.528,1.528,0,0,1-1.16-.533l-3.665-4.276a1.527,1.527,0,0,1,2.319-1.988l2.4,2.8L103,5.245c1.172-1.642,2.4-.733,1.222.916L96.784,17.739a1.528,1.528,0,0,1-1.175.638Z" transform="translate(530.651 338.622)" fill="#fff"/>
                                            </g>
                                        </svg>
                                    </div>
                                    <h5 class="font_14 f_w_500 m-0 text-nowrap">Order Placed</h5>
                                </div>
                                <div class="single_order_progress position-relative d-flex align-items-center flex-column">
                                    <div class="icon position-relative ">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                                        <g  data-name="1" transform="translate(-613 -335)">
                                            <g  data-name="Ellipse 239" transform="translate(613 335)" fill="none" stroke="#50cd89" stroke-width="2">
                                            <circle cx="15" cy="15" r="15" stroke="none"/>
                                            <circle cx="15" cy="15" r="14" fill="none"/>
                                            </g>
                                            <circle  data-name="Ellipse 240" cx="5" cy="5" r="5" transform="translate(623 345)" fill="#50cd89"/>
                                        </g>
                                    </svg>

                                    </div>
                                    <h5 class="font_14 f_w_500 m-0 text-nowrap">Confirmed</h5>
                                </div>
                                <div class="single_order_progress position-relative d-flex align-items-center flex-column">
                                    <div class="icon position-relative ">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                                        <g  data-name="1" transform="translate(-613 -335)">
                                            <g  data-name="Ellipse 239" transform="translate(613 335)" fill="none" stroke="#f1ece8" stroke-width="2">
                                            <circle cx="15" cy="15" r="15" stroke="none"/>
                                            <circle cx="15" cy="15" r="14" fill="none"/>
                                            </g>
                                            <circle  data-name="Ellipse 240" cx="5" cy="5" r="5" transform="translate(623 345)" fill="#f1ece8"/>
                                        </g>
                                    </svg>
                                    </div>
                                    <h5 class="font_14 f_w_500 m-0 mute_text  text-nowrap">Processed</h5>
                                </div>
                                <div class="single_order_progress position-relative d-flex align-items-center flex-column">
                                    <div class="icon position-relative ">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                                            <g  data-name="1" transform="translate(-613 -335)">
                                                <g  data-name="Ellipse 239" transform="translate(613 335)" fill="none" stroke="#f1ece8" stroke-width="2">
                                                <circle cx="15" cy="15" r="15" stroke="none"/>
                                                <circle cx="15" cy="15" r="14" fill="none"/>
                                                </g>
                                                <circle  data-name="Ellipse 240" cx="5" cy="5" r="5" transform="translate(623 345)" fill="#f1ece8"/>
                                            </g>
                                        </svg>

                                    </div>
                                    <h5 class="font_14 f_w_500 m-0 mute_text text-nowrap">Shipped</h5>
                                </div>
                                <div class="single_order_progress position-relative d-flex align-items-center flex-column">
                                    <div class="icon position-relative ">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                                            <g  data-name="1" transform="translate(-613 -335)">
                                                <g  data-name="Ellipse 239" transform="translate(613 335)" fill="none" stroke="#f1ece8" stroke-width="2">
                                                <circle cx="15" cy="15" r="15" stroke="none"/>
                                                <circle cx="15" cy="15" r="14" fill="none"/>
                                                </g>
                                                <circle  data-name="Ellipse 240" cx="5" cy="5" r="5" transform="translate(623 345)" fill="#f1ece8"/>
                                            </g>
                                        </svg>
                                    </div>
                                    <h5 class="font_14 f_w_500 m-0 mute_text text-nowrap">Delivered</h5>
                                </div>
                            </div>
                            <!-- summery_order_box  -->
                            <div class="summery_order_box">
                                <div class="summery_modal_bodyHeader">
                                    <h5 class="font_16 f_w_700 m-0">Order Summary</h5>
                                </div>
                                <div class="summery_order_body d-flex flex-wrap">
                                    <div class="summery_lists">
                                        <div class="single_summery_list d-flex align-items-start gap_20">
                                            <div class="order_text_head d-flex align-items-center justify-content-between font_14 f_w_500 "><h5 class="font_14 f_w_500 m-0">Order code</h5><span>:</span>
                                            </div>
                                            <p class="font_14 f_w_400 m-0">20211228-06450123</p>
                                        </div>
                                        <div class="single_summery_list d-flex align-items-start gap_20">
                                            <div class="order_text_head d-flex align-items-center justify-content-between font_14 f_w_500 "><h5 class="font_14 f_w_500 m-0">Name</h5><span>:</span>
                                            </div>
                                            <p class="font_14 f_w_400 m-0">Christina Ashens</p>
                                        </div>
                                        <div class="single_summery_list d-flex align-items-start gap_20">
                                            <div class="order_text_head d-flex align-items-center justify-content-between font_14 f_w_500 "><h5 class="font_14 f_w_500 m-0">Email </h5><span>:</span>
                                            </div>
                                            <p class="font_14 f_w_400 m-0">info@spondonit.com</p>
                                        </div>
                                        <div class="single_summery_list d-flex align-items-start gap_20">
                                            <div class="order_text_head d-flex align-items-center justify-content-between font_14 f_w_500 "><h5 class="font_14 f_w_500 m-0">Delivery type</h5><span>:</span>
                                            </div>
                                            <p class="font_14 f_w_400 m-0">Express</p>
                                        </div>
                                        <div class="single_summery_list d-flex align-items-start gap_20">
                                            <div class="order_text_head d-flex align-items-center justify-content-between font_14 f_w_500 "><h5 class="font_14 f_w_500 m-0">Shipping Address</h5><span>:</span>
                                            </div>
                                            <p class="font_14 f_w_400 m-0">3977 Duke Lane, 5520 Alabaster United States 234780</p>
                                        </div>
                                    </div>
                                    <div class="summery_lists">
                                        <div class="single_summery_list d-flex align-items-start gap_20">
                                            <div class="order_text_head d-flex align-items-center justify-content-between font_14 f_w_500 "><h5 class="font_14 f_w_500 m-0">Order code</h5><span>:</span>
                                            </div>
                                            <p class="font_14 f_w_400 m-0">20211228-06450123</p>
                                        </div>
                                        <div class="single_summery_list d-flex align-items-start gap_20">
                                            <div class="order_text_head d-flex align-items-center justify-content-between font_14 f_w_500 "><h5 class="font_14 f_w_500 m-0">Name</h5><span>:</span>
                                            </div>
                                            <p class="font_14 f_w_400 m-0">Christina Ashens</p>
                                        </div>
                                        <div class="single_summery_list d-flex align-items-start gap_20">
                                            <div class="order_text_head d-flex align-items-center justify-content-between font_14 f_w_500 "><h5 class="font_14 f_w_500 m-0">Email </h5><span>:</span>
                                            </div>
                                            <p class="font_14 f_w_400 m-0">info@spondonit.com</p>
                                        </div>
                                        <div class="single_summery_list d-flex align-items-start gap_20">
                                            <div class="order_text_head d-flex align-items-center justify-content-between font_14 f_w_500 "><h5 class="font_14 f_w_500 m-0">Delivery type</h5><span>:</span>
                                            </div>
                                            <p class="font_14 f_w_400 m-0">Express</p>
                                        </div>
                                        <div class="single_summery_list d-flex align-items-start gap_20">
                                            <div class="order_text_head d-flex align-items-center justify-content-between font_14 f_w_500 "><h5 class="font_14 f_w_500 m-0">Shipping Address</h5><span>:</span>
                                            </div>
                                            <p class="font_14 f_w_400 m-0">3977 Duke Lane, 5520 Alabaster United States 234780</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sumery_product_details">
                                <div class="summery_modal_bodyHeader">
                                    <h5 class="font_16 f_w_700 m-0">Order Details</h5>
                                </div>
                                <div class="table-responsive mb_30">
                                    <table class="table amazy_table3 mb-0">
                                        <thead>
                                            <tr>
                                            <th class="font_14 f_w_700" scope="col">Products</th>
                                            <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">Price</th>
                                            <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">QUANTITY</th>
                                            <th class="font_14 f_w_700 border-start-0 border-end-0" scope="col">Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$3200.00</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 2</h4>
                                                </td>
                                                <td>
                                                   <div class="d-flex align-items-center gap_10">
                                                       <h5 class="font_16 f_w_500 m-0">$4,00.00</h5>
                                                       <svg xmlns="http://www.w3.org/2000/svg" width="12.249" height="15.076" viewBox="0 0 12.249 15.076">
                                                        <g id="trash" transform="translate(-48)">
                                                            <path id="Path_1449" data-name="Path 1449" d="M59.071,1.884H56.48V1.413A1.415,1.415,0,0,0,55.067,0H53.182a1.415,1.415,0,0,0-1.413,1.413v.471H49.178A1.179,1.179,0,0,0,48,3.062V4.711a.471.471,0,0,0,.471.471h.257l.407,8.547a1.412,1.412,0,0,0,1.412,1.346H57.7a1.412,1.412,0,0,0,1.412-1.346l.407-8.547h.257a.471.471,0,0,0,.471-.471V3.062A1.179,1.179,0,0,0,59.071,1.884Zm-6.36-.471a.472.472,0,0,1,.471-.471h1.884a.472.472,0,0,1,.471.471v.471H52.711ZM48.942,3.062a.236.236,0,0,1,.236-.236h9.893a.236.236,0,0,1,.236.236V4.24H48.942Zm9.23,10.623a.471.471,0,0,1-.471.449H50.547a.471.471,0,0,1-.471-.449l-.4-8.5h8.905Z" fill="#687083"/>
                                                            <path id="Path_1450" data-name="Path 1450" d="M240.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,240.471,215.067Z" transform="translate(-186.347 -201.875)" fill="#687083"/>
                                                            <path id="Path_1451" data-name="Path 1451" d="M320.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,320.471,215.067Z" transform="translate(-263.991 -201.875)" fill="#687083"/>
                                                            <path id="Path_1452" data-name="Path 1452" d="M160.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,0,0-.942,0V214.6A.471.471,0,0,0,160.471,215.067Z" transform="translate(-108.702 -201.875)" fill="#687083"/>
                                                        </g>
                                                        </svg>
                                                   </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="subtotal_and_payment_information d-flex flex-wrap gap-3">
                                <div class="thumb flex-fill">
                                    <img class="img-fluid" src="img/amazPorduct/cash_on_delivery.png " alt="">
                                </div>
                                <div class="total_sumery_amount">
                                    <div class="single_amount pb-1 d-flex align-items-center justify-content-between">
                                        <h5 class="font_16 f_w_700 m-0">Subtotal</h5>
                                        <p class="font_14 f_w_500 m-0">$1324.35</p>
                                    </div>
                                    <div class="single_amount pb-1 d-flex align-items-center justify-content-between">
                                        <h5 class="font_16 f_w_700 m-0">Delivery Fee</h5>
                                        <p class="font_14 f_w_500 m-0">$75.35</p>
                                    </div>
                                    <div class="single_amount d-flex align-items-center justify-content-between">
                                        <h5 class="font_16 f_w_700 m-0">Discount</h5>
                                        <p class="font_14 f_w_500 m-0">+ USD 206.35</p>
                                    </div>
                                    <div class="amazy_bb mt_20  mb_28"></div>
                                    <div class="single_amount d-flex align-items-center justify-content-between">
                                        <h5 class="font_14 f_w_700 m-0">Total (Incl. VAT)</h5>
                                        <h5 class="font_14 f_w_700 m-0">+ USD 1324.35</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- purchase_history_modal::end  -->

    <!-- wallet_modal::start  -->
    <div class="modal fade theme_modal2" id="recharge_wallet" tabindex="-1" role="dialog" aria-labelledby="theme_modal" aria-hidden="true">
        <div class="modal-dialog max_width_700 modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="payment_modal_wallet">
                        <h3 class="font_24 f_w_700 mb_20">Payment Options</h3>
                        <div class="wallet_payent_box">
                            <button class="wallet_elemnt">
                                <img class="img-fluid" src="img/amazy_wallet/1.png " alt="">
                            </button>
                            <button class="wallet_elemnt">
                                <img class="img-fluid" src="img/amazy_wallet/2.png " alt="">
                            </button>
                            <button class="wallet_elemnt">
                                <img class="img-fluid" src="img/amazy_wallet/3.png " alt="">
                            </button>
                            <button class="wallet_elemnt">
                                <img class="img-fluid" src="img/amazy_wallet/4.png " alt="">
                            </button>
                            <button class="wallet_elemnt">
                                <img class="img-fluid" src="img/amazy_wallet/5.png " alt="">
                            </button>
                            <button class="wallet_elemnt">
                                <img class="img-fluid" src="img/amazy_wallet/6.png " alt="">
                            </button>
                        </div>
                        <div class="amazy_dashBorder mb_28 mt_30"> </div>
                        <h3 class="font_24 f_w_700 mb_18">Recharge Amount</h3>
                        <input name="name" placeholder="Enter Recharge Amount" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Recharge Amount'" class="primary_input3 rounded-0 style2  mb_15" required="" type="text">
                        <div class="d-flex justify-content-end gap_30 align-items-center">
                            <h5 class="font_14 f_w_700 text-uppercase gj-cursor-pointer m-0" data-bs-dismiss="modal">Cancel</h5>
                            <button class="amaz_primary_btn style2 text-nowrap ">Add fund</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- wallet_modal::end  -->

    <!-- wallet_modal::start  -->
    <div class="modal fade theme_modal2" id="Address_modal" tabindex="-1" role="dialog" aria-labelledby="theme_modal" aria-hidden="true">
        <div class="modal-dialog max_width_570 modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    
                    <div class="payment_modal_wallet style2">
                        <div class="d-flex align-items-center gap_10 mb_30">
                            <h3 class="font_24 f_w_700  flex-fill mb-0">Add/Update Address</h3>
                            <button type="button" class="close_modal_icon" data-bs-dismiss="modal">
                                <i class="ti-close"></i>
                            </button>
                        </div>
                        
                        <label class="primary_label2 style4 mb_15">Type</label>
                        <div class="address_type d-flex align-items-center gap_30 flex-wrap mb_30">
                            <label class="primary_checkbox style6 d-flex" >
                                <input type="radio" name="addRadio">
                                <span class="checkmark mr_10"></span>
                                <span class="label_name f_w_500">Billing</span>
                            </label>
                            <label class="primary_checkbox style6 d-flex" >
                                <input type="radio" name="addRadio">
                                <span class="checkmark mr_10"></span>
                                <span class="label_name f_w_500">Shipping</span>
                            </label>
                        </div>
                        <form action="#">
                            <div class="row">
                                <div class="col-12">
                                    <label class="primary_label2 style4 ">Title</label>
                                    <input name="name" placeholder="Enter title here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E.g. Huge'" class="primary_input3 radius_3px style6 mb_25" required="" type="text">
                                </div>
                                <div class="col-xl-6">
                                    <label class="primary_label2 style4 ">Country</label>
                                    <input name="name" placeholder="Country" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Country'" class="primary_input3 radius_3px style6 mb_25" required="" type="text">
                                </div>
                                <div class="col-xl-6">
                                    <label class="primary_label2 style4 ">City</label>
                                    <input name="name" placeholder="City" onfocus="this.placeholder = ''" onblur="this.placeholder = 'City'" class="primary_input3 radius_3px style6 mb_25" required="" type="text">
                                </div>
                                <div class="col-xl-6">
                                    <label class="primary_label2 style4 ">State</label>
                                    <input name="name" placeholder="State" onfocus="this.placeholder = ''" onblur="this.placeholder = 'State'" class="primary_input3 radius_3px style6 mb_25" required="" type="text">
                                </div>
                                <div class="col-xl-6">
                                    <label class="primary_label2 style4 ">Zip Code</label>
                                    <input name="name" placeholder="Zip Code" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Zip Code'" class="primary_input3 radius_3px style6 mb_25" required="" type="text">
                                </div>
                                <div class="col-12">
                                    <label class="primary_label2 style4 ">Street Address</label>
                                    <textarea name="name" placeholder="Enter street address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter street address'" class="primary_textarea4 radius_5px mb_25" required=""></textarea>
                                </div>
                                <div class="col-12 d-flex justify-content-end">
                                    <button class="amaz_primary_btn style2 radius_5px w-100 text-center  text-uppercase  text-center min_200">update now</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- wallet_modal::end  -->

    <!-- wallet_modal::start  -->
    <div class="modal fade theme_modal2" id="cart_add_modal" tabindex="-1" role="dialog" aria-labelledby="theme_modal" aria-hidden="true">
        <div class="modal-dialog max_width_430 modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="add_cart_modalAdded">
                        <button type="button" class="close_modal_icon" data-bs-dismiss="modal">
                            <i class="ti-close"></i>
                        </button>
                        <div class="product_checked_box d-flex flex-column justify-content-center align-items-center">
                            <svg id="checked" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                                <g id="Group_1587" data-name="Group 1587" transform="translate(7.118 3.77)">
                                    <g id="Group_1586" data-name="Group 1586">
                                    <path id="Path_3246" data-name="Path 3246" d="M143.592,64.66a1.131,1.131,0,0,0-1.6,0L128.426,78.189l-4.895-5.316a1.131,1.131,0,0,0-1.664,1.532l5.692,6.182a1.13,1.13,0,0,0,.808.365h.024a1.132,1.132,0,0,0,.8-.33l14.4-14.363A1.131,1.131,0,0,0,143.592,64.66Z" transform="translate(-121.568 -64.327)" fill="#4cb473"/>
                                    </g>
                                </g>
                                <g id="Group_1589" data-name="Group 1589">
                                    <g id="Group_1588" data-name="Group 1588">
                                    <path id="Path_3247" data-name="Path 3247" d="M28.869,13.869A1.131,1.131,0,0,0,27.739,15,12.739,12.739,0,1,1,15,2.261,1.131,1.131,0,1,0,15,0,15,15,0,1,0,30,15,1.131,1.131,0,0,0,28.869,13.869Z" fill="#4cb473"/>
                                    </g>
                                </g>
                            </svg>
                            <h4>Item added to your cart</h4>
                        </div>
                        <div class="cart_added_box">
                            <a href="product_details.php" class="cart_added_box_item d-flex align-items-center gap_25 flex-sm-wrap flex-md-nowrap">
                                <div class="thumb">
                                    <img class="img-fluid" src="img/cart_added_thumb.png" alt="">
                                </div>
                                <div class="cart_added_content">
                                    <h4>Strategic Social Media and Marketing Policy</h4>
                                    <h5>$290.00</h5>
                                </div>
                            </a>
                        </div>
                        <div class="d-flex flex-column gap_10">
                            <a href="cart_v2.php" class="amaz_primary_btn style2 text-uppercase ">View cart</a>
                            <a href="checkout_v2.php" class="amaz_primary_btn style2 text-uppercase ">Process to Checkout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- wallet_modal::end  -->


    <!-- UP_ICON  -->
    <div id="back-top" style="display: none;">
        <a title="Go to Top" href="#">
            <i class="ti-angle-up"></i>
        </a>
    </div>
    <!--/ UP_ICON -->

    <!--ALL JS SCRIPTS -->
    <script src="js/vendor/jquery-3.4.1.min.js"></script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/swiper-bundle.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/barfiller.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/parallax.js"></script>
     <script src="js/gijgo.min.js"></script>
     <script src="js/slick.min.js"></script>
     <script src="js/eleveti_zoom.js"></script>
     <script src="js/perfect-scrollbar.js"></script>
     <script src="js/jquery.nav.js"></script>
     <script src="js/summernote-lite.min.js"></script>
    <?php if (basename($_SERVER['PHP_SELF']) != 'profile.php') {?>
    <script src="js/query-ui.js"></script>
    <?php }?>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfpGBFn5yRPvJrvAKoGIdj1O1aO9QisgQ"></script>
    <script src="js/map.js"></script>
    <!-- MAIN JS   -->
    <script src="js/main.js"></script>

</body>

</html>