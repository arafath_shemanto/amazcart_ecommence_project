    <!-- FOOTER::START  -->
    <footer class="home_one_footer">
        <div class="main_footer_wrap">
            <div class="container">
                 <div class="row">
                    <div class="col-xl-2 col-lg-3 col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Information</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="my_order.php">Delivery information</a></li>
                                <li><a href="faq.php">Privacy Policy</a></li>
                                <li><a href="product.php">Sales</a></li>
                                <li><a href="faq.php">Terms & Conditions</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Account</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="about.php">About Us</a></li>
                                <li><a href="product.php">Brands</a></li>
                                <li><a href="product.php">Gift Vouchers</a></li>
                                <li><a href="contact.php">Site Map</a></li>
                                <li><a href="product.php">Accessories</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-2  col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>STORE</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="product.php">Affiliate</a></li>
                                <li><a href="product.php">Bestsellers</a></li>
                                <li><a href="product.php">Discount</a></li>
                                <li><a href="product.php">Latest products</a></li>
                                <li><a href="product.php">Sale</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xl-3 col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Need help</h3>
                            </div>
                            <h4 class="large_address">
                                0020 500 - Infix - 000
                            </h4>
                            <ul class="footer_links mb_20">
                                <li><a href="#">Monday - Friday: 9:00 - 20:00</a></li>
                                <li><a href="#">Saturday: 11:00 - 15:00</a></li>
                            </ul>
                            <a href="#" class="green_round_btn">info@codethemes.com</a>
                        </div>
                    </div>
                    <div class="col-lg-2  col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Our Store</h3>
                            </div>
                            <p class="address_text">1487 Rocky Horse Carrefour 
                                Arlington, TX 16819</p>
                            <div class="social__Links">
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright_area">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="copy_right_text">
                            <p>&copy; <span id="date_dynamic"></span> <a href="#"> Infix Cosmetics</a>. All rights reserved. Made By <a href="https://codecanyon.net/user/codethemes/portfolio">CodeThemes</a>.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="payment_imgs ">
                            <img src="img/payment_img.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER::END  -->

