    <!-- FOOTER::START  -->
    <footer class="home_three_footer">
        <div class="main_footer_wrap">
            <div class="container">
                 <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="footer_widget" >
                            <ul class="footer_links">
                                <li><a href="product.php">My Account</a></li>
                                <li><a href="order_tracking.php">Order Status</a></li>
                                <li><a href="shoping_v2.php">Shipping</a></li>
                                <li><a href="dashboard_refund.php">Returns & Exchanges</a></li>
                                <li><a href="merchant_register.php">Marhcant regiuster</a></li>
                                <li><a href="terms_conditions.php">Terms and condition </a></li>
                                <li><a href="dashboard_refund.php">Refund policy </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="footer_widget">
                            <ul class="footer_links">
                                <li><a href="#">Customer Service</a></li>
                                <li><a href="payment_v2.php">Billing</a></li>
                                <li><a href="terms_conditions.php">Accessibility</a></li>
                                <li><a href="shoping_v2.php">International Shipments</a></li>
                                <li><a href="faq.php">Privacy policy </a></li>
                                <li><a href="terms_conditions.php">How to Buy</a></li>
                                <li><a href="terms_conditions.php">Guide</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3 col-md-6">
                        <div class="footer_widget" >
                            <h4 class="large_address ">
                              <span>0042</span>300 223 334
                            </h4>
                            <div class="apps_boxs">
                                <a href="#" class="google_play_box d-flex align-items-center mb_10">
                                    <div class="icon">
                                        <img src="img/amaz_icon/google_play.svg" alt="">
                                    </div>
                                    <div class="google_play_text">
                                        <span>Get it on</span>
                                        <h4 class="text-nowrap">Google Play</h4>
                                    </div>
                                </a>
                                <a href="#" class="google_play_box d-flex align-items-center">
                                    <div class="icon">
                                        <img src="img/amaz_icon/apple_icon.svg" alt="">
                                    </div>
                                    <div class="google_play_text">
                                        <span>Download on</span>
                                        <h4 class="text-nowrap">Apple Store</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3  col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Subscribe Newsletter</h3>
                            </div>
                            <div class="subcribe-form mb_20 theme_mailChimp2"  id="mc_embed_signup">
                                <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                    method="get" class="subscription relative">
                                    <input name="EMAIL" class="form-control" placeholder="Type e-mail address…" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail address…'"
                                        required="" type="email">
                                    <div style="position: absolute; left: -5000px;">
                                        <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                    </div>
                                    <button class="">Subscribe</button>
                                    <div class="info"></div>
                                </form>
                            </div>
                            <div class="social__Links">
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright_area p-0">
            <div class="container">
                <div class="footer_border m-0"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="copy_right_text d-flex align-items-center gap_20 flex-wrap">
                            <p class="flex-fill">&copy; <span id="date_dynamic"></span> <a href="#"> Amazing </a>. All rights reserved. Made By <a href="https://codecanyon.net/user/codethemes/portfolio">CodeThemes</a>.</p>
                            <div class="footer_list_links">
                                <a href="terms_conditions.php">Terms & Conditions</a>
                                <a href="refund_details.php">Return Policy</a>
                                <a href="faq.php">Privacy Policy</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer_border m-0"></div>
                <div class="row">
                    <div class="col-12">
                        <div class="payment_imgs text-center ">
                            <img class="img-fluid" src="img/ssl_img.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER::END  -->
    <!-- side_chartView_total::start  -->
    <div class="side_chartView_total d-flex align-items-center add_to_cart  gj-cursor-pointer ">
        <div class="icon_lock">
            <img src="img/svg/lock_icon.svg" alt="">
        </div>
        <div class="cart_view_text">
            <span>03 Items</span>
            <h5 class="lh-1">£3249.00</h5>
        </div>
    </div>
    <!-- side_chartView_total::end  -->
