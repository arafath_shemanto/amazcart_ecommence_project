<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>


<!-- cart_wrapper::start  -->
<div class="cart_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="cart_tile mb_30">
                    <h3 class="fs-4 f_w_700 m-0">Shoping Cart</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-9">
                <div class="cart_table_wrapper mb_30">
                    <div class="table-responsive mb_25">
                        <table class="table custom_table mb-0">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="ps-0">
                                        <div class="product_name d-flex align-items-center">
                                            <div class="close_icon">
                                                <svg  xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                    <path  data-name="Path 174" d="M0,0H16V16H0Z" fill="none"/>
                                                    <path  data-name="Path 175" d="M14.95,6l-1-1L9.975,8.973,6,5,5,6,8.973,9.975,5,13.948l1,1,3.973-3.973,3.973,3.973,1-1L10.977,9.975Z" transform="translate(-1.975 -1.975)" fill="#fd4949"/>
                                                </svg>
                                            </div>
                                            <div class="thumb">
                                                <img class="img-fluid" src="img/cart/cart_products_1.png" alt="">
                                            </div>
                                            <span>The Unbundled University</span>
                                        </div>
                                    </td>
                                    <td class="f_w_400 mute_text" >$169.00</td>
                                    <td>
                                        <div class="product_number_count style_3" data-target="amount-1">
                                            <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                            <input id="amount-1" class="count_single_item input-number" type="text" value="1" >
                                            <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                                        </div>
                                    </td>
                                    <td  class="f_w_700">$4,000.00 
                                        <span class="close_icon style_2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.249" height="15.076" viewBox="0 0 12.249 15.076">
                                                <g  transform="translate(-48)">
                                                    <path  data-name="Path 1449" d="M59.071,1.884H56.48V1.413A1.415,1.415,0,0,0,55.067,0H53.182a1.415,1.415,0,0,0-1.413,1.413v.471H49.178A1.179,1.179,0,0,0,48,3.062V4.711a.471.471,0,0,0,.471.471h.257l.407,8.547a1.412,1.412,0,0,0,1.412,1.346H57.7a1.412,1.412,0,0,0,1.412-1.346l.407-8.547h.257a.471.471,0,0,0,.471-.471V3.062A1.179,1.179,0,0,0,59.071,1.884Zm-6.36-.471a.472.472,0,0,1,.471-.471h1.884a.472.472,0,0,1,.471.471v.471H52.711ZM48.942,3.062a.236.236,0,0,1,.236-.236h9.893a.236.236,0,0,1,.236.236V4.24H48.942Zm9.23,10.623a.471.471,0,0,1-.471.449H50.547a.471.471,0,0,1-.471-.449l-.4-8.5h8.905Z" fill="#00124e"/>
                                                    <path  data-name="Path 1450" d="M240.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,240.471,215.067Z" transform="translate(-186.347 -201.875)" fill="#00124e"/>
                                                    <path  data-name="Path 1451" d="M320.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,320.471,215.067Z" transform="translate(-263.991 -201.875)" fill="#00124e"/>
                                                    <path  data-name="Path 1452" d="M160.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,0,0-.942,0V214.6A.471.471,0,0,0,160.471,215.067Z" transform="translate(-108.702 -201.875)" fill="#00124e"/>
                                                </g>
                                            </svg>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ps-0">
                                        <div class="product_name d-flex align-items-center">
                                            <div class="close_icon">
                                                <svg id="icon4" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                    <path  data-name="Path 174" d="M0,0H16V16H0Z" fill="none"/>
                                                    <path  data-name="Path 175" d="M14.95,6l-1-1L9.975,8.973,6,5,5,6,8.973,9.975,5,13.948l1,1,3.973-3.973,3.973,3.973,1-1L10.977,9.975Z" transform="translate(-1.975 -1.975)" fill="#fd4949"/>
                                                </svg>
                                            </div>
                                            <div class="thumb">
                                                <img class="img-fluid" src="img/cart/cart_products_1.png" alt="">
                                            </div>
                                            <span>The Unbundled University</span>
                                        </div>
                                    </td>
                                    <td class="f_w_400 mute_text" >$169.00</td>
                                    <td>
                                        <div class="product_number_count style_3" data-target="amount-2">
                                            <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                            <input id="amount-2" class="count_single_item input-number" type="text" value="1" >
                                            <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                                        </div>
                                    </td>
                                    <td  class="f_w_700">$4,000.00 
                                    <span class="close_icon style_2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.249" height="15.076" viewBox="0 0 12.249 15.076">
                                        <g  transform="translate(-48)">
                                            <path  data-name="Path 1449" d="M59.071,1.884H56.48V1.413A1.415,1.415,0,0,0,55.067,0H53.182a1.415,1.415,0,0,0-1.413,1.413v.471H49.178A1.179,1.179,0,0,0,48,3.062V4.711a.471.471,0,0,0,.471.471h.257l.407,8.547a1.412,1.412,0,0,0,1.412,1.346H57.7a1.412,1.412,0,0,0,1.412-1.346l.407-8.547h.257a.471.471,0,0,0,.471-.471V3.062A1.179,1.179,0,0,0,59.071,1.884Zm-6.36-.471a.472.472,0,0,1,.471-.471h1.884a.472.472,0,0,1,.471.471v.471H52.711ZM48.942,3.062a.236.236,0,0,1,.236-.236h9.893a.236.236,0,0,1,.236.236V4.24H48.942Zm9.23,10.623a.471.471,0,0,1-.471.449H50.547a.471.471,0,0,1-.471-.449l-.4-8.5h8.905Z" fill="#00124e"/>
                                            <path  data-name="Path 1450" d="M240.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,240.471,215.067Z" transform="translate(-186.347 -201.875)" fill="#00124e"/>
                                            <path  data-name="Path 1451" d="M320.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,320.471,215.067Z" transform="translate(-263.991 -201.875)" fill="#00124e"/>
                                            <path  data-name="Path 1452" d="M160.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,0,0-.942,0V214.6A.471.471,0,0,0,160.471,215.067Z" transform="translate(-108.702 -201.875)" fill="#00124e"/>
                                        </g>
                                        </svg>
                                    </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ps-0">
                                        <div class="product_name d-flex align-items-center">
                                            <div class="close_icon">
                                                <svg  xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                    <path data-name="Path 174" d="M0,0H16V16H0Z" fill="none"/>
                                                    <path  data-name="Path 175" d="M14.95,6l-1-1L9.975,8.973,6,5,5,6,8.973,9.975,5,13.948l1,1,3.973-3.973,3.973,3.973,1-1L10.977,9.975Z" transform="translate(-1.975 -1.975)" fill="#FB6600"/>
                                                </svg>
                                            </div>
                                            <div class="thumb">
                                                <img class="img-fluid" src="img/cart/cart_products_1.png" alt="">
                                            </div>
                                            <span>The Unbundled University</span>
                                        </div>
                                    </td>
                                    <td class="f_w_400 mute_text" >$169.00</td>
                                    <td>
                                        <div class="product_number_count style_3" data-target="amount-3">
                                            <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                            <input id="amount-3" class="count_single_item input-number" type="text" value="1" >
                                            <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                                        </div>
                                    </td>
                                    <td  class="f_w_700">$4,000.00 
                                        <span class="close_icon style_2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.249" height="15.076" viewBox="0 0 12.249 15.076">
                                            <g  transform="translate(-48)">
                                                <path  data-name="Path 1449" d="M59.071,1.884H56.48V1.413A1.415,1.415,0,0,0,55.067,0H53.182a1.415,1.415,0,0,0-1.413,1.413v.471H49.178A1.179,1.179,0,0,0,48,3.062V4.711a.471.471,0,0,0,.471.471h.257l.407,8.547a1.412,1.412,0,0,0,1.412,1.346H57.7a1.412,1.412,0,0,0,1.412-1.346l.407-8.547h.257a.471.471,0,0,0,.471-.471V3.062A1.179,1.179,0,0,0,59.071,1.884Zm-6.36-.471a.472.472,0,0,1,.471-.471h1.884a.472.472,0,0,1,.471.471v.471H52.711ZM48.942,3.062a.236.236,0,0,1,.236-.236h9.893a.236.236,0,0,1,.236.236V4.24H48.942Zm9.23,10.623a.471.471,0,0,1-.471.449H50.547a.471.471,0,0,1-.471-.449l-.4-8.5h8.905Z" fill="#00124e"/>
                                                <path  data-name="Path 1450" d="M240.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,240.471,215.067Z" transform="translate(-186.347 -201.875)" fill="#00124e"/>
                                                <path  data-name="Path 1451" d="M320.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,1,0-.942,0V214.6A.471.471,0,0,0,320.471,215.067Z" transform="translate(-263.991 -201.875)" fill="#00124e"/>
                                                <path  data-name="Path 1452" d="M160.471,215.067a.471.471,0,0,0,.471-.471v-6.125a.471.471,0,0,0-.942,0V214.6A.471.471,0,0,0,160.471,215.067Z" transform="translate(-108.702 -201.875)" fill="#00124e"/>
                                            </g>
                                        </svg>

                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" class="amaz_primary_btn style2 min_180 text-uppercase text-center">update cart</button>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="cart_sub_box mb_30">
                    <div class="cart_sub_box_amount">
                        <p> <span>Subtotal</span> + USD 1324.35</p>
                        <p> <span>Delivery Fee</span>+ USD 75.35</p>
                        <p> <span>Discount</span> + USD 206.35</p>
                    </div>
                    <div class="cart_sub_box_total mb_25">
                        <h4 class="m-0">Total (Incl. VAT)</h4>
                        <h4 class="m-0">+ USD 1324.35</h4>
                    </div>
                    <div class="coupon_verify_information style2 mb_20 d-flex">
                        <div class="icon">
                            <img src="img/cart/warning.svg" alt="">
                        </div>
                        <div class="coupon_content">
                            <h4 class="font_14 f_w_700 lh-1">Invalid Coupon Code</h4>
                            <p class="m-0">Please enter a valid coupon 
                                codeto avail this offer</p>
                        </div>
                    </div>
                    <div class="amazy_bb"></div>
                    <label class="primary_checkbox d-flex ">
                        <input type="checkbox">
                        <span class="checkmark mr_10"></span>
                        <span class="label_name">I agree with the terms and conditions.</span>
                    </label>
                    <a href="#" class="amaz_primary_btn style2  w-100 text-uppercase text-center">+ Proceed to checkout</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cart_wrapper::end  -->
<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>