<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- blog_details_area::start  -->
<div class="blog_details_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-7">
                <div class="blog_details_inner m-0">
                    <div class="blog_details_banner">
                        <img class="img-fluid" src="img/blog_details/blog_banner.jpg" alt="">
                    </div>
                    <div class="blog_post_date d-flex align-items-center"> <span>Programing</span> <p>September 14, 2020</p> </div>
                    <h3>2020 Complete Python Bootcamp: 
                        From Zero to Hero in Python</h3>
                    <p class="mb_25">Duis aute irure dolor reprehenderit  voluptate velit esse cillum dolore eu fugiatnulla xcepteur sint aecatpidatat nones proident, sunt in culpa qui officiat mollit anim idestborum. Sedutes perspiciatis unde omnis iste natus error sitluptatem  enim ad minim veniamquis nostrud exercitation perspiciatis unde omnis iste natus error sit voluptatem exercitation perspiciatis unde .</p>
                    <p class="mb-0">Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute  reprehenderit inluptatee cillum ugiatnulla xcepteur sint aecat cupidatat nones proident, sunt in culpa qui officiat.</p>
                    <div class="quote_text">
                        <div class="horizontal_line"></div>
                        <h4>Risus commodo viverra maecenas accumsan lacus velesinm facilisis ipsum dolor sit amet, consectetur adipiscing elitsed eiusmod tempor incididunte viverra maecenas accumsan lacus velesinm.</h4>
                    </div>
                    <p>Duis aute irure dolor reprehenderit  voluptate velit esse cillum dolore eu fugiatnulla xcepteur sint aecat cupidatat nones proident, sunt in culpa qui officiat mollit anim idestborum. Sedutes perspiciatis unde omnis iste natus error sitluptatem  enim ad minim veniamquis nostrud.</p>
                    <div class="details_info">
                        <h4>The Cycleing Extraterrestrial </h4>
                        <p class="mb_25">Duis aute irure dolor reprehenderit  voluptate velit esse cillum dolore eu fugiatnulla xcepteur sint aecat cupidatat nones proident, sunt in culpa qui officiat mollit anim idestborum. Sedutes perspiciatis unde omnis iste natus error sitluptatem  enim ad minim veniamquis nostrud exercitation perspiciatis unde omnis iste natus error sit voluptatem.</p>
                        <p>Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute  reprehenderit inluptatee cillum dolore eugiatnulla xcepteur sint aecat cupidatat nones proident, sunt in culpa qui officiat mollit anim idestborumvelit esse cillume cillum dolore eu fugiatnulla xcepteur sint aecat cupidatat nones proident.</p>
                    </div>
                </div>
                <div class="blog_details_tags d-flex align-items-center gap_10">
                    <h4 class="font_16 f_w_700 m-0">Tags:</h4>
                    <p class="font_14 f_w_500 m-0">Baby Fashion, Men Clothing</p>
                </div>
                <div class="blog_reviews">
                    <h3 class="font_30 f_w_700 mb_35 lh-1">03 Comments</h3>
                    <div class="blog_reviews_inner">
                        <div class="single_reviews">
                            <div class="thumb">
                                ks
                            </div>
                            <div class="review_content">
                                <div class="review_content_head d-flex justify-content-between align-items-start flex-wrap">
                                    <div class="review_content_head_left">
                                        <h4 class="f_w_700 font_20">Kristen Stewart</h4>
                                        <div class="rated_customer d-flex align-items-center">
                                            <div class="feedmak_stars">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <span>2 weeks ago</span>
                                        </div>
                                    </div>
                                </div>
                                <p>See-through delicate embroidered organza blue lining luxury acetate-mix stretch pleat detailing Leather detail shoulder contrastic colour contour stunni silhouette working peplum. Statement buttons cover-up tweaks patch pockets perennia lapel collar.</p>
                            </div>
                        </div>
                        <div class="single_reviews">
                            <div class="thumb">
                                ks
                            </div>
                            <div class="review_content">
                                <div class="review_content_head d-flex justify-content-between align-items-start flex-wrap">
                                    <div class="review_content_head_left">
                                        <h4 class="f_w_700 font_20">Kristen Stewart</h4>
                                        <div class="rated_customer d-flex align-items-center">
                                            <div class="feedmak_stars">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <span>2 weeks ago</span>
                                        </div>
                                    </div>
                                </div>
                                <p>See-through delicate embroidered organza blue lining luxury acetate-mix stretch pleat detailing Leather detail shoulder contrastic colour contour stunni silhouette working peplum. Statement buttons cover-up tweaks patch pockets perennia lapel collar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="blog_reply_box mb_30">
                    <h3 class="font_30 f_w_700 mb_40 lh-1">Leave a Reply</h3>
                    <form action="#">
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="primary_label2">Full Name <span>*</span> </label>
                                <input name="name" placeholder="Enter Full Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Full Name'" class="primary_input3 bg_style1 radius_5px  mb_20" required="" type="text">
                            </div>
                            <div class="col-12">
                                <label class="primary_label2">Email Address <span>*</span> </label>
                                <input name="name" placeholder="Enter Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email Address'" class="primary_input3 bg_style1 radius_5px mb_20" required="" type="text">
                            </div>
                            <div class="col-12">
                                <label class="primary_label2">Comments<span>*</span></label>
                                <textarea  name="name" placeholder="Write your comments here…" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Write your comments here…'" class="primary_textarea3 radius_5px mb_15" required=""></textarea>
                            </div>
                            <div class="col-12">
                                <button class="amaz_primary_btn min_220 style2 text-center   text-uppercase  text-center">Post comment</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <div class="blog_sidebar_wrap mb_30">
                    <div class="input-group  theme_search_field4 w-100 mb_20 style2">
                        <div class="input-group-prepend">
                            <button class="btn" type="button" > <i class="ti-search"></i> </button>
                        </div>
                        <input type="text" class="form-control" placeholder="Search…">
                    </div>
                    <div class="blog_sidebar_box mb_20">
                        <h4 class="font_18 f_w_700 mb_10">
                            Product categories
                        </h4>
                        <div class="home6_border w-100 mb_20"></div>
                        <ul class="Check_sidebar mb-0">
                            <li>
                                <label class="primary_checkbox d-flex">
                                    <input type="checkbox">
                                    <span class="checkmark mr_10"></span>
                                    <span class="label_name f_w_400">Beauty & Health</span>
                                </label>
                            </li>
                            <li>
                                <label class="primary_checkbox d-flex">
                                    <input type="checkbox">
                                    <span class="checkmark mr_10"></span>
                                    <span class="label_name f_w_400">Clothing</span>
                                </label>
                            </li>
                            <li>
                                <label class="primary_checkbox d-flex">
                                    <input type="checkbox">
                                    <span class="checkmark mr_10"></span>
                                    <span class="label_name f_w_400">Electronics & Computers</span>
                                </label>
                            </li>
                            <li>
                                <label class="primary_checkbox d-flex">
                                    <input type="checkbox">
                                    <span class="checkmark mr_10"></span>
                                    <span class="label_name f_w_400">Food & Grocery</span>
                                </label>
                            </li>
                            <li>
                                <label class="primary_checkbox d-flex">
                                    <input type="checkbox">
                                    <span class="checkmark mr_10"></span>
                                    <span class="label_name f_w_400">Furniture</span>
                                </label>
                            </li>
                            <li>
                                <label class="primary_checkbox d-flex">
                                    <input type="checkbox">
                                    <span class="checkmark mr_10"></span>
                                    <span class="label_name f_w_400">Garden & Kitchen</span>
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="blog_sidebar_box mb_15">
                        <h4 class="font_18 f_w_700 mb_10">
                        Recent News
                        </h4>
                        <div class="home6_border w-100 mb_20"></div>
                        <div class="news_lists">
                            <div class="single_newslist">
                                <a href="#">
                                    <h4>10 days quick challange for boost 
                                        your online visitors.</h4>
                                </a>
                                <p>April 08, 2020  /  Fashion</p>
                            </div>
                            <div class="single_newslist">
                                <a href="#">
                                    <h4>10 days quick challange for boost 
                                        your online visitors.</h4>
                                </a>
                                <p>April 08, 2020  /  Fashion</p>
                            </div>
                            <div class="single_newslist">
                                <a href="#">
                                    <h4>10 days quick challange for boost 
                                        your online visitors.</h4>
                                </a>
                                <p>April 08, 2020  /  Fashion</p>
                            </div>
                        </div>
                    </div>
                    <div class="blog_sidebar_box mb_30 p-0 border-0">
                        <h4 class="font_18 f_w_700 mb_10">
                        Keywords
                        </h4>
                        <div class="home6_border w-100 mb_20"></div>
                        <div class="keyword_lists d-flex align-items-center flex-wrap gap_10">
                            <a href="#">Project</a>
                            <a href="#">Technology</a>
                            <a href="#">Illustration</a>
                            <a href="#">Illustration</a>
                            <a href="#">Project</a>
                            <a href="#">Travel</a>
                            <a href="#">Project</a>
                            <a href="#">Technology</a>
                            <a href="#">Illustration</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- blog_details_area::end  -->


<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>