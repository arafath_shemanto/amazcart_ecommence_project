<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>
<div class="flash_deal_banner mb_30">
    <img src="img/banner/flashDeal_banner2.jpg" alt="" class="img-fluid w-100">
</div>
<div class="new_user_section section_spacing6 pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title d-flex align-items-center gap-3 m-0 flex-wrap">
                    <h3 class="m-0 flex-fill">Deal End’s In</h3>
                    <div id="count_small" class="deals_end_count amazy_date_counter"></div>
                </div>
                <div class="amazy_bb mb_30 mt_20"></div>
            </div>
        </div>
        <div class="row ">
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/1.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/2.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/3.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/4.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/1.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/2.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/3.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/4.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/1.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/2.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/3.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="product_widget5 mb_25">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/4.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="#">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1"> -20% </span>
                        <span class="badge_1 style2 text-uppercase"> New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- new_user_section::end  -->

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>