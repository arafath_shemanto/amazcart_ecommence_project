<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- brand_banner::start  -->
<div class="brand_banner">
    <img src="img/banner/category_banner.jpg" alt="category_banner" class="img-fluid w-100">
</div>
<!-- brand_banner::end  -->
<!-- prodcuts_area ::start  -->
<div class="prodcuts_area ">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xl-3">
                <div id="product_category_chose" class="product_category_chose mb_30 mt_15">
                    <div class="course_title mb_15 d-flex align-items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="19.5" height="13" viewBox="0 0 19.5 13">
                            <g id="filter-icon" transform="translate(28)">
                                <rect id="Rectangle_1" data-name="Rectangle 1" width="19.5" height="2" rx="1" transform="translate(-28)" fill="#fd4949"/>
                                <rect id="Rectangle_2" data-name="Rectangle 2" width="15.5" height="2" rx="1" transform="translate(-26 5.5)" fill="#fd4949"/>
                                <rect id="Rectangle_3" data-name="Rectangle 3" width="5" height="2" rx="1" transform="translate(-20.75 11)" fill="#fd4949"/>
                            </g>
                        </svg>
                        <h5 class="font_16 f_w_700 mb-0 ">Filter Category</h5>
                        <div class="catgory_sidebar_closeIcon flex-fill justify-content-end d-flex d-lg-none">
                            <button id="catgory_sidebar_closeIcon" class="home10_primary_btn2 gj-cursor-pointer mb-0 small_btn">close</button>
                        </div>
                    </div>
                    <div class="course_category_inner">
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_700 ">
                                Product categories 
                            </h4> 
                            <ul class="Check_sidebar mb_35">
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Beauty & Health</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Clothing</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Electronics & Computers</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Food & Grocery</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Furniture</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Garden & Kitchen</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_700">
                            Filter by Brands
                            </h4>
                            <ul class="Check_sidebar mb_35">
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">La Re Ve</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Easy Fashion</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Yellow</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Estasy</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Unileaver</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_700">
                            Filter by Size
                            </h4>
                            <ul class="Check_sidebar mb_35">
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">M</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">L</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Xl</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">XXl</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">XXXL</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_700">
                            Filter by Rating
                            </h4>
                            <ul class="rating_lists mb_35">
                                <li>
                                    <div class="ratings">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                </li>
                                <li>
                                    <div class="ratings">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <span>And Up</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="ratings">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <span>And Up</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="ratings">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <span>And Up</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="ratings">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <span>And Up</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_700">
                                Filter by Colors
                            </h4>
                            <div class="color_filter">
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox black_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox blue_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox gray_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox paste_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox brouwn_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox violate_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter red_check">
                                    <label class="round_checkbox d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox yellow_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_700">
                            Filter by Price
                            </h4>
                            <div class="filter_wrapper">
                                <div id="slider-range"></div>
                                <div class="d-flex align-items-center prise_line">
                                    <button class="home10_primary_btn2 mr_20 mb-0 small_btn">Filter</button>
                                    <span>Price: </span> <input type="text" id="amount" readonly >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-xl-9">
                <div class="row ">
                    <div class="col-12">
                        <div class="box_header d-flex flex-wrap align-items-center justify-content-between">
                            <h5 class="font_16 f_w_500 mr_10 mb-0">Showing 1–12 of 40 Results</h5>
                            <div class="box_header_right ">
                                <div class="short_select d-flex align-items-center gap_10 flex-wrap">
                                    <div class="prduct_showing_style">
                                        <ul class="nav align-items-center" id="myTab" role="tablist">
                                            <li class="nav-item lh-1">
                                                <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                                                    <img src="img/svg/grid_view.svg" alt="">
                                                </a>
                                            </li>
                                            <li class="nav-item lh-1">
                                                <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                                                    <img src="img/svg/list_view.svg" alt="">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="shorting_box">
                                        <select class="amaz_select" >
                                            <option data-display="Show 20 item’s">Show 20 item’s</option>
                                            <option value="1">Show 20 item’s</option>
                                            <option value="2">Show 30 item’s</option>
                                            <option value="3">Show 40 item’s</option>
                                            <option value="4">Show 45 item’s</option>
                                            <option value="5">Show 50 item’s </option>
                                        </select>
                                    </div>
                                    <div class="shorting_box">
                                        <select class="amaz_select" >
                                            <option data-display="Default Shorting">Default Shorting</option>
                                            <option value="1">by Popularity</option>
                                            <option value="2">by Rating</option>
                                            <option value="3">by Latest</option>
                                            <option value="4">Low to High</option>
                                            <option value="5">High  to Low </option>
                                        </select>
                                    </div>
                                    <div class="flex-fill text-end">
                                        <div class="category_toggler d-inline-block d-lg-none  gj-cursor-pointer">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="19.5" height="13" viewBox="0 0 19.5 13">
                                                <g id="filter-icon" transform="translate(28)">
                                                    <rect id="Rectangle_1" data-name="Rectangle 1" width="19.5" height="2" rx="1" transform="translate(-28)" fill="#fd4949"/>
                                                    <rect id="Rectangle_2" data-name="Rectangle 2" width="15.5" height="2" rx="1" transform="translate(-26 5.5)" fill="#fd4949"/>
                                                    <rect id="Rectangle_3" data-name="Rectangle 3" width="5" height="2" rx="1" transform="translate(-20.75 11)" fill="#fd4949"/>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content mb_30" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <!-- content  -->
                        <div class="row custom_rowProduct">
                            <div class="col-xl-4 col-lg-4 col-md-6 col-6">
                                <div class="product_widget5 mb_30 style5">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-6">
                                <div class="product_widget5 mb_30 style5">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-6">
                                <div class="product_widget5 mb_30 style5">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-6">
                                <div class="product_widget5 mb_30 style5">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-6">
                                <div class="product_widget5 mb_30 style5">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/5.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-6">
                                <div class="product_widget5 mb_30 style5">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/6.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-6">
                                <div class="product_widget5 mb_30 style5">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/7.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-6">
                                <div class="product_widget5 mb_30 style5">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/8.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-6">
                                <div class="product_widget5 mb_30 style5">
                                    <div class="product_thumb_upper">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/9.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta text-center">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-center">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="product_prise">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ content  -->
                    </div>
                    <div class="tab-pane fade " id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <!-- content  -->
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="product_widget5 mb_30 list_style_product">
                                    <div class="product_thumb_upper m-0">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/1.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-start">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <span class="font_14 f_w_500 mute_text">(698 ratings)</span>
                                        </div>
                                        <p class="description_text">Powered by 4500mAh battery which lets you be more energetic in business
                                            games, videos and reading. 5MP selfie camera allows.</p>
                                        <div class="product_prise justify-content-start">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="product_widget5 mb_30 list_style_product">
                                    <div class="product_thumb_upper m-0">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/2.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-start">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <span class="font_14 f_w_500 mute_text">(698 ratings)</span>
                                        </div>
                                        <p class="description_text">Powered by 4500mAh battery which lets you be more energetic in business
                                            games, videos and reading. 5MP selfie camera allows.</p>
                                        <div class="product_prise justify-content-start">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="product_widget5 mb_30 list_style_product">
                                    <div class="product_thumb_upper m-0">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/3.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-start">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <span class="font_14 f_w_500 mute_text">(698 ratings)</span>
                                        </div>
                                        <p class="description_text">Powered by 4500mAh battery which lets you be more energetic in business
                                            games, videos and reading. 5MP selfie camera allows.</p>
                                        <div class="product_prise justify-content-start">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="product_widget5 mb_30 list_style_product">
                                    <div class="product_thumb_upper m-0">
                                        <a href="product_details.php" class="thumb">
                                            <img src="img/amazPorduct/4.png" alt="">
                                        </a>
                                        <div class="product_action">
                                            <a href="compare.php">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-star"></i>
                                            </a>
                                        </div>
                                        <span class="badge_1"> -20% </span>
                                        <span class="badge_1 style2 text-uppercase"> New </span>
                                    </div>
                                    <div class="product__meta">
                                        <span class="product_banding ">Studio Design</span>
                                        <a href="product_details.php">
                                            <h4>Designs Woolrich Kletter</h4>
                                        </a>
                                        <div class="stars justify-content-start">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <span class="font_14 f_w_500 mute_text">(698 ratings)</span>
                                        </div>
                                        <p class="description_text">Powered by 4500mAh battery which lets you be more energetic in business
                                            games, videos and reading. 5MP selfie camera allows.</p>
                                        <div class="product_prise justify-content-start">
                                            <p><span>$29.00 </span>  $20.00</p>
                                            <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ content  -->
                    </div>
                </div>
                <div class="amaz_pagination d-flex align-items-center pt_15 mb_20">
                    <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                        <i class="fas fa-chevron-left"></i>
                        <span>Prev</span>
                    </a>
                    <a class="page_counter active" href="#">1</a>
                    <a class="page_counter" href="#">2</a>
                    <a class="page_counter" href="#">3</a>
                    <a href="#">...</a>
                    <a class="page_counter" href="#">8</a>
                    <a class="arrow_btns d-inline-flex align-items-center justify-content-center" href="#">
                        <span>Next</span>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>