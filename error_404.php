<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- error_area::start  -->
<div class="error_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="error_inner">
                    <div class="thumb">
                        <img class="img-fluid" src="img/error_img.png" alt="">
                    </div>
                    <h3>Opps! Page Not Found</h3>
                    <p>Perhaps you can try to refresh the page, sometimes it works.</p>
                    <a href="index.php" class="amaz_primary_btn min_200 style6 f_w_700 radius_3px">Back To Homepage</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- error_area:: end  -->


<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>