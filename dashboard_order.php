<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<div class="amazy_dashboard_area dashboard_bg section_spacing6">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <?php include 'include/dashboard_sidebar.php' ?>
            </div>
            <div class="col-xl-8 col-lg-8">
                <div class="order_tab_box d-flex justify-content-between gap-2 flex-wrap mb_20">
                    <ul class="nav amazy_order_tabs d-inline-flex" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">All</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="Pay-tab" data-bs-toggle="tab" data-bs-target="#Pay" type="button" role="tab" aria-controls="Pay" aria-selected="false">To Pay</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="Ship-tab" data-bs-toggle="tab" data-bs-target="#Ship" type="button" role="tab" aria-controls="Ship" aria-selected="false">To Ship</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="Receive-tab" data-bs-toggle="tab" data-bs-target="#Receive" type="button" role="tab" aria-controls="Receive" aria-selected="false">To Receive</button>
                        </li>
                    </ul>
                    <div class="d-flex align-items-center">
                        <select class="amaz_select5">
                            <option value="">Last 04 Order</option>
                            <option value="">Last 10 Order</option>
                            <option value="">Last 40 Order</option>
                            <option value="">Last 50 Order</option>
                        </select>
                    </div>
                </div>
                <!-- tab-content  -->
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <!-- content ::start  -->
                        <div class="white_box style2 bg-white mb_20">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap  amazy_bb3 justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <button class="amaz_primary_btn gray_bg_btn min_200 radius_3px">+ Download invoice</button>
                                </div>
                            </div>
                            <div class="dashboard_white_box_body">
                                <div class="table-responsive mb_10">
                                    <table class="table amazy_table3 style2 mb-0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="dashboard_Order_details.php" class="amaz_primary_btn style2 text-nowrap ">Order details</a>
                                </div>
                            </div>
                        </div>
                        <div class="white_box style2 bg-white mb_20">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap   justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <div class="d-flex  gap_10  ">
                                <a href="#" class="amaz_badge_btn5 text-nowrap  text-center text-uppercase" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample1" >View</a>
                                <a href="#" class="amaz_badge_btn5 text-nowrap  text-center text-uppercase">Invoice</a>
                                </div>
                            </div>
                        </div>
                        <div class="white_box style2 bg-white mb_20 collapse" id="collapseExample1">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap  amazy_bb3 justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <!-- <div class="d-flex flex-column  ">
                                    <button class="amaz_primary_btn gray_bg_btn min_200 radius_3px">+ Download invoice</button>
                                </div> -->
                                <div class="d-flex  gap_10  ">
                                    <a href="#" class="amaz_badge_btn5 text-nowrap  text-center text-uppercase" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample1">close</a>
                                    <a href="#" class="amaz_badge_btn5 text-nowrap  text-center text-uppercase">Invoice</a>
                                </div>
                            </div>
                            <div class="dashboard_white_box_body">
                                <div class="table-responsive mb_10">
                                    <table class="table amazy_table3 style2 mb-0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="dashboard_Order_details.php" class="amaz_primary_btn style2 text-nowrap ">Order details</a>
                                </div>
                            </div>
                        </div>
                        <!-- content ::end    -->
                    </div>
                    <div class="tab-pane fade" id="Pay" role="tabpanel" aria-labelledby="Pay-tab">
                        <!-- content ::start  -->
                        <div class="white_box style2 bg-white mb_20">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap  amazy_bb3 justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <button class="amaz_primary_btn gray_bg_btn min_200 radius_3px">+ Download invoice</button>
                                </div>
                            </div>
                            <div class="dashboard_white_box_body">
                                <div class="table-responsive mb_10">
                                    <table class="table amazy_table3 style2 mb-0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="dashboard_Order_details.php" class="amaz_primary_btn style2 text-nowrap ">Order details</a>
                                </div>
                            </div>
                        </div>
                        <div class="white_box style2 bg-white mb_20">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap   justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <div class="d-flex  gap_10  ">
                                <a href="#" class="amaz_badge_btn5 text-nowrap  text-center text-uppercase">View</a>
                                <a href="#" class="amaz_badge_btn5 text-nowrap  text-center text-uppercase">Invoice</a>
                                </div>
                            </div>
                        </div>
                        <div class="white_box style2 bg-white mb_20">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap  amazy_bb3 justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <button class="amaz_primary_btn gray_bg_btn min_200 radius_3px">+ Download invoice</button>
                                </div>
                            </div>
                            <div class="dashboard_white_box_body">
                                <div class="table-responsive mb_10">
                                    <table class="table amazy_table3 style2 mb-0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="dashboard_Order_details.php" class="amaz_primary_btn style2 text-nowrap ">Order details</a>
                                </div>
                            </div>
                        </div>
                        <!-- content ::end    -->
                    </div>
                    <div class="tab-pane fade" id="Ship" role="tabpanel" aria-labelledby="Ship-tab">
                        <!-- content ::start  -->
                        <div class="white_box style2 bg-white mb_20">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap  amazy_bb3 justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <button class="amaz_primary_btn gray_bg_btn min_200 radius_3px">+ Download invoice</button>
                                </div>
                            </div>
                            <div class="dashboard_white_box_body">
                                <div class="table-responsive mb_10">
                                    <table class="table amazy_table3 style2 mb-0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="dashboard_Order_details.php" class="amaz_primary_btn style2 text-nowrap ">Order details</a>
                                </div>
                            </div>
                        </div>
                        <div class="white_box style2 bg-white mb_20">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap   justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <div class="d-flex  gap_10  ">
                                <a href="#" class="amaz_badge_btn5 text-nowrap  text-center text-uppercase">View</a>
                                <a href="#" class="amaz_badge_btn5 text-nowrap  text-center text-uppercase">Invoice</a>
                                </div>
                            </div>
                        </div>
                        <div class="white_box style2 bg-white mb_20">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap  amazy_bb3 justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <button class="amaz_primary_btn gray_bg_btn min_200 radius_3px">+ Download invoice</button>
                                </div>
                            </div>
                            <div class="dashboard_white_box_body">
                                <div class="table-responsive mb_10">
                                    <table class="table amazy_table3 style2 mb-0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="dashboard_Order_details.php" class="amaz_primary_btn style2 text-nowrap ">Order details</a>
                                </div>
                            </div>
                        </div>
                        <!-- content ::end    -->
                    </div>
                    <div class="tab-pane fade" id="Receive" role="tabpanel" aria-labelledby="Receive-tab">
                        <!-- content ::start  -->
                        <div class="white_box style2 bg-white mb_20">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap  amazy_bb3 justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <button class="amaz_primary_btn gray_bg_btn min_200 radius_3px">+ Download invoice</button>
                                </div>
                            </div>
                            <div class="dashboard_white_box_body">
                                <div class="table-responsive mb_10">
                                    <table class="table amazy_table3 style2 mb-0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="dashboard_Order_details.php" class="amaz_primary_btn style2 text-nowrap ">Order details</a>
                                </div>
                            </div>
                        </div>
                        <div class="white_box style2 bg-white mb_20">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap   justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <div class="d-flex  gap_10  ">
                                <a href="#" class="amaz_badge_btn5 text-nowrap  text-center text-uppercase">View</a>
                                <a href="#" class="amaz_badge_btn5 text-nowrap  text-center text-uppercase">Invoice</a>
                                </div>
                            </div>
                        </div>
                        <div class="white_box style2 bg-white mb_20">
                            <div class="white_box_header d-flex align-items-center gap_20 flex-wrap  amazy_bb3 justify-content-between ">
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order ID: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 3211228025521</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Date : </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-12-28 02:55:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Status: </h4> <p class="font_14 f_w_400 m-0 lh-base"> Confirmed</p>
                                    </div>
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Request Send Data: </h4> <p class="font_14 f_w_400 m-0 lh-base"> 2021-06-10 15:17:21</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <div class="d-flex align-items-center flex-wrap gap_5">
                                        <h4 class="font_14 f_w_500 m-0 lh-base">Order Amount: </h4> <p class="font_14 f_w_400 m-0 lh-base"> $8420.00</p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column  ">
                                    <button class="amaz_primary_btn gray_bg_btn min_200 radius_3px">+ Download invoice</button>
                                </div>
                            </div>
                            <div class="dashboard_white_box_body">
                                <div class="table-responsive mb_10">
                                    <table class="table amazy_table3 style2 mb-0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="product_details.php" class="d-flex align-items-center gap_20">
                                                        <div class="thumb">
                                                            <img src="img/amazPorduct/summery_product_1.png" alt="">
                                                        </div>
                                                        <div class="summery_pro_content">
                                                            <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                            <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center gap_7">
                                                        <span class="green_badge">-30%</span>
                                                        <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">Qty: 1</h4>
                                                </td>
                                                <td>
                                                    <h4 class="font_16 f_w_500 m-0 ">$4,00.00</h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="dashboard_Order_details.php" class="amaz_primary_btn style2 text-nowrap ">Order details</a>
                                </div>
                            </div>
                        </div>
                        <!-- content ::end    -->
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>