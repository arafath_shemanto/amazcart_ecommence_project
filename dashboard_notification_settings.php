<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<div class="amazy_dashboard_area dashboard_bg section_spacing6">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <?php include 'include/dashboard_sidebar.php' ?>
            </div>
            <div class="col-xl-7 col-lg-7">
                <div class="dashboard_white_box style2 bg-white mb_25">
                    <div class="dashboard_white_box_body">
                        <div class="table-responsive mb_40">
                            <table class="table amazy_table style7 mb-0">
                                <thead>
                                    <tr>
                                        <th class="font_16 f_w_700" scope="col">Notify me when…</th>
                                        <th class="font_16 f_w_700 border-start-0 border-end-0" scope="col">Email</th>
                                        <th class="font_16 f_w_700" scope="col">Sms</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <p class="font_16 f_w_500 mb-0 lh-1">There is a new conversation</p>
                                            </div>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Anable12">
                                                <input type="checkbox" id="Anable12">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Sms12">
                                                <input type="checkbox" id="Sms12">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <p class="font_16 f_w_500 mb-0 lh-1">There is a new conversation</p>
                                            </div>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Anable13">
                                                <input type="checkbox" id="Anable13">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Sms13">
                                                <input type="checkbox" id="Sms13">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <p class="font_16 f_w_500 mb-0 lh-1">There is a new conversation</p>
                                            </div>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Anable145">
                                                <input type="checkbox" id="Anable145">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Sms145">
                                                <input type="checkbox" id="Sms145">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive mb_40">
                            <table class="table amazy_table style7 mb-0">
                                <thead>
                                    <tr>
                                        <th class="font_16 f_w_700" scope="col">Notify me when a customer replies…</th>
                                        <th class="font_16 f_w_700 border-start-0 border-end-0" scope="col">Email</th>
                                        <th class="font_16 f_w_700" scope="col">Sms</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <p class="font_16 f_w_500 mb-0 lh-1">There is a new conversation</p>
                                            </div>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Anable2">
                                                <input type="checkbox" id="Anable2">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Sms2">
                                                <input type="checkbox" id="Sms2">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <p class="font_16 f_w_500 mb-0 lh-1">There is a new conversation</p>
                                            </div>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Anable3">
                                                <input type="checkbox" id="Anable3">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Sms3">
                                                <input type="checkbox" id="Sms3">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <p class="font_16 f_w_500 mb-0 lh-1">There is a new conversation</p>
                                            </div>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Anable4">
                                                <input type="checkbox" id="Anable4">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Sms4">
                                                <input type="checkbox" id="Sms4">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive">
                            <table class="table amazy_table style7 mb-0">
                                <thead>
                                    <tr>
                                        <th class="font_16 f_w_700" scope="col">Notify me when a agent replies or add note…</th>
                                        <th class="font_16 f_w_700 border-start-0 border-end-0" scope="col">Email</th>
                                        <th class="font_16 f_w_700" scope="col">Sms</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <p class="font_16 f_w_500 mb-0 lh-1">There is a new conversation</p>
                                            </div>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Anable5">
                                                <input type="checkbox" id="Anable5">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Sms5">
                                                <input type="checkbox" id="Sms5">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <p class="font_16 f_w_500 mb-0 lh-1">There is a new conversation</p>
                                            </div>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Anable6">
                                                <input type="checkbox" id="Anable6">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Sms6">
                                                <input type="checkbox" id="Sms6">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <p class="font_16 f_w_500 mb-0 lh-1">There is a new conversation</p>
                                            </div>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Anable7">
                                                <input type="checkbox" id="Anable7">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="amazySwitch_toggle" for="Sms7">
                                                <input type="checkbox" id="Sms7">
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>