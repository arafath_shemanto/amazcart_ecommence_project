<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- checkout_v3_area::start  -->
<div class="checkout_v3_area">
    <div class="checkout_v3_left d-flex justify-content-end">
        <div class="checkout_v3_inner">
            <div class="checout_head">
                <i class="ti-folder"></i>
                <p>Returning customer? <a data-bs-toggle="modal" data-bs-target="#checkot_login_form" href="#">Click here to login</a></p>
                
            </div>
            <div class="checout_head_title d-flex align-items-center ">
                <span class="flex-fill">5 ITEM(S)</span>
                <span>QUANTITY</span>
                <span>PRICE</span>
            </div>
            <div class="checkout_shiped_box">
                <div class="checout_shiped_head flex-wrap d-flex align-items-center ">
                    <span class="package_text flex-fill">Package 2 of 4</span>
                    <p><span class="Shipped_text">Shipped by</span> <span class="name_text">M M RAHNAN</span></p>
                </div>
                <div class="delevery_box">
                    <span class="check_icon">
                        <img src="img/check_icon.svg" alt="">
                    </span>
                    <div class="delevery_box_text">
                        <h5>$ 400.00</h5>
                        <h4>Home Delivery</h4>
                        <p>Estimated Delivery By 18-21 Sep 2021</p>
                    </div>
                </div>
                <div class="checout_shiped_products">
                    <div class="table-responsive mb-0">
                        <table class="table amazy_table3 style3 mb-0">
                            <tbody>
                                <tr>
                                    <td>
                                        <a href="product_details.php" class="d-flex align-items-center gap_20">
                                            <div class="thumb">
                                                <img src="img/amazPorduct/summery_product_1.png" alt="">
                                            </div>
                                            <div class="summery_pro_content">
                                                <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                            </div>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center gap_7">
                                            <span class="green_badge">-30%</span>
                                            <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                        </div>
                                    </td>
                                    <td>
                                        <h4 class="font_16 f_w_500 m-0 text-nowrap">Qty: 1</h4>
                                    </td>
                                    <td>
                                        <h4 class="font_16 f_w_500 m-0 text-nowrap">$4,00.00</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="product_details.php" class="d-flex align-items-center gap_20">
                                            <div class="thumb">
                                                <img src="img/amazPorduct/summery_product_1.png" alt="">
                                            </div>
                                            <div class="summery_pro_content">
                                                <h4 class="font_16 f_w_700 text-nowrap m-0 theme_hover">UiiSii C100 In Ear Earphone</h4>
                                                <p class="font_14 f_w_400 m-0 ">UiiSii, Color Family:Black</p>
                                            </div>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center gap_7">
                                            <span class="green_badge">-30%</span>
                                            <span class="font_16 f_w_500 mute_text text-decoration-line-through ">$5,00.00</span>
                                        </div>
                                    </td>
                                    <td>
                                        <h4 class="font_16 f_w_500 m-0 text-nowrap">Qty: 1</h4>
                                    </td>
                                    <td>
                                        <h4 class="font_16 f_w_500 m-0 text-nowrap">$4,00.00</h4>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="shiping_address_box checkout_form m-0">
                <div class="billing_address">
                    <h3 class="check_v3_title mb_25">Shipping Address</h3>
                    <form action="#">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="primary_label2 style3" >First Name</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="Enter your first and last name">
                            </div>
                            <div class="col-lg-6">
                                <label class="primary_label2 style3" >Last Name</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="Enter your first and last name">
                            </div>
                            <div class="col-lg-12">
                                <label class="primary_label2 style3" >Address</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="House# 122, Street# 245, ABC Road">
                            </div>
                            <div class="col-lg-12">
                                <label class="primary_label2 style3" >Address Line 2 (Optional)</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="Apartment, suite, etc. (optional)">
                            </div>
                            <div class="col-lg-6">
                                <label class="primary_label2 style3" >Country / Region</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="United Kingdom">
                            </div>
                            <div class="col-lg-6">
                                <label class="primary_label2 style3" >Postal Code</label>
                                <input class="primary_input3 style5 radius_3px mb_20" type="text"  placeholder="154327">
                            </div>
                            <div class="col-12 mb_25">
                                <label class="primary_checkbox d-flex">
                                    <input checked="" type="checkbox">
                                    <span class="checkmark mr_15"></span>
                                    <span class="label_name f_w_400 ">I agree with the terms and conditions.</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <div class="check_v3_btns flex-wrap d-flex align-items-center">
                                    <a href="shoping_v2.php" class="amaz_primary_btn style2  min_200 text-center text-uppercase ">Continue to shipping</a>
                                    <a href="cart_v2.php" class="return_text">Return to cart</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="checkout_v3_right d-flex justify-content-start">
        <div class="order_sumery_box flex-fill">
            <h3 class="check_v3_title mb_25">Order Summary</h3>
            <div class="subtotal_lists">
                <div class="single_total_list d-flex align-items-center">
                    <div class="single_total_left flex-fill">
                        <h4>Subtotal</h4>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 1324.35</span>
                    </div>
                </div>
                <div class="single_total_list d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <h4>Shipping Charge</h4>
                        <p>Package Wise Shipping Charge</p>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 75.35</span>
                    </div>
                </div>
                <div class="single_total_list d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <h4>Discount</h4>
                    </div>
                    <div class="single_total_right">
                        <span>+ USD 75.35</span>
                    </div>
                </div>
                <div class="total_amount d-flex align-items-center flex-wrap">
                    <div class="single_total_left flex-fill">
                        <span class="total_text">Total (Incl. VAT)</span>
                    </div>
                    <div class="single_total_right">
                        <span class="total_text">USD <span>$1324.35</span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- checkout_v3_area::end  -->

<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>