<?php include 'include/header.php' ?>
<?php include 'include/menu_1.php' ?>

<!-- home_banner::start  -->
<div class="home_banner bannerUi_active owl-carousel">
    <div class="banner_single home_Banner_bg_1">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-1">
                    <div class="banner__text">
                        <span>Spring Accessories</span>
                        <h3>Minimal Fashion 
                            Boy’s T-Shirt.</h3>
                        <a href="product.php" class="amaz_primary_btn min_200">Explore Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="banner_single home_Banner_bg_2">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-1">
                    <div class="banner__text">
                        <span>Spring Accessories</span>
                        <h3>Minimal Fashion 
                            Boy’s T-Shirt.</h3>
                        <a href="product.php" class="amaz_primary_btn min_200">Explore Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="banner_single home_Banner_bg_3">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-1">
                    <div class="banner__text">
                        <span>Spring Accessories</span>
                        <h3>Minimal Fashion 
                            Boy’s T-Shirt.</h3>
                        <a href="product.php" class="amaz_primary_btn min_200">Explore Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- home_banner::end  -->

<div class="amaz_section section_spacing ">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title d-flex align-items-center gap-3 mb_30 flex-wrap">
                    <h3 class="m-0 flex-fill">Trending Products</h3>
                    <a href="product.php" class="title_link d-flex align-items-center lh-1">
                        <span class="title_text">More Deals</span>
                        <span class="title_icon">
                            <i class="fas fa-chevron-right"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row custom_rowProduct ">
            <div class="col-xl-3 col-lg-4 col-md-6 col-6">
                <div class="product_widget5 mb_30">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/1.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="compare.php">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1" > -20% </span>
                        <span class="badge_1 style2 text-uppercase" > New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                             <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-6">
                <div class="product_widget5 mb_30">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/2.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="compare.php">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1" > -20% </span>
                        <span class="badge_1 style2 text-uppercase" > New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                             <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-6">
                <div class="product_widget5 mb_30">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/3.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="compare.php">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1" > -20% </span>
                        <span class="badge_1 style2 text-uppercase" > New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                             <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-6">
                <div class="product_widget5 mb_30">
                    <div class="product_thumb_upper">
                        <a href="product_details.php" class="thumb">
                            <img src="img/amazPorduct/4.png" alt="">
                        </a>
                        <div class="product_action">
                            <a href="compare.php">
                                <i class="ti-control-shuffle"></i>
                            </a>
                            <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                <i class="ti-eye"></i>
                            </a>
                            <a href="#">
                                <i class="ti-star"></i>
                            </a>
                        </div>
                        <span class="badge_1" > -20% </span>
                        <span class="badge_1 style2 text-uppercase" > New </span>
                    </div>
                    <div class="product__meta text-center">
                        <span class="product_banding ">Studio Design</span>
                        <a href="product_details.php">
                            <h4>Designs Woolrich Kletter</h4>
                        </a>
                        <div class="stars justify-content-center">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <div class="product_prise">
                            <p><span>$29.00 </span>  $20.00</p>
                             <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- amaz_section::start  -->
<div class="amaz_section ">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title d-flex align-items-center gap-3 mb_30 flex-wrap ">
                    <h3 class="m-0 flex-fill">Hot Categories</h3>
                    <a href="product.php" class="title_link d-flex align-items-center lh-1">
                        <span class="title_text">More Deals</span>
                        <span class="title_icon">
                            <i class="fas fa-chevron-right"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="amaz_home_cartBox amaz_cat_bg1 d-flex justify-content-end mb_30">
                    <div class="amazcat_text_box">
                        <h4>
                            <a href="#">Electronic Gadget</a>
                        </h4>
                        <p class="lh-1">43 Products</p>
                        <a class="shop_now_text" href="product.php">Shop now »</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="amaz_home_cartBox amaz_cat_bg2 d-flex justify-content-end mb_30">
                    <div class="amazcat_text_box">
                        <h4>
                            <a href="#">Electronic Gadget</a>
                        </h4>
                        <p class="lh-1">43 Products</p>
                        <a class="shop_now_text" href="product.php">Shop now »</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="amaz_home_cartBox amaz_cat_bg3 d-flex justify-content-end mb_30">
                    <div class="amazcat_text_box">
                        <h4>
                            <a href="#">Electronic Gadget</a>
                        </h4>
                        <p class="lh-1">43 Products</p>
                        <a class="shop_now_text" href="product.php">Shop now »</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="amaz_home_cartBox amaz_cat_bg4 d-flex justify-content-end mb_30">
                    <div class="amazcat_text_box">
                        <h4>
                            <a href="#">Electronic Gadget</a>
                        </h4>
                        <p class="lh-1">43 Products</p>
                        <a class="shop_now_text" href="product.php">Shop now »</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="amaz_home_cartBox amaz_cat_bg5 d-flex justify-content-end mb_30">
                    <div class="amazcat_text_box">
                        <h4>
                            <a href="#">Electronic Gadget</a>
                        </h4>
                        <p class="lh-1">43 Products</p>
                        <a class="shop_now_text" href="product.php">Shop now »</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="amaz_home_cartBox amaz_cat_bg6 d-flex justify-content-end mb_30">
                    <div class="amazcat_text_box">
                        <h4>
                            <a href="#">Electronic Gadget</a>
                        </h4>
                        <p class="lh-1">43 Products</p>
                        <a class="shop_now_text" href="product.php">Shop now »</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- amaz_section::end  -->
<!-- new  -->
<!-- new  -->
<!-- amaz_section::start  -->
<div class="amaz_section section_spacing2">
    <div class="container ">
        <div class="row no-gutters">
            <div class="col-xl-5 p-0 col-lg-12">
                <div class="House_Appliances_widget">
                    <div class="House_Appliances_widget_left d-flex flex-column flex-fill">
                        <h4>House Appliances</h4>
                        <ul class="nav nav-tabs flex-fill flex-column border-0" id="myTab10" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="Dining-tab" data-bs-toggle="tab" data-bs-target="#Dining" type="button" role="tab" aria-controls="Dining" aria-selected="true">Dining Table</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="Living-tab" data-bs-toggle="tab" data-bs-target="#Living" type="button" role="tab" aria-controls="Living" aria-selected="false">Living Room</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="Bed-tab" data-bs-toggle="tab" data-bs-target="#Bed" type="button" role="tab" aria-controls="Bed" aria-selected="false">Bed Room</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="Dressing-tab" data-bs-toggle="tab" data-bs-target="#Dressing" type="button" role="tab" aria-controls="Dressing" aria-selected="false">Dressing Table</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="Sofa-tab" data-bs-toggle="tab" data-bs-target="#Sofa" type="button" role="tab" aria-controls="Sofa" aria-selected="false">Sofa Set</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="Drawing-tab" data-bs-toggle="tab" data-bs-target="#Drawing" type="button" role="tab" aria-controls="Drawing" aria-selected="false">Drawing Room</button>
                            </li>
                        </ul>
                        <a href="product.php" class="title_link d-flex align-items-center lh-1">
                            <span class="title_text">More Deals</span>
                            <span class="title_icon">
                                <i class="fas fa-chevron-right"></i>
                            </span>
                        </a>
                    </div>
                    <div class="House_Appliances_widget_right">
                        <h4 class="d-flex flex-column mb-0"> <span>SHOT.</span> <span>Create.</span> <span>Inspire.</span> </h4>
                        <h5>Save up-to 24%</h5>
                        <a href="#" class="amaz_primary_btn small_btn text-center">Buy now</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-7 p-0 col-lg-12">
                <div class="tab-content" id="myTabContent10">
                    <div class="tab-pane fade show active" id="Dining" role="tabpanel" aria-labelledby="Dining-tab">
                        <!-- content  -->
                        <div class="House_Appliances_product">
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_1.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_2.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_3.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_4.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_5.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_6.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content  -->
                    </div>
                    <div class="tab-pane fade" id="Living" role="tabpanel" aria-labelledby="Living-tab">
                        <!-- content  -->
                        <div class="House_Appliances_product">
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_4.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_2.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_3.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_4.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_5.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_6.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content  -->
                    </div>
                    <div class="tab-pane fade" id="Bed" role="tabpanel" aria-labelledby="Bed-tab">
                        <!-- content  -->
                        <div class="House_Appliances_product">
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_6.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_1.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_3.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_4.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_5.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_6.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content  -->
                    </div>
                    <div class="tab-pane fade" id="Dressing" role="tabpanel" aria-labelledby="Dressing-tab">
                        <!-- content  -->
                        <div class="House_Appliances_product">
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_1.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_2.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_3.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_4.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_5.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_6.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content  -->
                    </div>
                    <div class="tab-pane fade" id="Sofa" role="tabpanel" aria-labelledby="Sofa-tab">
                        <!-- content  -->
                        <div class="House_Appliances_product">
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_4.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_3.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_1.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_4.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_5.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_6.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content  -->
                    </div>
                    <div class="tab-pane fade" id="Drawing" role="tabpanel" aria-labelledby="Drawing-tab">
                        <!-- content  -->
                        <div class="House_Appliances_product">
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_1.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_2.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_3.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_4.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_5.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style4 ">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/house_6.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <div class="stars justify-content-center m-0">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="product_details.php">
                                        <h4>Soborg Chair Wood
                                        Frame New Model</h4>
                                        </a>
                                    
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- amaz_section::end  -->

<!-- cta::start  -->
<div class="amaz_section">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="amaz_cta_box">
                    <div class="row justify-content-end ">
                        <div class="col-xl-8 d-flex align-items-center gap-2 flex-wrap ">
                            <div class="amaz_cta_text flex-fill">
                                <span>Xiaomi MI Band 5</span>
                                <h4>Bigger Dynamic</h4>
                                <h5>Color range</h5>
                            </div>
                            <a href="#" class="amaz_white_btn min_170 lh-1">Explore Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta::end  -->


<div class="amaz_section section_spacing3">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="nav amzcart_tabs d-flex align-items-center justify-content-center flex-wrap " id="myTab" role="tablist">
                    <li class="nav-item " role="presentation">
                        <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Top Rating</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">People’s choice</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Featured</button>
                    </li>
                </ul>
                
            </div>
            <div class="col-xl-12">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <!-- conttent  -->
                        <div class="amaz_fieature_active fieature_crousel_area owl-carousel">
                            <div class="product_widget5 style2 mb_30">
                                <div class="product_thumb_upper">
                                    <div class="thumb">
                                        <img src="img/amazPorduct/rating_pro1.png" alt="">
                                    </div>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                    <span class="badge_1" > -20% </span>
                                    <span class="badge_1 style2 text-uppercase" > New </span>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Designs Woolrich Kletter</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$200.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style2 mb_30">
                                <div class="product_thumb_upper">
                                    <div class="thumb">
                                        <img src="img/amazPorduct/rating_pro2.png" alt="">
                                    </div>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Designs Woolrich Kletter</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$200.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- conttent  -->
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <!-- conttent  -->
                        <div class="amaz_fieature_active fieature_crousel_area owl-carousel">
                            <div class="product_widget5 style2 mb_30">
                                <div class="product_thumb_upper">
                                    <div class="thumb">
                                        <img src="img/amazPorduct/rating_pro1.png" alt="">
                                    </div>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                    <span class="badge_1" > -20% </span>
                                    <span class="badge_1 style2 text-uppercase" > New </span>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Designs Woolrich Kletter</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$200.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style2 mb_30">
                                <div class="product_thumb_upper">
                                    <div class="thumb">
                                        <img src="img/amazPorduct/rating_pro2.png" alt="">
                                    </div>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Designs Woolrich Kletter</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$200.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- conttent  -->
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <!-- conttent  -->
                        <div class="amaz_fieature_active fieature_crousel_area owl-carousel">
                            <div class="product_widget5 style2 mb_30">
                                <div class="product_thumb_upper">
                                    <div class="thumb">
                                        <img src="img/amazPorduct/rating_pro1.png" alt="">
                                    </div>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                    <span class="badge_1" > -20% </span>
                                    <span class="badge_1 style2 text-uppercase" > New </span>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Designs Woolrich Kletter</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$200.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style2 mb_30">
                                <div class="product_thumb_upper">
                                    <div class="thumb">
                                        <img src="img/amazPorduct/rating_pro2.png" alt="">
                                    </div>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Designs Woolrich Kletter</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$200.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- conttent  -->
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<div class="amaz_section amaz_deal_area ">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="deal_wizard deal_wizard_bg1 mb_30">
                    <h3>SCREEN
                    SHIELDS
                    -50%</h3>
                    <a href="product.php" class="shop_text">Shop now</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="deal_wizard deal_wizard_bg2 mb_30">
                    <h3>NEW
                    COLOR
                    CASES</h3>
                    <a href="product.php" class="shop_text">Shop now</a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="deal_wizard deal_wizard_bg3 mb_30">
                    <h3>Daltex 
                        Product 
                        Example</h3>
                    <a href="product.php" class="shop_text">Shop now</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- amaz_recomanded::start  -->
<div class="amaz_recomanded_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="amaz_recomanded_box mb_60">
                    <div class="amaz_recomanded_box_head">
                        <h4 class="mb-0">Recommendation For You</h4>
                    </div>
                    <div class="bannerUi_Recommendation_active owl-carousel ">
                        <div class="amaz_recomanded_box_body">
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_1.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_2.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="amaz_recomanded_box_body">
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_3.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_4.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="amaz_recomanded_box_body">
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_5.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_1.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="amaz_recomanded_box_body">
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_1.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_2.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="amaz_recomanded_box_body">
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_4.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a  href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_5.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="amaz_recomanded_box_body">
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_5.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_widget5 style3 bg-white">
                                <div class="product_thumb_upper">
                                    <a href="product_details.php" class="thumb">
                                        <img src="img/amazPorduct/rec_produict_1.png" alt="">
                                    </a>
                                    <div class="product_action">
                                        <a href="compare.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-bs-toggle="modal" data-bs-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product__meta text-center">
                                    <span class="product_banding ">Studio Design</span>
                                    <a href="product_details.php">
                                        <h4>Universal Wifi Range
                                            Extender</h4>
                                    </a>
                                    <div class="stars justify-content-center">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <div class="product_prise">
                                        <p>$20.00</p>
                                        <a class="add_cart add_to_cart " data-bs-toggle="modal" data-bs-target="#cart_add_modal"  href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center">
                <a href="#" class="amaz_primary_btn2 min_200">Load more</a>
            </div>
        </div>
    </div>
</div>
<!-- amaz_recomanded::end -->

<!-- amaz_brand::start  -->
<div class="amaz_brand">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title d-flex align-items-center gap-3 mb_30">
                    <h3 class="m-0 flex-fill">Top Brand</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="amazBrand_boxes">
                    <a href="brand.php" class="single_brand d-block">
                        <img src="img/amaz_brand/1.png" alt="">
                    </a>
                    <a href="brand.php" class="single_brand d-block">
                        <img src="img/amaz_brand/2.png" alt="">
                    </a>
                    <a href="brand.php" class="single_brand d-block">
                        <img src="img/amaz_brand/3.png" alt="">
                    </a>
                    <a href="brand.php" class="single_brand d-block">
                        <img src="img/amaz_brand/4.png" alt="">
                    </a>
                    <a href="brand.php" class="single_brand d-block">
                        <img src="img/amaz_brand/5.png" alt="">
                    </a>
                    <a href="brand.php" class="single_brand d-block">
                        <img src="img/amaz_brand/6.png" alt="">
                    </a>
                    <a href="brand.php" class="single_brand d-block">
                        <img src="img/amaz_brand/7.png" alt="">
                    </a>
                    <a href="brand.php" class="single_brand d-block">
                        <img src="img/amaz_brand/8.png" alt="">
                    </a>
                    <a href="brand.php" class="single_brand d-block">
                        <img src="img/amaz_brand/9.png" alt="">
                    </a>
                    <a href="brand.php" class="single_brand d-block">
                        <img src="img/amaz_brand/10.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- amaz_brand::end  -->

<!-- Popular Searches::start  -->
<div class="amaz_popular_search section_spacing">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title d-flex align-items-center gap-3 mb_30">
                    <h3 class="m-0 flex-fill">Popular Searches</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="popular_search_lists mb_30">
                    <a class="popular_search_list" href="#">Corporate</a>
                    <a class="popular_search_list" href="#">New Arrival</a>
                    <a class="popular_search_list" href="#">Office</a>
                    <a class="popular_search_list" href="#">Sports & Outdoor</a>
                    <a class="popular_search_list" href="#">Baby Toys</a>
                    <a class="popular_search_list" href="#">Household Items</a>
                    <a class="popular_search_list" href="#">Accessories</a>
                    <a class="popular_search_list" href="#">Computer & Office</a>
                    <a class="popular_search_list" href="#">Appliance</a>
                    <a class="popular_search_list" href="#">Indoor Chair</a>
                    <a class="popular_search_list" href="#">Hot Categories</a>
                    <a class="popular_search_list" href="#">Bluetooth</a>
                    <a class="popular_search_list" href="#">Watches</a>
                    <a class="popular_search_list" href="#">Ladies Watches</a>
                    <a class="popular_search_list" href="#">Headphones</a>
                    <a class="popular_search_list" href="#">Most Popular Products</a>
                    <a class="popular_search_list" href="#">Men’s Fashion</a>
                    <a class="popular_search_list" href="#">Kitchen Appliance</a>
                    <a class="popular_search_list" href="#">Women’s Fashion</a>
                    <a class="popular_search_list" href="#">Mobile Accessories</a>
                    <a class="popular_search_list" href="#">Nike</a>
                    <a class="popular_search_list" href="#">Electronic Gadgets</a>
                    <a class="popular_search_list" href="#">Men’s Gadgets</a>
                    <a class="popular_search_list" href="#">Women’s Gadgets</a>
                    <a class="popular_search_list" href="#">Mobile Accessories</a>
                    <a class="popular_search_list" href="#">Nike</a>
                    <a class="popular_search_list" href="#">Electronic Gadgets</a>
                    <a class="popular_search_list" href="#">Men’s Gadgets</a>
                    <a class="popular_search_list" href="#">Women’s Gadgets</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Popular Searches::end  -->

<div class="amaz_section section_spacing4">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title d-flex align-items-center gap-3 mb_20">
                    <h3 class="m-0 flex-fill">Amazing | AN ONLINE SHOPPING PLATFORM</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="amaz_mazing_text">
                    <p>Everyone loves online shopping for the convenience and the low prices are a bonus! THE SHOP strives to help you get a bang for your buck with multiple sales and promotions happening at any one time. 
                        Make sure your wallet is ready for our big sales campaigns, such as 7.7 Orange Madness, 9.9 Super Shopping Day, 10.10 Brands Festival, 11.11 Big Sale, and 12.12 Birthday Sale. We also have massive sales 
                        and offer great savings to you during major festivals like Chinese New Year and Raya! At the same time, check in often because we also have smaller sale periods which are category-specific, such as our 
                        Baby Fair and Black Friday sales.</p>
                    <h4 class=" text-uppercase f_w_700 font_14 ">SHOP FOR VARIETY WITH Amazing</h4>
                    <p>Everyone loves online shopping for the convenience and the low prices are a bonus! THE SHOP strives to help you get a bang for your buck with multiple sales and promotions happening at any one time. 
                        Make sure your wallet is ready for our big sales campaigns, such as 7.7 Orange Madness, 9.9 Super Shopping Day, 10.10 Brands Festival, 11.11 Big Sale, and 12.12 Birthday Sale. We also have massive sales 
                        and offer great savings to you during major festivals like Chinese New Year and Raya! At the same time, check in often because we also have smaller sale periods which are category-specific, such as our 
                        Baby Fair and Black Friday sales.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include 'include/footer_content3.php' ?>
<?php include 'include/footer.php' ?>
